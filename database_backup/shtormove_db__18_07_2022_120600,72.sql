-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: shtormove_db
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `about_page_info`
--

DROP TABLE IF EXISTS `about_page_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `about_page_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_title` varchar(255) DEFAULT NULL,
  `main_subtitle` varchar(255) DEFAULT NULL,
  `about_main_title` varchar(255) DEFAULT NULL,
  `about_main_subtitle` varchar(255) DEFAULT NULL,
  `about_description` text,
  `about_ad1_subtitle` varchar(255) DEFAULT NULL,
  `about_ad1_description` text,
  `about_ad2_subtitle` varchar(255) DEFAULT NULL,
  `about_ad2_description` text,
  `team_title` varchar(255) DEFAULT NULL,
  `team_subtitle` varchar(255) DEFAULT NULL,
  `ad_title` varchar(255) DEFAULT NULL,
  `ad_subtitle` varchar(255) DEFAULT NULL,
  `ad_specification` text,
  `clients_title` varchar(255) DEFAULT NULL,
  `clients_subtitle` varchar(255) DEFAULT NULL,
  `reservation_title` varchar(255) DEFAULT NULL,
  `reservation_subtitle` varchar(255) DEFAULT NULL,
  `reservation_description` text,
  `reservation_features` text,
  `reservation_video_link` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_page_info`
--

LOCK TABLES `about_page_info` WRITE;
/*!40000 ALTER TABLE `about_page_info` DISABLE KEYS */;
INSERT INTO `about_page_info` VALUES (1,'Про Нас','Більше Про Нас','Прочитати Більше <em>Про Нас</em>','ОТРИМАТИ ІНФОРМАЦІЮ','У пансіонаті «Штормове» є чудові природні піщані пляжі, що омиваються чистою теплою водою Чорного моря. Пляжі, які розтягнулися на десять кілометрів, мають пологе дно, що дуже зручно для людей, які відпочивають з дітьми. Яскраве сонце, чисте свіже повітря та чудові краєвиди чудово підійдуть для любителів літнього відпочинку.\r\n<br><br>\r\nМ\'який морський клімат чудово поєднується із сухим степовим повітрям. Наявність унікальних грязей, які мають лікувальні властивості, роблять це місце ще привабливішим. Температура повітря влітку зазвичай 20-30 градусів, але може досягати 40-градусної позначки в середині літа. Курортний сезон починається з середини травня, а закінчується у вересні або навіть жовтні.','Найкраще планування','Ми сплануємо найкращий для Вас відпочинок, не тільки зручний, але й з додатковими морськими і туристичними розвагами.','Креативний відпочинок','До того ж, ми пропонуємо подорожі до багатьох туристичних місць. У нас можна знайти всі види морських розваг: катання на катамарані та ін.','<em>Учасники</em> Нашої Команди','НАША КОМАНДА','Знижка до 50% на відпочинок всією сім\'єю','Отримайте знижку просто зараз!','Зверніться до адміністрації пансіонату \"Штормове\", щоб дізнатися більше.','Що Наші Клієнти Думають <em>Про Пансіонат</em>','ВІДГУКИ','Станьте ближчими до <em>Відпочинку</em>','ПАНСІОНАТ \"ШТОРМОВЕ\"','Завітайте до нашого пансіонату \"Штормове\"!','<li>- Тепле і чисте море.</li>\r\n                        <li>- Захоплюючі водні атракціони й екскурсії.</li>\r\n                        <li>- Басейн та бар біля басейну.</li>\r\n                        <li>- Столова all inclusive.</li>','https://www.youtube.com/watch?v=JXGd7PB4UYY');
/*!40000 ALTER TABLE `about_page_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bed_type`
--

DROP TABLE IF EXISTS `bed_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bed_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bed_type`
--

LOCK TABLES `bed_type` WRITE;
/*!40000 ALTER TABLE `bed_type` DISABLE KEYS */;
INSERT INTO `bed_type` VALUES (1,'одномісне ліжко'),(2,'двомісне ліжко');
/*!40000 ALTER TABLE `bed_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booked_rooms`
--

DROP TABLE IF EXISTS `booked_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booked_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_room` int NOT NULL,
  `id_site_user` int NOT NULL,
  `move_in` date NOT NULL,
  `move_out` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_room` (`id_room`),
  KEY `id_site_user` (`id_site_user`),
  CONSTRAINT `booked_rooms_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id`),
  CONSTRAINT `booked_rooms_ibfk_2` FOREIGN KEY (`id_site_user`) REFERENCES `site_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booked_rooms`
--

LOCK TABLES `booked_rooms` WRITE;
/*!40000 ALTER TABLE `booked_rooms` DISABLE KEYS */;
INSERT INTO `booked_rooms` VALUES (1,1,2,'2022-07-18','2022-07-20'),(2,2,7,'2022-07-17','2022-07-21'),(3,3,8,'2022-07-17','2022-07-21'),(4,1,6,'2022-07-21','2022-07-23');
/*!40000 ALTER TABLE `booked_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canteen`
--

DROP TABLE IF EXISTS `canteen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `canteen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `category` int DEFAULT NULL,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci,
  `ingredients` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `is_showing` int DEFAULT NULL,
  `is_trend` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `canteen_ibfk_1` (`category`),
  CONSTRAINT `canteen_ibfk_1` FOREIGN KEY (`category`) REFERENCES `canteen_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canteen`
--

LOCK TABLES `canteen` WRITE;
/*!40000 ALTER TABLE `canteen` DISABLE KEYS */;
INSERT INTO `canteen` VALUES (1,'Восьминіг із зеленню',2,'Скільки середземноморських країн - стільки і способів приготування восьминога. У даній страві пропонується приготовлений восьминіг по-іспанськи. Він виходить на подив м\'яким і ароматним. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','цибуля, морква, селера, восьминіг, лавровий лист, запашний перець, трави, картопля відварна, паприка, оливкова олія, сіль морська',158,1,1),(2,'Коктейль \"Ягідна насолода\"',4,'Даний коктейль прекрасно освіжає у літню спеку та дає позитивний заряд енергії. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','сік манго, ожина, банан',50,1,1),(3,'Кава з молоком',4,'Кава з молоком чудово заряджає енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','кава, молоко',45,1,0),(4,'Домашній пиріг',2,'Домашній пиріг смачний і приносить масу користі. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','сир, помідори, кукурудза, броколі, горох, гриби, мясо курки',120,1,1),(5,'Яєшня з мідіями',2,'Яєшня з мідіями насичує організм корисними елементами та заряджає енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','мідії, яйця, зелень, соус',70,1,1),(6,'Мясо із зеленню',2,'Мясо із зеленню насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','мясо, морква, броколі, соус, зелень',90,1,0),(7,'Риба із пастою та зеленню',2,'Риба із пастою та зеленню смакує чудово. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','риба, паста, зелень, соус',62,1,0),(8,'Риба із соусом та зеленню',2,'Риба із соусом та зеленню неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','риба, зелень, соус',84,1,0),(9,'Картопляні пиріжки із соусом та зеленню',2,'Картопляні пиріжки із соусом та зеленню неймовірно смакують та насичують організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','картопля, соус, зелень',36,1,0),(10,'Коктейль з лимоном та лаймом',4,'Коктейль з лимоном та лаймом неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','лимон, лайм, вода, лід',30,1,0),(11,'Смажений банан з манго',3,'Смажений банан з манго неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','банан, манго',28,1,0),(12,'Спегетті в соусі із зеленню',2,'Спегетті в соусі із зеленню неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','спагетті, соус, зелень',67,1,0),(13,'Куряча ніжка з рисом та зеленню',2,'Куряча ніжка з рисом та зеленню неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','куряча ніжка, рис, морква, броколі, зелень',158,1,0),(14,'Млинці із апельсином і грейпфрутом',3,'Млинці із апельсином і грейпфрутом неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','млинці, апельсин, грейпфрут',42,1,1),(15,'Коктейль Апельсинова свіжіть',4,'Коктейль Апельсинова свіжіть неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','сік апельсину та лимону, вода, лід, лайм',20,1,0),(16,'Мясо із сиром та зеленню',2,'Мясо із сиром та зеленню неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','мясо, сир, помідори, зелень',200,1,1),(17,'Коктейль поцілунок космосу',4,'Коктейль поцілунок космосу неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','сік ананаса і манго, вода, лимон, лід',15,1,0),(18,'Ікра з соусом',2,'Ікра з соусом неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','ікра, соус, зелень',500,1,0),(19,'Салат із хамоном і овочами',2,'Салат із хамоном і овочами неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','хамон, морква, зелень, помідори, сир',71,1,0),(20,'Рибні суші із васабі',2,'Рибні суші із васабі неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','рис, риба, васабі, зелень',98,1,1),(21,'Лобстер із лимоном',2,'Лобстер із лимоном неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','лобстер, лимон',800,1,1),(22,'Мясо випечене з сиром, грибами і зеленню',2,'Мясо випечене з сиром і зеленню неймовірно смакує та насичує організм енергією. Страва особисто рекомендована нашим поваром. Смак витончений і незабутній. Також страва корисна для організму.','мясо, сир, гриби, зелень',250,1,0);
/*!40000 ALTER TABLE `canteen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canteen_categories`
--

DROP TABLE IF EXISTS `canteen_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `canteen_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canteen_categories`
--

LOCK TABLES `canteen_categories` WRITE;
/*!40000 ALTER TABLE `canteen_categories` DISABLE KEYS */;
INSERT INTO `canteen_categories` VALUES (1,'перша страва'),(2,'друга страва'),(3,'десерт'),(4,'напій'),(5,'закуска');
/*!40000 ALTER TABLE `canteen_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps`
--

DROP TABLE IF EXISTS `corps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `corps` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `is_showing` tinyint(1) DEFAULT '1',
  `is_popular` tinyint(1) DEFAULT '0',
  `sea_line` int DEFAULT NULL,
  `category` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sea_line` (`sea_line`),
  KEY `category` (`category`),
  CONSTRAINT `corps_ibfk_1` FOREIGN KEY (`sea_line`) REFERENCES `sea_lines` (`id`),
  CONSTRAINT `corps_ibfk_2` FOREIGN KEY (`category`) REFERENCES `corps_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps`
--

LOCK TABLES `corps` WRITE;
/*!40000 ALTER TABLE `corps` DISABLE KEYS */;
INSERT INTO `corps` VALUES (1,'Бригантина','вул. Набережна 1','Чудовий корпус для прожиття та відпочинку всією сімєю.\n                                                        Тут ви можете прекрасно провести свій час. У даному корпусі є все для\n                                                        комфортного відпочинку.\n                                                        Цей корпус розташований недалеко від моря, водних атракціонів та столової.\n                                                        Також тут є бар із неймовірною кількістю різноманітних напоїв.\n                                                        Регулярно проводяться екскурсії на верблюдах, катерах, кораблях, а також\n                                                        екскурсії із скатами із погруженням під воду. У номерах завжди чисто і затишно.\n                                                        Адміністрація корпусу завжди привітна і рада допомогти відпочивальникам.\n                                                        З нетерпінням чекаємо на Вас у нашому пансіонаті!',1,0,1,1),(2,'Чайка','вул. Набережна 2','Чудовий корпус для прожиття та відпочинку всією сімєю.\n                                                        Тут ви можете прекрасно провести свій час. У даному корпусі є все для\n                                                        комфортного відпочинку.\n                                                        Цей корпус розташований недалеко від моря, водних атракціонів та столової.\n                                                        Також тут є бар із неймовірною кількістю різноманітних напоїв.\n                                                        Регулярно проводяться екскурсії на верблюдах, катерах, кораблях, а також\n                                                        екскурсії із скатами із погруженням під воду. У номерах завжди чисто і затишно.\n                                                        Адміністрація корпусу завжди привітна і рада допомогти відпочивальникам.\n                                                        З нетерпінням чекаємо на Вас у нашому пансіонаті!',1,0,2,3),(3,'Фрегат','вул. Набережна 3','Чудовий корпус для прожиття та відпочинку всією сімєю.\n                                                        Тут ви можете прекрасно провести свій час. У даному корпусі є все для\n                                                        комфортного відпочинку.\n                                                        Цей корпус розташований недалеко від моря, водних атракціонів та столової.\n                                                        Також тут є бар із неймовірною кількістю різноманітних напоїв.\n                                                        Регулярно проводяться екскурсії на верблюдах, катерах, кораблях, а також\n                                                        екскурсії із скатами із погруженням під воду. У номерах завжди чисто і затишно.\n                                                        Адміністрація корпусу завжди привітна і рада допомогти відпочивальникам.\n                                                        З нетерпінням чекаємо на Вас у нашому пансіонаті!',1,0,1,4),(4,'Галеон','вул. Набережна 4','Чудовий корпус для прожиття та відпочинку всією сімєю.\n                                                        Тут ви можете прекрасно провести свій час. У даному корпусі є все для\n                                                        комфортного відпочинку.\n                                                        Цей корпус розташований недалеко від моря, водних атракціонів та столової.\n                                                        Також тут є бар із неймовірною кількістю різноманітних напоїв.\n                                                        Регулярно проводяться екскурсії на верблюдах, катерах, кораблях, а також\n                                                        екскурсії із скатами із погруженням під воду. У номерах завжди чисто і затишно.\n                                                        Адміністрація корпусу завжди привітна і рада допомогти відпочивальникам.\n                                                        З нетерпінням чекаємо на Вас у нашому пансіонаті!',1,0,3,2);
/*!40000 ALTER TABLE `corps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corps_category`
--

DROP TABLE IF EXISTS `corps_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `corps_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corps_category`
--

LOCK TABLES `corps_category` WRITE;
/*!40000 ALTER TABLE `corps_category` DISABLE KEYS */;
INSERT INTO `corps_category` VALUES (1,'Із атракціонами'),(2,'Найкращий для відпочинку з дітьми'),(3,'Із басейном'),(4,'Найкращий для вечірок та молоді');
/*!40000 ALTER TABLE `corps_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `footer_info`
--

DROP TABLE IF EXISTS `footer_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `footer_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `facebook_link` text NOT NULL,
  `twitter_link` text NOT NULL,
  `linkedin_link` text NOT NULL,
  `instagram_link` text NOT NULL,
  `developer` varchar(255) NOT NULL,
  `developer_link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footer_info`
--

LOCK TABLES `footer_info` WRITE;
/*!40000 ALTER TABLE `footer_info` DISABLE KEYS */;
INSERT INTO `footer_info` VALUES (1,'с. Молочне, Одеса, Україна','+380975846227','https://www.facebook.com','https://twitter.com/','https://ua.linkedin.com','https://www.instagram.com','Olena Machushnyk','https://telegram.me/Oleennaa');
/*!40000 ALTER TABLE `footer_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_photos`
--

DROP TABLE IF EXISTS `gallery_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery_photos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `alt_attribute` varchar(255) DEFAULT NULL,
  `category` int DEFAULT NULL,
  `is_showing` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  CONSTRAINT `gallery_photos_ibfk_1` FOREIGN KEY (`category`) REFERENCES `gallery_photos_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_photos`
--

LOCK TABLES `gallery_photos` WRITE;
/*!40000 ALTER TABLE `gallery_photos` DISABLE KEYS */;
INSERT INTO `gallery_photos` VALUES (1,'Краєвид на гори та вихід до басейну','Басейн, гори','Басейн',4,1),(2,'Краєвид на басейн та атракціони','Басейн, атракціони','Басейн',5,1),(3,'Вид із столової на море','Столова, море','Столова',3,1),(4,'Страва із морепродуктами','Страва','Страва',2,1),(5,'Вид із столової','Столова, краєвид','Столова',3,1),(6,'Номер пансіонату','Номер','Номер',1,1),(7,'Ванна номера із видом на море і гори','Ванна, номер','Ванна',1,1),(8,'Вид номера пансіонату','Номер, ліжко, балкон','Номер',1,1),(9,'Вигляд з номера на балкон','Номер, балкон','Номер',1,1),(10,'Вигляд на море з балкону','Балкон, вид, море','Балкон',4,1),(11,'Страва в столовій','Страва, столова','Страва',2,1),(12,'Вигляд столової пансіонату','Столова','Столова',3,1),(13,'Вигляд столової пансіонату','Столова','Столова',3,1),(14,'Вид з балкону','Балкон, краєвид','Краєвид',4,1),(15,'Екскурсія на кораблі','Корабель, екскурсія','Корабель',6,1),(16,'Вечірка в пансіонаті','Вечірка, dj','Вечірка',7,1),(17,'Святкування дня народження','День народження, свято','Свято',7,1),(18,'Атракціон із парашутом','Атракціон, море, парашут','Атракціон',5,1),(19,'Вечірка в пансіонаті','Вечірка, js','Вечірка',7,1),(20,'Екскурсія під водою із скатами','Екскурсія, море, скати','Екскурсія',6,1),(21,'Водні гірки та басейн','Водні атракціони','Атракціони',5,1),(22,'Екскурсія на катері','Екскурсія, море, катер','Екскурсія',6,1),(23,'Водні гірки та басейн','Водні атракціони','Атракціони',5,1),(24,'Екскурсія на верблюдах','Екскурсія, верблюди','Екскурсія',6,1),(25,'Водні гірки та басейн','Водні атракціони','Атракціони',5,1);
/*!40000 ALTER TABLE `gallery_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_photos_categories`
--

DROP TABLE IF EXISTS `gallery_photos_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery_photos_categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_photos_categories`
--

LOCK TABLES `gallery_photos_categories` WRITE;
/*!40000 ALTER TABLE `gallery_photos_categories` DISABLE KEYS */;
INSERT INTO `gallery_photos_categories` VALUES (1,'Номери'),(2,'Їжа'),(3,'Столові'),(4,'Краєвиди'),(5,'Атракціони'),(6,'Екскурсії'),(7,'Свята');
/*!40000 ALTER TABLE `gallery_photos_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gender` (
  `id` int NOT NULL AUTO_INCREMENT,
  `gender` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'жінка'),(2,'чоловік');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_page_info`
--

DROP TABLE IF EXISTS `main_page_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `main_page_info` (
  `id` int NOT NULL AUTO_INCREMENT,
  `main_name` varchar(255) NOT NULL,
  `main_description` text NOT NULL,
  `about_name` varchar(255) NOT NULL,
  `about_description` text NOT NULL,
  `services_name` varchar(255) NOT NULL,
  `services_description` text NOT NULL,
  `services_array` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `gallery_name` varchar(255) NOT NULL,
  `gallery_description` text NOT NULL,
  `gallery_array` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `testimonials_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `main_page_info_chk_1` CHECK (json_valid(`services_array`)),
  CONSTRAINT `main_page_info_chk_2` CHECK (json_valid(`gallery_array`))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_page_info`
--

LOCK TABLES `main_page_info` WRITE;
/*!40000 ALTER TABLE `main_page_info` DISABLE KEYS */;
INSERT INTO `main_page_info` VALUES (1,'Пансіонат \"Штормове\"','У пансіонаті «Штормове» є чудові природні піщані пляжі, що омиваються чистою теплою водою Чорного моря. Пляжі, які розтягнулися на десять кілометрів, мають пологе дно, що дуже зручно для людей, які відпочивають з дітьми. Яскраве сонце, чисте свіже повітря та чудові краєвиди чудово підійдуть для любителів літнього відпочинку.','Про пансіонат','Пансіонат «Штормове» знаходиться в невеликому селищі Молочне, саме тут відпочинок в Одесі гостьові будинки та пансіонати пропонують доступний за вартістю та гідний за рівнем комфорту. Молочне - це зовсім молоде курортне селище. Його інфраструктура почала розвиватися нещодавно, в порівнянні з іншими курортами Одеси. Селище знаходиться на Західному узбережжі Одеської області в унікальному місці.','Послуги','У пансіонаті перша лінія виходу до моря, басейни, водні атракціони (дитячі, надувні атракціони, водні гірки, підводне плавання, гідроцикл), столова all inclusive, бар та багато різних всеможливих екскурсій.','[ [\"1\", \"Море\"], [\"2\", \"Атракціони\"], [\"3\", \"Басейн\"], [\"4\", \"Столова\"], [\"5\", \"Бар\"], [\"6\", \"Екскурсії\"]]','Деякі фотографії із пансіонату','eadable content of a page when looking at its layout. The point of\r\nusing Lorem Ipsum is that it has a more-or-less normal distribution of letters,','[1, 2, 3]','Відгуки');
/*!40000 ALTER TABLE `main_page_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordered_dishes`
--

DROP TABLE IF EXISTS `ordered_dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ordered_dishes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_dish` int NOT NULL,
  `id_site_user` int NOT NULL,
  `quantity` int NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dish` (`id_dish`),
  KEY `id_site_user` (`id_site_user`),
  CONSTRAINT `ordered_dishes_ibfk_1` FOREIGN KEY (`id_dish`) REFERENCES `canteen` (`id`),
  CONSTRAINT `ordered_dishes_ibfk_2` FOREIGN KEY (`id_site_user`) REFERENCES `site_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordered_dishes`
--

LOCK TABLES `ordered_dishes` WRITE;
/*!40000 ALTER TABLE `ordered_dishes` DISABLE KEYS */;
INSERT INTO `ordered_dishes` VALUES (1,22,6,1,'2022-07-17 20:38:21','2022-07-19 20:38:21'),(2,21,2,1,'2022-07-17 08:07:00','2022-07-18 15:02:00'),(3,20,6,1,'2022-07-17 11:07:00','2022-07-15 10:00:00'),(4,18,1,2,'2022-07-18 12:07:00','2022-07-20 10:00:00');
/*!40000 ALTER TABLE `ordered_dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_corps`
--

DROP TABLE IF EXISTS `reviews_corps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews_corps` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_site_user` int NOT NULL,
  `id_corp` int NOT NULL,
  `stars` int NOT NULL,
  `review` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `date` date DEFAULT NULL,
  `is_showing` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_site_user` (`id_site_user`),
  KEY `id_corp` (`id_corp`),
  CONSTRAINT `reviews_corps_ibfk_1` FOREIGN KEY (`id_site_user`) REFERENCES `site_users` (`id`),
  CONSTRAINT `reviews_corps_ibfk_2` FOREIGN KEY (`id_corp`) REFERENCES `corps` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_corps`
--

LOCK TABLES `reviews_corps` WRITE;
/*!40000 ALTER TABLE `reviews_corps` DISABLE KEYS */;
INSERT INTO `reviews_corps` VALUES (1,6,1,5,'Все сподобалося! Заселюсь у цей корпус наступного року також!','2022-07-01',1),(2,2,1,3,'Я думаю, що ціни за проживання завищені для даного корпусу. Якби ціни були нижчі, поставила б всі 5 зірочок!','2021-07-15',1),(3,7,1,2,'Корпус був для мене нудний, часто не було що робити. Проводьте більше вечірок із dj','2022-06-14',1),(4,9,1,5,'Просто чудово!','2020-08-21',1),(5,8,1,1,'У корпусі були таргани!!! Жах!','2022-07-16',1),(6,6,4,3,'Занадто довго йти пішки до моря!','2020-06-21',1),(7,2,4,5,'Прекрасний корпус для відпочинку з дітьми!','2021-08-02',1),(8,8,3,5,'Я провів один з найкращих відпусток саме у цьому корпусі! Рекомендую всім!','2022-07-16',1),(9,8,2,5,'Неймовірний корпус! Також у басейні завжди підходяща вода для плавання!','2022-07-16',1),(10,8,4,5,'Сподобалося все у цьому корпусі, особливо прекрасно продуманий інтерєр!','2022-07-16',1);
/*!40000 ALTER TABLE `reviews_corps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_dishes`
--

DROP TABLE IF EXISTS `reviews_dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews_dishes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_site_user` int NOT NULL,
  `id_dish` int NOT NULL,
  `stars` int NOT NULL,
  `review` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `date` date NOT NULL,
  `is_showing` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_site_user` (`id_site_user`),
  KEY `id_dish` (`id_dish`),
  CONSTRAINT `reviews_dishes_ibfk_1` FOREIGN KEY (`id_site_user`) REFERENCES `site_users` (`id`),
  CONSTRAINT `reviews_dishes_ibfk_2` FOREIGN KEY (`id_dish`) REFERENCES `canteen` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_dishes`
--

LOCK TABLES `reviews_dishes` WRITE;
/*!40000 ALTER TABLE `reviews_dishes` DISABLE KEYS */;
INSERT INTO `reviews_dishes` VALUES (1,2,1,5,'Неймовірно!','2021-05-05',1),(2,6,22,5,'Страва нереально смачна!','2022-07-17',1),(3,6,2,2,'Занадто багато води у коктейлі','2022-07-17',1),(4,6,3,3,'Нормально.','2022-07-17',1),(5,6,4,5,'Дуже смачно!','2022-07-17',1),(6,6,5,3,'Мала порція, голод втамувати не можна!','2022-07-17',1),(7,6,6,1,'Мені не сподобалась страва.','2022-07-17',1),(8,6,7,5,'Супер!','2022-07-17',1),(9,6,8,5,'Рекомендую всім!','2022-07-17',1),(10,6,9,4,'Соус не дуже','2022-07-17',1),(11,6,10,5,'Дуже класний коктейль!','2022-07-17',1),(12,6,11,3,'Таке собі на смак','2022-07-17',1),(13,6,12,2,'Не рекомендую','2022-07-17',1),(14,6,13,5,'Дуже смачно!','2022-07-17',1),(15,6,14,5,'Супер!','2022-07-17',1),(16,2,14,5,'Моя найулюбленіша страва у пансіонаті!','2022-07-17',1),(17,6,15,4,'Смачно, але не на 5 зірочок','2022-07-17',1),(18,6,16,5,'Це шедевр!','2022-07-17',1),(19,6,17,2,'Багато льоду в коктейлі','2022-07-17',1),(20,6,18,2,'Мала порція','2022-07-17',1),(21,6,19,3,'Хамону можна було б і більше додати','2022-07-17',1),(22,6,20,5,'Супер!!!','2022-07-17',1),(23,6,21,5,'Смак бажествений, але ціна кусається.. воно того варте!','2022-07-17',1),(24,2,22,5,'Обожнюю цю страву!','2022-07-17',1);
/*!40000 ALTER TABLE `reviews_dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews_rooms`
--

DROP TABLE IF EXISTS `reviews_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews_rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_site_user` int DEFAULT NULL,
  `id_room` int DEFAULT NULL,
  `stars` int DEFAULT NULL,
  `review` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci,
  `date` date DEFAULT NULL,
  `is_showing` int DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_site_user` (`id_site_user`),
  KEY `id_room` (`id_room`),
  CONSTRAINT `reviews_rooms_ibfk_1` FOREIGN KEY (`id_site_user`) REFERENCES `site_users` (`id`),
  CONSTRAINT `reviews_rooms_ibfk_2` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews_rooms`
--

LOCK TABLES `reviews_rooms` WRITE;
/*!40000 ALTER TABLE `reviews_rooms` DISABLE KEYS */;
INSERT INTO `reviews_rooms` VALUES (1,2,1,5,'Все сподобалось!','2022-07-01',1),(2,6,2,4,'Чудово!','2021-08-27',1),(3,9,17,4,'Номер прекрасний, але з балконом було б краще.','2022-07-17',1),(4,8,1,5,'Недоліків у цього номера просто немає!','2021-07-23',1),(5,2,3,5,'Неймовірний вид із вікна!','2022-04-13',1),(6,8,4,3,'Ремонт трішки застарівший.','2022-06-16',1),(7,7,5,3,'Дуже багато темних кольорів у дизайні.','2022-07-08',1),(8,8,6,5,'Найкращий вибір комфорту та якості!','2022-05-27',1),(9,7,7,5,'Добре, що дозволено палити цигарки у номері','2022-07-11',1),(10,8,8,5,'Розкішний дизайн!','2022-07-17',1),(11,2,9,5,'Неймовірний ремонт, рекомендую всім.','2022-06-15',1),(12,9,10,3,'Балкон є не дуже безпечним для дітей.','2021-05-14',1),(13,6,11,4,'Хотілося б більше дизайнерських рішень у даному номері.','2020-08-31',1),(14,8,12,5,'Неймовірно! Всим задоволені!','2022-07-17',1),(15,8,13,1,'Деколи заповзають таргани, жах...','2022-07-17',1),(16,8,14,3,'Шумоізоляція на поганому рівні.','2022-07-17',1),(17,8,15,5,'Сподобалось все!','2022-07-17',1),(18,8,16,5,'Неймовірно!!!','2022-07-17',1);
/*!40000 ALTER TABLE `reviews_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `team_table` int DEFAULT NULL,
  `canteen_table` int DEFAULT NULL,
  `gallery_table` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `team_table` (`team_table`),
  KEY `canteen_table` (`canteen_table`),
  KEY `gallery_table` (`gallery_table`),
  CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`team_table`) REFERENCES `roles_actions` (`id`),
  CONSTRAINT `roles_ibfk_2` FOREIGN KEY (`canteen_table`) REFERENCES `roles_actions` (`id`),
  CONSTRAINT `roles_ibfk_3` FOREIGN KEY (`gallery_table`) REFERENCES `roles_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin',7,7,7),(2,'Vacationer',8,1,1);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_actions`
--

DROP TABLE IF EXISTS `roles_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_actions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_actions`
--

LOCK TABLES `roles_actions` WRITE;
/*!40000 ALTER TABLE `roles_actions` DISABLE KEYS */;
INSERT INTO `roles_actions` VALUES (1,'тільки читати'),(2,'тільки оновлювати'),(3,'тільки видаляти'),(4,'читати і оновлювати'),(5,'читати і видаляти'),(6,'оновлювати і видаляти'),(7,'читати, оновлювати, видаляти'),(8,'немає доступу');
/*!40000 ALTER TABLE `roles_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_status`
--

DROP TABLE IF EXISTS `room_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_status`
--

LOCK TABLES `room_status` WRITE;
/*!40000 ALTER TABLE `room_status` DISABLE KEYS */;
INSERT INTO `room_status` VALUES (1,'вільно'),(2,'зарезервовано'),(3,'заселено'),(4,'потребує прибирання'),(5,'проводиться ремонт'),(6,'тимчасово недоступно');
/*!40000 ALTER TABLE `room_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `room_number` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `corps` int DEFAULT NULL,
  `price_per_night` decimal(10,0) NOT NULL,
  `beds_type` int NOT NULL,
  `beds_quantity` int DEFAULT NULL,
  `status` int NOT NULL,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `smoking` tinyint(1) NOT NULL,
  `balcony` tinyint(1) NOT NULL,
  `design_style` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `beds_type` (`beds_type`),
  KEY `status` (`status`),
  KEY `corps` (`corps`),
  CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`beds_type`) REFERENCES `bed_type` (`id`),
  CONSTRAINT `rooms_ibfk_2` FOREIGN KEY (`status`) REFERENCES `room_status` (`id`),
  CONSTRAINT `rooms_ibfk_3` FOREIGN KEY (`corps`) REFERENCES `corps` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'101a',1,200,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білі, сині та коричневі тона'),(2,'102b',2,320,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білі, сині та коричневі тона'),(3,'103с',3,295,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білі, сині та коричневі тона'),(4,'204d',4,420,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'кремові, коричневі та червоні відтінки'),(5,'105a',1,198,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'кремові, коричневі та червоні відтінки'),(6,'106b',2,315,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'кремові, коричневі та червоні відтінки'),(7,'107c',3,180,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'кремові, коричневі та червоні відтінки'),(8,'108d',4,410,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'білий та кремові тони'),(9,'109a',1,405,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білий та кремові тони'),(10,'110b',2,500,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'білий та кремові тони'),(11,'111c',3,520,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білий та кремові тони'),(12,'120d',4,270,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білий та кремові тони'),(13,'130a',1,395,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'білий та кремові тони'),(14,'140b',2,150,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білий та кремові тони'),(15,'150c',3,800,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,1,'білий та кремові тони'),(16,'160d',4,650,2,1,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',1,1,'білий та кремові тони'),(17,'170a',1,705,1,2,1,'Простора сонячна кімната вже чекає на Вас у нашому пансіонаті!',0,0,'білий та кремові тони');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sea_lines`
--

DROP TABLE IF EXISTS `sea_lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sea_lines` (
  `id` int NOT NULL AUTO_INCREMENT,
  `line` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sea_lines`
--

LOCK TABLES `sea_lines` WRITE;
/*!40000 ALTER TABLE `sea_lines` DISABLE KEYS */;
INSERT INTO `sea_lines` VALUES (1,'1 лінія'),(2,'2 лінія'),(3,'3 лінія');
/*!40000 ALTER TABLE `sea_lines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_users`
--

DROP TABLE IF EXISTS `site_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` int NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci NOT NULL,
  `id_employee` int DEFAULT NULL,
  `id_vacationer` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_vacationer` (`id_vacationer`),
  KEY `site_users_ibfk_1` (`role`),
  CONSTRAINT `site_users_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `site_users_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `team` (`id`),
  CONSTRAINT `site_users_ibfk_3` FOREIGN KEY (`id_vacationer`) REFERENCES `vacationers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_users`
--

LOCK TABLES `site_users` WRITE;
/*!40000 ALTER TABLE `site_users` DISABLE KEYS */;
INSERT INTO `site_users` VALUES (1,1,'admin@gmail.com','123456789',6,NULL),(2,2,'marichka@gmail.com','123456789',NULL,1),(6,2,'martin@gmail.com','123456789',NULL,2),(7,2,'david@gmail.com','123456789',NULL,3),(8,2,'calvin@gmail.com','123456789',NULL,4),(9,2,'christpher@gmail.com','123456789',NULL,5);
/*!40000 ALTER TABLE `site_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `team` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `gender` int DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `about_person` text CHARACTER SET utf8mb3 COLLATE utf8_general_ci,
  `job` varchar(255) NOT NULL,
  `publicly_showing` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `gender` (`gender`),
  CONSTRAINT `team_ibfk_1` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Антон','Мороз',2,'1992-02-02','380975862147','Успішно виконую обов\'язки директора пансіонату \"Штормове\". За час моєї роботи пансіонат значно підвищив свій рівень якості наданих послуг і обслуговування. Радий працювати на користь і задоволення людей, які вирішили відпочити у цьому пансіонаті!','директор',1),(2,'Віра','Яненко',1,'1975-08-01','380502477258','Стараюсь кожен день вкладати свою працю у розбудову та покращення пансіонату \"Штормове\".','зам. директора',1),(3,'Людмила','Ільченко',1,'1966-12-29','380975846231','Беру мотивацію із позитивних відгуків відпочивальників про пансіонат \"Штормове\" і роблю даний пансіонат ще кращим кожного дня!','зам. директора',1),(4,'Надія','Лук\'янчук',1,'1978-05-28','380954859226','Розробляю дизайн інтер\'єрів та екстер\'єрів. У свята кожного року придумую сучасний дизайн для підняття святкового настрою. Працюю для втіхи відпочивальників пансіонату!','дизайнер',1),(5,'Ярослава','Семенчук',1,'1993-08-03','380678515489','Розбудовую і зміцнюю внутрішню економіку пансіонату \"Штормове\". Стараюсь для кращого майбутнього.','економіст',1),(6,'Олена','Мачушник',1,'2002-02-15','380976309002','головний програміст','головний програміст',0);
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `description` text,
  `job` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Петро','Мельник','Дуже задоволений відпочинком у пансіонаті! Особливо сподобалось обслуговування, страви у їдальні та екскурсії. Рекомендую всім!','менеджер'),(2,'Надія','Іванчук','Все сподобалось, обов\'язково приїду ще наступного року. Обожнюю водні атракціони у пансіонаті \"Штормове\", вони найкращі!','вчителька'),(3,'Михайло','Лисенко','Чудовий сервіс, море прекрасне. Бар біля басейну - просто наупрекрасніше, що можна було зробити!','секретар'),(4,'Марія','Лук\'янчук','Вже 5-тий рік підряд їздимо відпочивати в пансіонат \"Штормове\". Усім задоволена!','директор'),(5,'Олексій','Бойчук','Відпочинок надзвичайно сподобався! Обов\'язково приїду наступного року теж!','медбрат'),(6,'Мар\'ян','Костюченко','Все просто чудово, особливо море! Рекомендую пансіонат \"Штормове\" всім і всюди! ','лікар'),(11,'Ольга','Скиба','У пансіонаті \"Штормове\" сподобалось все, було неймовірно! Рекомендую всім!','бухгалтер');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacationers`
--

DROP TABLE IF EXISTS `vacationers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacationers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `room` int DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8_general_ci DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `date_of_settlement` date DEFAULT NULL,
  `departure_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gender` (`gender`),
  CONSTRAINT `vacationers_ibfk_1` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacationers`
--

LOCK TABLES `vacationers` WRITE;
/*!40000 ALTER TABLE `vacationers` DISABLE KEYS */;
INSERT INTO `vacationers` VALUES (1,'Марічка','Ткаченко','2002-06-01',NULL,'380974821775',1,NULL,NULL),(2,'Мартін','Гарікс','1996-05-14',NULL,'380976308772',2,NULL,NULL),(3,'Девід','Гета','1967-11-07',NULL,'380507245812',1,NULL,NULL),(4,'Калвін','Гарріс','1984-01-17',NULL,'380675289461',2,NULL,NULL),(5,'Крістофер','Комсток','1992-05-19',NULL,'380975864337',2,NULL,NULL);
/*!40000 ALTER TABLE `vacationers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-18 12:06:01
