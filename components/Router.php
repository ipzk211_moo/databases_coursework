<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = 'config/routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        $uri = $this->getURI();

        foreach ($this->routes as $uriPattern => $path) {

            if (preg_match("~$uriPattern~", $uri)) {


                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segmentsTemp = str_split($internalRoute);

                $segments = [];
                $stringTemp = "";
                for($i=0;$i<count($segmentsTemp);$i++) {

                    if($segmentsTemp[$i] == '/') {
                        array_push($segments, $stringTemp);
                        $stringTemp = "";
                        continue;
                    }
                    else {
                        $stringTemp = $stringTemp . $segmentsTemp[$i];
                    }

                    if($i == count($segmentsTemp)-1) {
                        array_push($segments, $stringTemp);
                        $stringTemp = "";
                    }

                }

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);



                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                $controllerFile = 'controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }


                $controllerObject = new $controllerName;

                $errorsArray = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "?"];
                for($i=0; $i<count($errorsArray);$i++) {
                    $actionName = str_replace($errorsArray[$i] . 'site', '', $actionName);
                }

                $actionName = str_replace('?', '', $actionName);

                if (method_exists($controllerObject, $actionName)
                    && is_callable(array($controllerObject, $actionName)))
                {
                    $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                    if ($result != null) {
                        break;
                    }
                }
                else {
                    $result = call_user_func_array(array("SiteController", "actionIndex"));

                    if ($result != null) {
                        break;
                    }
                }
            }
        }
    }
}
