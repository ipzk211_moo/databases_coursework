<?php
abstract class AdminBase {
    public static function checkAdmin() {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        if($user['role'] != '2') {
            return true;
        }
        die("Доступ заборонений!");
    }

    public static function checkAdminTrueFalse() {
        $userId = User::checkLogged();
        $user = User::getUserById($userId);

        if(!$user) header("Location: /404");

        if($user['role'] != '2') {
            return true;
        }
        return false;
    }
}
?>