<?php

class UserController {
    public function actionRegister() {
        $footer_data = Site::getFooterData();

        $name = '';
        $surname = '';
        $gender = '';
        $birthday = '';
        $phone = '';
        $email = '';
        $password = '';
        $result = false;

        if(isset($_POST['submit'])) {
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $gender = $_POST['gender'];
            $birthday = $_POST['birthday'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;


            if(!User::checkName($name)) {
                $errors[] = 'Імя не повинно бути коротше 2-х символів';
            }

            if(!User::checkEmail($email)) {
                $errors[] = 'Неправильний email';
            }

            if(!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротше 6-ти символів';
            }

            if(User::checkEmailExist($email)) {
                $errors[] = 'Такий email вже використовується';
            }

            if($errors == false) {
                $result = User::register($name, $surname, $gender, $birthday, $phone, $email, $password);
            }

        }
        require_once('views/user/register.php');

        return true;
    }

    public function actionLogin() {
        $footer_data = Site::getFooterData();
        $email = '';
        $password = '';

        if(isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if(!User::checkEmail($email)) {
                $errors[] = 'Неправильний email';
            }
            if(!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротшим 6-ти символів';
            }

            $userId = User::checkUserData($email, $password);

            if($userId == false) {
                $errors[] = 'Неправильні дані для входу на сайт';
            }
            else {
                User::auth($userId);
                header("Location: /cabinet/");
            }
        }
        require_once('views/user/login.php');

        return true;
    }

    public function actionLogout() {
        unset($_SESSION["userLoggedId"]);
        header("Location: /");
    }
}



?>