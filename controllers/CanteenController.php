<?php

    class CanteenController {
        public function actionIndex($page = 1) {
            $page = preg_replace('/[^0-9]/i', ' ', $page);

            $footer_data = Site::getFooterData();

            $dishes_data = array();
            $dishes_data = Canteen::getCanteenList($page);

            $dishes_category_data = array();
            $dishes_category_data = Canteen::getCanteenCategoryList();

            $total = Canteen::getTotalDishes();

            $dish_min_price = Canteen::getMinPriceDishOfAll();
            $dish_max_price = Canteen::getMaxPriceDishOfAll();

            $pagination = new Pagination($total, $page, Canteen::SHOW_BY_DEFAULT, 'page-');

            require_once('views/canteen/index.php');
            return true;
        }

        public function actionView($dishId) {


            $dish = Canteen::getDishById($dishId);
            if(!$dish) {
                header("Location: /404");

            }
            else {
                $footer_data = Site::getFooterData();

                if (isset($_POST['submit'])) {
                    if(isset($_POST['stars']) && isset($_POST['review'])) {
                        $options['id_site_user'] = $_SESSION['userLoggedId'];
                        $options['id_dish'] = $dishId;
                        $options['stars'] = $_POST['stars'];
                        $options['review'] = $_POST['review'];
                        $options['date'] = date("y-m-d");

                        $errors = false;

                        if (!isset($options['stars']) || empty($options['stars'])) {
                            $errors[] = 'Заповніть поля';
                        }

                        if ($errors == false) {
                            $id = Reviews::createReviewDish($options);

                            header("Location: /canteen/".$dishId);
                        }
                    }
                    else if(isset($_POST['ordered_dish_quantity']) && isset($_POST['ordered_dish_date'])) {
                        $options['id_dish'] = $dishId;
                        $options['ordered_dish_quantity'] = $_POST['ordered_dish_quantity'];
                        $options['ordered_dish_date'] = $_POST['ordered_dish_date'];
                        $options['from_date'] = date("y-m-d h:m");
                        $options['id_site_user'] = $_SESSION['userLoggedId'];
                        if (!isset($options['ordered_dish_quantity']) || empty($options['ordered_dish_quantity'])) {
                            $errors[] = 'Заповніть поля';
                        }

                        if ($errors == false) {
                            $id = Canteen::orderDish($options);

                            header("Location: /canteen/".$dishId);
                        }
                    }

                }

                $dishes_category_data = array();
                $dishes_category_data = Canteen::getCanteenCategoryList();

                $other_dishes_in_category = Canteen::getDishesByCategory($dish['category']);

                $reviews = Reviews::getDishReviewsById($dishId);

                $site_users_data = User::getSiteUsersData();

                require_once('views/canteen/view.php');
            }

            return true;

        }

        public function actionDelete($id)
        {
            $userId = User::checkLogged();

            $dish = Canteen::getDishById($id);

            if(!$dish) header("Location: /404");

            $footer_data = Site::getFooterData();

            if (isset($_POST['submit'])) {

                Canteen::deleteDishByIdFromOrders($userId, $id);

                header("Location: /cabinet");
            }

            require_once('views/canteen/delete.php');
            return true;
        }


    }
?>


