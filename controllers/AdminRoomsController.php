<?php
class AdminRoomsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['rooms_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $rooms = Rooms::getAdminRoomsList();

        require_once('views/admin_rooms/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['rooms_table'] != 4 && $access['rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps_list = Corps::getAdminCorpsList();
        $beds_type_list = Rooms::getBedTypesList();
        $status_list = Rooms::getStatusList();

        if (isset($_POST['submit'])) {
            $options['room_number'] = $_POST['room_number'];
            $options['corps'] = $_POST['corps'];
            $options['price_per_night'] = $_POST['price_per_night'];
            $options['beds_type'] = $_POST['beds_type'];
            $options['beds_quantity'] = $_POST['beds_quantity'];
            $options['status'] = $_POST['status'];
            $options['description'] = $_POST['description'];
            $options['smoking'] = $_POST['smoking'];
            $options['balcony'] = $_POST['balcony'];
            $options['design_style'] = $_POST['design_style'];

            $errors = false;

            if (!isset($options['room_number']) || empty($options['room_number'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Rooms::createRoom($options);

                if ($id) {
                    $file_ary = $_FILES['userfile'];
                    $arr_temp = ['-main', '-1', '-2', '-3'];

                    for($m=0; $m<count($file_ary); $m++) {
                        if (is_uploaded_file($file_ary['tmp_name'][$m])) {
                            move_uploaded_file($file_ary['tmp_name'][$m], $_SERVER['DOCUMENT_ROOT'] . "/template/images/rooms/room-{$id}{$arr_temp[$m]}.jpg");
                        }
                    }
                }

                header("Location: /admin/rooms");
            }
        }

        require_once('views/admin_rooms/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['rooms_table'] != 4 && $access['rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps_list = Corps::getAdminCorpsList();
        $beds_type_list = Rooms::getBedTypesList();
        $status_list = Rooms::getStatusList();

        $room = Rooms::getRoomById($id);

        if(!$room) header("Location: /404");

        if (isset($_POST['submit'])) {
            $options['room_number'] = $_POST['room_number'];
            $options['corps'] = $_POST['corps'];
            $options['price_per_night'] = $_POST['price_per_night'];
            $options['beds_type'] = $_POST['beds_type'];
            $options['beds_quantity'] = $_POST['beds_quantity'];
            $options['status'] = $_POST['status'];
            $options['description'] = $_POST['description'];
            $options['smoking'] = $_POST['smoking'];
            $options['balcony'] = $_POST['balcony'];
            $options['design_style'] = $_POST['design_style'];

            if(Rooms::updateRoomById($id, $options)) {

                $file_ary = $_FILES['userfile'];
                $arr_temp = ['-main', '-1', '-2', '-3'];

                for($m=0; $m<count($file_ary); $m++) {
                    if (is_uploaded_file($file_ary['tmp_name'][$m])) {
                        move_uploaded_file($file_ary['tmp_name'][$m], $_SERVER['DOCUMENT_ROOT'] . "/template/images/rooms/room-{$id}{$arr_temp[$m]}.jpg");
                    }
                }
            }

            header("Location: /admin/rooms");
        }

        require_once('views/admin_rooms/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['rooms_table'] != 5 && $access['rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $room = Rooms::getRoomById($id);

        if(!$room) header("Location: /404");

        if(isset($_POST['submit'])) {
            Rooms::deleteRoomById($id);
            header("Location: /admin/rooms");
        }
        require_once('views/admin_rooms/delete.php');
        return true;
    }

}
?>