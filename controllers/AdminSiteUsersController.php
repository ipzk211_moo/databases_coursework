<?php
class AdminSiteUsersController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['site_users_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $site_users = User::getAllSiteUsersList();

        require_once('views/admin_site_users/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['site_users_table'] != 4 && $access['site_users_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $roles_list = User::getRolesList();
        $employees_list = User::getTeamList();
        $vacationers_list = User::getVacationersList();

        if (isset($_POST['submit'])) {
            $options['role'] = $_POST['role'];
            $options['email'] = $_POST['email'];
            $options['password'] = $_POST['password'];
            $options['id_employee'] = $_POST['id_employee'];
            $options['id_vacationer'] = $_POST['id_vacationer'];

            $errors = false;

            if (!isset($options['email']) || empty($options['email'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                if(is_numeric($_POST['id_employee'])) {
                    User::createSiteUserEmployee($options);
                }
                else if(is_numeric($_POST['id_vacationer'])) {
                    User::createSiteUserVacationer($options);
                }
                else {
                    User::createSiteUser($options);
                }

                header("Location: /admin/site_users");
            }
        }

        require_once('views/admin_site_users/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['site_users_table'] != 4 && $access['site_users_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $roles_list = User::getRolesList();
        $employees_list = User::getTeamList();
        $vacationers_list = User::getVacationersList();

        $site_user = User::getSiteUserById($id);

        if(!$site_user) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['role'] = $_POST['role'];
            $options['email'] = $_POST['email'];
            $options['password'] = $_POST['password'];
            $options['id_employee'] = $_POST['id_employee'];
            $options['id_vacationer'] = $_POST['id_vacationer'];


            if(is_numeric($_POST['id_employee'])) {
                User::updateSiteUserEmployeeById($id, $options);

            }
            else if(is_numeric($_POST['id_vacationer'])) {
                User::updateSiteUserVacationerById($id, $options);
            }
            else {
                User::updateSiteUserById($id, $options);
            }

            header("Location: /admin/site_users");
        }
        require_once('views/admin_site_users/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['site_users_table'] != 5 && $access['site_users_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $site_user = User::getSiteUserById($id);

        if(!$site_user) header("Location: /404");

        if(isset($_POST['submit'])) {
            User::deleteSiteUserById($id);
            header("Location: /admin/site_users");
        }
        require_once('views/admin_site_users/delete.php');
        return true;
    }



}
?>