<?php

    class RoomsController {
        public function actionIndex($page = 1) {

            $page = preg_replace('/[^0-9]/i', ' ', $page);

            $footer_data = Site::getFooterData();

            $rooms_data = array();
            $rooms_data = Rooms::getRoomsList($page);

            $bed_types = array();
            $bed_types = Rooms::getBedTypesList();

            $total = Rooms::getTotalRooms();

            $rooms_corps = Corps::getCorpsList();

            $rooms_min_price = Rooms::getMinPriceRoomOfAll();
            $rooms_max_price = Rooms::getMaxPriceRoomOfAll();

            $pagination = new Pagination($total, $page, Rooms::SHOW_BY_DEFAULT, 'page-');

            require_once('views/rooms/index.php');
            return true;
        }

        public function actionView($roomId) {
            $room = Rooms::getRoomById($roomId);

            if(!$room) {
                header("Location: /404");
            }
            else {
                $footer_data = Site::getFooterData();

                $corps_categories = Corps::getCorpsList();

                $rooms_beds_type = Rooms::getBedTypesList();
                $room_exemp = new Rooms();
                $all_rooms_in_corp = $room_exemp->getRoomsListByCorp($room['corps']);

                $reviews = Reviews::getRoomReviewsById($roomId);

                $site_users_data = User::getSiteUsersData();
                $room_exs = new Rooms();
                $room_booking_info = $room_exs->getBookedRoomInfoById($roomId);

                if (isset($_POST['submit'])) {
                    if(isset($_POST['stars']) && isset($_POST['review'])) {
                        $options['id_site_user'] = $_SESSION['userLoggedId'];
                        $options['id_room'] = $roomId;
                        $options['stars'] = $_POST['stars'];
                        $options['review'] = $_POST['review'];
                        $options['date'] = date("y-m-d");

                        $errors = false;

                        if (!isset($options['stars']) || empty($options['stars'])) {
                            $errors[] = 'Заповніть поля';
                        }

                        if ($errors == false) {
                            $id = Reviews::createReviewRoom($options);

                            header("Location: /rooms/".$roomId);
                        }
                    }
                    else if(isset($_POST['date_move_in']) && isset($_POST['date_move_out'])) {
                        $options['id_room'] = $roomId;
                        $options['move_in'] = $_POST['date_move_in'];
                        $options['move_out'] = $_POST['date_move_out'];
                        $options['id_site_user'] = $_SESSION['userLoggedId'];

                        if (!isset($options['move_in']) || empty($options['move_out'])) {
                            $errors[] = 'Заповніть поля';
                        }

                        for($j=0; $j<count($room_booking_info);$j++) {
                            if($room_booking_info[$j]['move_in'] == $options['move_in']) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }

                            if($room_booking_info[$j]['move_out'] == $options['move_out']) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }

                            if(($options['move_in'] >= $room_booking_info[$j]['move_in']) &&  ($options['move_in'] <= $room_booking_info[$j]['move_out'])) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }

                            if(($room_booking_info[$j]['move_in'] >= $options['move_in']) &&  ($room_booking_info[$j]['move_in'] <= $options['move_out'])) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }

                            if(($options['move_out'] >= $room_booking_info[$j]['move_in']) &&  ($options['move_out'] <= $room_booking_info[$j]['move_out'])) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }

                            if(($room_booking_info[$j]['move_out'] >= $options['move_out']) &&  ($room_booking_info[$j]['move_out'] <= $options['move_out'])) {
                                $errors[] = 'Неможливо забронювати на дану дату';
                            }
                        }

                        if ($errors == false) {

                            $id = Rooms::bookRoom($options);

                            header("Location: /rooms/".$roomId);
                        }
                        else if($errors == true) {
                            echo '<script>alert("Помилка бронювання!")</script>';
                        }
                    }
                }

                require_once('views/rooms/view.php');

            }

            return true;

        }

        public function actionDelete($id)
        {
            $userId = User::checkLogged();

            $room = Rooms::getRoomById($id);

            if(!$room) header("Location: /404");

            $footer_data = Site::getFooterData();

            if (isset($_POST['submit'])) {

                Rooms::deleteRoomByIdFromBooking($userId, $id);

                header("Location: /cabinet");
            }

            require_once('views/rooms/delete.php');
            return true;
        }


    }
?>


