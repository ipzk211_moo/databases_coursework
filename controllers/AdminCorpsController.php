<?php
class AdminCorpsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['corps_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps = Corps::getAdminCorpsList();

        require_once('views/admin_corps/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['corps_table'] != 4 && $access['corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $sea_lines = Corps::getSeaLines();
        $categories = Corps::getCorpsCategories();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['address'] = $_POST['address'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_popular'] = $_POST['is_popular'];
            $options['sea_line'] = $_POST['sea_line'];
            $options['category'] = $_POST['category'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {
                $id = Corps::createCorp($options);

                if ($id) {
                    $file_ary = $_FILES['userfile'];
                    $arr_temp = ['', '-2', '-3', '-4'];

                    for($m=0; $m<count($file_ary); $m++) {
                        if (is_uploaded_file($file_ary['tmp_name'][$m])) {
                            move_uploaded_file($file_ary['tmp_name'][$m], $_SERVER['DOCUMENT_ROOT'] . "/template/images/corps/{$id}{$arr_temp[$m]}.jpg");
                        }
                    }
                }

                header("Location: /admin/corps");
            }
        }

        require_once('views/admin_corps/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['corps_table'] != 4 && $access['corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $sea_lines = Corps::getSeaLines();
        $categories = Corps::getCorpsCategories();

        $corp = Corps::getCorpById($id);

        if(!$corp) header("Location: /404");

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['address'] = $_POST['address'];
            $options['description'] = $_POST['description'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_popular'] = $_POST['is_popular'];
            $options['sea_line'] = $_POST['sea_line'];
            $options['category'] = $_POST['category'];

            if(Corps::updateCorpById($id, $options)) {

                $file_ary = $_FILES['userfile'];
                $arr_temp = ['', '-2', '-3', '-4'];

                for($m=0; $m<count($file_ary); $m++) {
                    if (is_uploaded_file($file_ary['tmp_name'][$m])) {
                        move_uploaded_file($file_ary['tmp_name'][$m], $_SERVER['DOCUMENT_ROOT'] . "/template/images/corps/{$id}{$arr_temp[$m]}.jpg");

                    }
                }
            }

            header("Location: /admin/corps");

        }

        require_once('views/admin_corps/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['corps_table'] != 5 && $access['corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corp = Corps::getCorpById($id);

        if(!$corp) header("Location: /404");

        if(isset($_POST['submit'])) {
            Corps::deleteCorpById($id);
            header("Location: /admin/corps");
        }
        require_once('views/admin_corps/delete.php');
        return true;
    }



}
?>