<?php
class CabinetController extends AdminBase {
    public function actionIndex() {
        $footer_data = Site::getFooterData();

        $isAdmin = self::checkAdminTrueFalse();

        $userId = User::checkLogged();

        $site_user = User::getUserById($userId);

        $user_extended_data = '';
        $role_name = '';
        $user = new User();

        if($site_user['id_vacationer'] != NULL) {
            $user_extended_data = $user->getVacationerDataById($site_user['id_vacationer']);
            $role_name = "відпочивальник";
        }
        else if($site_user['id_employee'] != NULL) {
            $user_extended_data = $user->getEmployeeDataById($site_user['id_employee']);
            $role_name = "працівник";
        }

        $ordered_dishes = Canteen::getOrderedDishesByUserId($userId);
        $booked_rooms = Rooms::getBookedRoomsByUserId($userId);

        if(!$site_user) header("Location: /404");

        require_once('views/cabinet/index.php');

        return true;
    }

    public function actionEdit() {


        $userId = User::checkLogged();

        $site_user = User::getUserById($userId);

        if(!$site_user) header("Location: /404");

        $footer_data = Site::getFooterData();

        $user = new User();
        $temp = 0;
        if($site_user['id_vacationer'] != NULL) {
            $user_extended_data = $user->getVacationerDataById($site_user['id_vacationer']);
            $temp = 1;
        }
        else if($site_user['id_employee'] != NULL) {
            $user_extended_data = $user->getEmployeeDataById($site_user['id_employee']);
            $temp = 2;
        }

        $id = $user_extended_data['id'];
        $name = $user_extended_data['name'];
        $surname = $user_extended_data['surname'];
        $phone = $user_extended_data['phone'];
        $email = $site_user['email'];
        $password = $site_user['password'];

        $result = false;
        if(isset($_POST['submit'])) {
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = false;

            if(!User::checkName($name)) {
                $errors[] = "Ім'я не повинно бути коротшим за 2 символа";
            }

            if(!User::checkPassword($password)) {
                $errors[] = 'Пароль не повинен бути коротшим за 6 символів';
            }

            if($errors == false) {
                $result = User::edit($temp, $userId, $id, $name, $surname, $phone, $email, $password);
            }
        }

        require_once('views/cabinet/edit.php');

        return true;
    }
}
?>