<?php
class AdminCorpsCategoryController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['corps_category_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps_categories = Corps::getCorpsCategories();

        require_once('views/admin_corps_category/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['corps_category_table'] != 4 && $access['corps_category_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        if (isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            $errors = false;

            if (!isset($options['category']) || empty($options['category'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Corps::createCorpsCategory($options);

                header("Location: /admin/corps_category");
            }
        }

        require_once('views/admin_corps_category/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['corps_category_table'] != 4 && $access['corps_category_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Corps::getCorpCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            Corps::updateCorpCategoryById($id, $options);

            header("Location: /admin/corps_category");
        }
        require_once('views/admin_corps_category/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['corps_category_table'] != 5 && $access['corps_category_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Corps::getCorpCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            Corps::deleteCorpCategoryById($id);
            header("Location: /admin/corps_category");
        }
        require_once('views/admin_corps_category/delete.php');
        return true;
    }


}
?>