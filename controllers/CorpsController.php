<?php

    class CorpsController {
        public function actionIndex($page = 1) {

            $page = preg_replace('/[^0-9]/i', ' ', $page);

            $footer_data = Site::getFooterData();

            $corps_data = array();
            $corps_data = Corps::getCorpsList($page);

            $sea_lines = Corps::getSeaLines();

            $corps_categories = Corps::getCorpsCategories();

            $total = Corps::getTotalCorps();

            $pagination = new Pagination($total, $page, Corps::SHOW_BY_DEFAULT, 'page-');

            require_once('views/corps/index.php');
            return true;
        }

        public function actionView($corpId) {
//            $room = Rooms::getRoomById($roomId);
            $corp = Corps::getCorpById($corpId);

            if(!$corp) {
                header("Location: /404");
            }
            else {
                $footer_data = Site::getFooterData();

                if (isset($_POST['submit'])) {
                    $options['id_site_user'] = $_SESSION['userLoggedId'];
                    $options['id_corp'] = $corpId;
                    $options['stars'] = $_POST['stars'];
                    $options['review'] = $_POST['review'];
                    $options['date'] = date("y-m-d");

                    $errors = false;

                    if (!isset($options['stars']) || empty($options['stars'])) {
                        $errors[] = 'Заповніть поля';
                    }

                    if ($errors == false) {

                        $id = Reviews::createReviewCorp($options);

                        header("Location: /corps/".$corpId);
                    }
                }

                $corp = Corps::getCorpById($corpId);
                $corps_categories = Corps::getCorpsCategories();
                $corps_sea_lines = Corps::getSeaLines();

                $room = new Rooms();
                $rooms_data = $room->getRoomsListByCorp($corpId);

                $reviews = Reviews::getCorpReviewsById($corpId);

                $site_users_data = User::getSiteUsersData();

                if(!$corp) header("Location: /404");

                require_once('views/corps/view.php');

            }

            return true;
        }


    }
?>


