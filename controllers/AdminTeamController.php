<?php
class AdminTeamController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['team_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $team_members = User::getTeamList();

        require_once('views/admin_team/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['team_table'] != 4 && $access['team_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $gender_list = User::getGenderList();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['gender'] = $_POST['gender'];
            $options['birthday'] = $_POST['birthday'];

            $options['phone'] = $_POST['phone'];
            $options['about_person'] = $_POST['about_person'];
            $options['job'] = $_POST['job'];
            $options['publicly_showing'] = $_POST['publicly_showing'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = User::createTeamUser($options);

                if ($id) {

                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/team/{$id}.jpg");
                    }
                };

                header("Location: /admin/team");
            }
        }

        require_once('views/admin_team/create.php');
        return true;
    }


    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['team_table'] != 4 && $access['team_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $gender_list = User::getGenderList();

        $member = User::getTeamMemberById($id);

        if(!$member) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['gender'] = $_POST['gender'];
            $options['birthday'] = $_POST['birthday'];

            $options['phone'] = $_POST['phone'];
            $options['about_person'] = $_POST['about_person'];
            $options['job'] = $_POST['job'];
            $options['publicly_showing'] = $_POST['publicly_showing'];

            if(User::updateTeamUserById($id, $options)) {

                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/team/{$id}.jpg");
                }
            }
            header("Location: /admin/team");
        }
        require_once('views/admin_team/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['team_table'] != 5 && $access['team_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $member = User::getTeamMemberById($id);

        if(!$member) header("Location: /404");

        if(isset($_POST['submit'])) {
            User::deleteTeamUserById($id);
            header("Location: /admin/team");
        }
        require_once('views/admin_team/delete.php');
        return true;
    }


}
?>