<?php
class AdminRolesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['roles_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $roles = User::getRolesList();

        require_once('views/admin_roles/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['roles_table'] != 4 && $access['roles_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $actions = User::getRolesActionsList();


        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['team_table'] = $_POST['team_table'];
            $options['canteen_table'] = $_POST['canteen_table'];
            $options['gallery_photos_table'] = $_POST['gallery_photos_table'];
            $options['corps_table'] = $_POST['corps_table'];
            $options['rooms_table'] = $_POST['rooms_table'];
            $options['vacationers_table'] = $_POST['vacationers_table'];
            $options['testimonials_table'] = $_POST['testimonials_table'];
            $options['site_users_table'] = $_POST['site_users_table'];
            $options['sea_lines_table'] = $_POST['sea_lines_table'];
            $options['roles_table'] = $_POST['roles_table'];
            $options['reviews_rooms_table'] = $_POST['reviews_rooms_table'];
            $options['reviews_corps_table'] = $_POST['reviews_corps_table'];
            $options['reviews_dishes_table'] = $_POST['reviews_dishes_table'];
            $options['ordered_dishes_table'] = $_POST['ordered_dishes_table'];
            $options['booked_rooms_table'] = $_POST['booked_rooms_table'];
            $options['gallery_photos_categories_table'] = $_POST['gallery_photos_categories_table'];
            $options['corps_category_table'] = $_POST['corps_category_table'];
            $options['canteen_categories_table'] = $_POST['canteen_categories_table'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                User::createRole($options);

                header("Location: /admin/roles");
            }
        }

        require_once('views/admin_roles/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['roles_table'] != 4 && $access['roles_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $actions = User::getRolesActionsList();

        $role = User::getRoleById($id);

        if(!$role) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['team_table'] = $_POST['team_table'];
            $options['canteen_table'] = $_POST['canteen_table'];
            $options['gallery_photos_table'] = $_POST['gallery_photos_table'];
            $options['corps_table'] = $_POST['corps_table'];
            $options['rooms_table'] = $_POST['rooms_table'];
            $options['vacationers_table'] = $_POST['vacationers_table'];
            $options['testimonials_table'] = $_POST['testimonials_table'];
            $options['site_users_table'] = $_POST['site_users_table'];
            $options['sea_lines_table'] = $_POST['sea_lines_table'];
            $options['roles_table'] = $_POST['roles_table'];
            $options['reviews_rooms_table'] = $_POST['reviews_rooms_table'];
            $options['reviews_corps_table'] = $_POST['reviews_corps_table'];
            $options['reviews_dishes_table'] = $_POST['reviews_dishes_table'];
            $options['ordered_dishes_table'] = $_POST['ordered_dishes_table'];
            $options['booked_rooms_table'] = $_POST['booked_rooms_table'];
            $options['gallery_photos_categories_table'] = $_POST['gallery_photos_categories_table'];
            $options['corps_category_table'] = $_POST['corps_category_table'];
            $options['canteen_categories_table'] = $_POST['canteen_categories_table'];

            User::updateRoleById($id, $options);

            header("Location: /admin/roles");
        }
        require_once('views/admin_roles/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['roles_table'] != 5 && $access['roles_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $role = User::getRoleById($id);

        if(!$role) header("Location: /404");

        if(isset($_POST['submit'])) {
            User::deleteRoleById($id);
            header("Location: /admin/roles");
        }
        require_once('views/admin_roles/delete.php');
        return true;
    }

}
?>