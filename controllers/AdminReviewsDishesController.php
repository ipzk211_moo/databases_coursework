<?php
class AdminReviewsDishesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_dishes_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $reviews = Reviews::getDishReviews();

        require_once('views/admin_reviews_dishes/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_dishes_table'] != 4 && $access['reviews_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $dishes_list = Canteen::getAdminCanteenList();
        $site_users_list = User::getAllSiteUsersList();

        if (isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_dish'] = $_POST['id_dish'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            $errors = false;

            if (!isset($options['stars']) || empty($options['stars'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Reviews::createReviewDish($options);

                header("Location: /admin/reviews_dishes");
            }
        }

        require_once('views/admin_reviews_dishes/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_dishes_table'] != 4 && $access['reviews_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $dishes_list = Canteen::getAdminCanteenList();
        $site_users_list = User::getAllSiteUsersList();

        $review = Reviews::getDishReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_dish'] = $_POST['id_dish'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            Reviews::updateDishReviewById($id, $options);


            header("Location: /admin/reviews_dishes");
        }
        require_once('views/admin_reviews_dishes/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_dishes_table'] != 5 && $access['reviews_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $review = Reviews::getDishReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            Reviews::deleteDishReviewById($id);
            header("Location: /admin/reviews_dishes");
        }
        require_once('views/admin_reviews_dishes/delete.php');
        return true;
    }

}
?>