<?php
class AdminTestimonialsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['testimonials_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $testimonials = Site::getTestimonials();

        require_once('views/admin_testimonials/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['testimonials_table'] != 4 && $access['testimonials_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['description'] = $_POST['description'];
            $options['job'] = $_POST['job'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Site::createTestimonial($options);

                header("Location: /admin/testimonials");
            }
        }

        require_once('views/admin_testimonials/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['testimonials_table'] != 4 && $access['testimonials_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $testimonial = Site::getTestimonialById($id);

        if(!$testimonial) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['description'] = $_POST['description'];
            $options['job'] = $_POST['job'];

            Site::updateTestimonialById($id, $options);

            header("Location: /admin/testimonials");
        }
        require_once('views/admin_testimonials/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['testimonials_table'] != 5 && $access['testimonials_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $testimonial = Site::getTestimonialById($id);

        if(!$testimonial) header("Location: /404");

        if(isset($_POST['submit'])) {
            Site::deleteTestimonialById($id);
            header("Location: /admin/testimonials");
        }
        require_once('views/admin_testimonials/delete.php');
        return true;
    }

}
?>