<?php
class AdminOrderedDishesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['ordered_dishes_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $ordered_dishes = Canteen::getOrderedDishes();

        require_once('views/admin_ordered_dishes/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['ordered_dishes_table'] != 4 && $access['ordered_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $dishes_list = Canteen::getAdminCanteenList();
        $site_users_list = User::getAllSiteUsersList();

        if (isset($_POST['submit'])) {
            $options['id_dish'] = $_POST['id_dish'];
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['quantity'] = $_POST['quantity'];
            $options['from_date'] = $_POST['from_date'];
            $options['to_date'] = $_POST['to_date'];

            $errors = false;

            if (!isset($options['quantity']) || empty($options['quantity'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Canteen::createDishOrder($options);

                header("Location: /admin/ordered_dishes");
            }
        }

        require_once('views/admin_ordered_dishes/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['ordered_dishes_table'] != 4 && $access['ordered_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $dishes_list = Canteen::getAdminCanteenList();
        $site_users_list = User::getAllSiteUsersList();

        $order = Canteen::getDishOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['id_dish'] = $_POST['id_dish'];
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['quantity'] = $_POST['quantity'];
            $options['from_date'] = $_POST['from_date'];
            $options['to_date'] = $_POST['to_date'];

            Canteen::updateDishOrderById($id, $options);

            header("Location: /admin/ordered_dishes");
        }
        require_once('views/admin_ordered_dishes/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['ordered_dishes_table'] != 5 && $access['ordered_dishes_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $order = Canteen::getDishOrderById($id);

        if(!$order) header("Location: /404");

        if(isset($_POST['submit'])) {
            Canteen::deleteDishOrderById($id);
            header("Location: /admin/ordered_dishes");
        }
        require_once('views/admin_ordered_dishes/delete.php');
        return true;
    }




}
?>