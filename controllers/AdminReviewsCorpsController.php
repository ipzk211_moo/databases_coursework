<?php
class AdminReviewsCorpsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_corps_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $reviews = Reviews::getCorpReviews();

        require_once('views/admin_reviews_corps/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_corps_table'] != 4 && $access['reviews_corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps_list = Corps::getAdminCorpsList();
        $site_users_list = User::getAllSiteUsersList();

        if (isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_corp'] = $_POST['id_corp'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            $errors = false;

            if (!isset($options['stars']) || empty($options['stars'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Reviews::createReviewCorp($options);

                header("Location: /admin/reviews_corps");
            }
        }

        require_once('views/admin_reviews_corps/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_corps_table'] != 4 && $access['reviews_corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $corps_list = Corps::getAdminCorpsList();
        $site_users_list = User::getAllSiteUsersList();

        $review = Reviews::getCorpReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_corp'] = $_POST['id_corp'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            Reviews::updateCorpReviewById($id, $options);

            header("Location: /admin/reviews_corps");
        }
        require_once('views/admin_reviews_corps/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_corps_table'] != 5 && $access['reviews_corps_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $review = Reviews::getCorpReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            Reviews::deleteCorpReviewById($id);
            header("Location: /admin/reviews_corps");
        }
        require_once('views/admin_reviews_corps/delete.php');
        return true;
    }

}
?>