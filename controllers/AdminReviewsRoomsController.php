<?php
class AdminReviewsRoomsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_rooms_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $reviews = Reviews::getRoomReviews();

        require_once('views/admin_reviews_rooms/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['reviews_rooms_table'] != 4 && $access['reviews_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $rooms_list = Rooms::getAdminRoomsList();
        $site_users_list = User::getAllSiteUsersList();

        if (isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_room'] = $_POST['id_room'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            $errors = false;

            if (!isset($options['stars']) || empty($options['stars'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Reviews::createReviewRoom($options);

                header("Location: /admin/reviews_rooms");
            }
        }

        require_once('views/admin_reviews_rooms/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_rooms_table'] != 4 && $access['reviews_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $rooms_list = Rooms::getAdminRoomsList();
        $site_users_list = User::getAllSiteUsersList();

        $review = Reviews::getRoomReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['id_room'] = $_POST['id_room'];
            $options['stars'] = $_POST['stars'];
            $options['review'] = $_POST['review'];
            $options['date'] = $_POST['date'];
            $options['is_showing'] = $_POST['is_showing'];

            Reviews::updateRoomReviewById($id, $options);


            header("Location: /admin/reviews_rooms");
        }
        require_once('views/admin_reviews_rooms/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['reviews_rooms_table'] != 5 && $access['reviews_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $review = Reviews::getRoomReviewById($id);

        if(!$review) header("Location: /404");

        if(isset($_POST['submit'])) {
            Reviews::deleteRoomReviewById($id);
            header("Location: /admin/reviews_rooms");
        }
        require_once('views/admin_reviews_rooms/delete.php');
        return true;
    }

}
?>