<?php
class AdminController extends AdminBase {
    public function actionIndex() {

        self::checkAdmin();

        $footer_data = Site::getFooterData();
        $userId = User::checkLogged();

        $user_accesses = User::getTableAccessesList($userId);

        require_once('views/admin/index.php');
        return true;
    }
}
?>