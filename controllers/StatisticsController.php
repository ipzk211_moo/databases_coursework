<?php

    class StatisticsController extends AdminBase {
        public function actionIndex() {
            self::checkAdmin();

            $footer_data = Site::getFooterData();

            $rating_popular_dishes = Canteen::getListPopularDishes();
            $rating_popular_dishes_category = Canteen::getListPopularDishesByCategory();
            $rating_popular_rooms = Rooms::getListPopularRooms();

            require_once('views/statistics/index.php');
            return true;
        }


    }
?>


