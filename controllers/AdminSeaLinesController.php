<?php
class AdminSeaLinesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['sea_lines_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $sea_lines = Corps::getSeaLines();

        require_once('views/admin_sea_lines/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['sea_lines_table'] != 4 && $access['sea_lines_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        if (isset($_POST['submit'])) {
            $options['line'] = $_POST['line'];

            $errors = false;

            if (!isset($options['line']) || empty($options['line'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Corps::createSeaLine($options);

                header("Location: /admin/sea_lines");
            }
        }

        require_once('views/admin_sea_lines/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['sea_lines_table'] != 4 && $access['sea_lines_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $sea_line = Corps::getSeaLineById($id);

        if(!$sea_line) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['line'] = $_POST['line'];

            Corps::updateSeaLineById($id, $options);

            header("Location: /admin/sea_lines");
        }
        require_once('views/admin_sea_lines/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['sea_lines_table'] != 5 && $access['sea_lines_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $sea_line = Corps::getSeaLineById($id);

        if(!$sea_line) header("Location: /404");

        if(isset($_POST['submit'])) {
            Corps::deleteSeaLineById($id);
            header("Location: /admin/sea_lines");
        }
        require_once('views/admin_sea_lines/delete.php');
        return true;
    }


}
?>