<?php
class AdminGalleryPhotosController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['gallery_photos_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $photos = Gallery::getAdminPhotosData();

        require_once('views/admin_gallery_photos/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['gallery_photos_table'] != 4 && $access['gallery_photos_table'] != 7){
            header("Location: /404");
        }

        $categories_list = Gallery::getPhotoCategories();

        if (isset($_POST['submit'])) {
            $options['title'] = $_POST['title'];
            $options['subtitle'] = $_POST['subtitle'];
            $options['alt_attribute'] = $_POST['alt_attribute'];
            $options['category'] = $_POST['category'];
            $options['is_showing'] = $_POST['is_showing'];

            $errors = false;

            if (!isset($options['title']) || empty($options['title'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {
                $id = Gallery::createGalleryPhoto($options);

                if ($id) {
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/gallery_large/gallery-{$id}.jpg");
                    }
                }

                header("Location: /admin/gallery_photos");
            }
        }

        require_once('views/admin_gallery_photos/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['gallery_photos_table'] != 4 && $access['gallery_photos_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $categories_list = Gallery::getPhotoCategories();

        $photo = Gallery::GetPhotoById($id);

        if(!$photo) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['title'] = $_POST['title'];
            $options['subtitle'] = $_POST['subtitle'];
            $options['alt_attribute'] = $_POST['alt_attribute'];
            $options['category'] = $_POST['category'];
            $options['is_showing'] = $_POST['is_showing'];

            if(Gallery::updatePhotoById($id, $options)) {
                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/gallery_large/gallery-{$id}.jpg");
                }
            }
            header("Location: /admin/gallery_photos");
        }
        require_once('views/admin_gallery_photos/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['gallery_photos_table'] != 5 && $access['gallery_photos_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $photo = Gallery::GetPhotoById($id);

        if(!$photo) header("Location: /404");

        if(isset($_POST['submit'])) {
            Gallery::deletePhotoById($id);
            header("Location: /admin/gallery_photos");
        }
        require_once('views/admin_gallery_photos/delete.php');
        return true;
    }

}
?>