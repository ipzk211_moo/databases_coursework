<?php

    class GalleryController {
        public function actionIndex($page = 1) {

            $page = preg_replace('/[^0-9]/i', ' ', $page);

            $footer_data = Site::getFooterData();

            $photos_data = array();
            $photos_data = Gallery::getPhotosData($page);

            $photo_categories = array();
            $photo_categories = Gallery::getPhotoCategories();

            $total = Gallery::getTotalPhotos();

            $pagination = new Pagination($total, $page, Gallery::SHOW_BY_DEFAULT, 'page-');

            require_once('views/gallery/index.php');
            return true;
        }


    }
?>


