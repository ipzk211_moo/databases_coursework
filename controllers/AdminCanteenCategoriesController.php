<?php
class AdminCanteenCategoriesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['canteen_categories_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $canteen_categories = Canteen::getCanteenCategoryList();

        require_once('views/admin_canteen_categories/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['canteen_categories_table'] != 4 && $access['canteen_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        if (isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            $errors = false;

            if (!isset($options['category']) || empty($options['category'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Canteen::createCanteenCategory($options);

                header("Location: /admin/canteen_categories");
            }
        }

        require_once('views/admin_canteen_categories/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['canteen_categories_table'] != 4 && $access['canteen_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Canteen::getCanteenCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            Canteen::updateCanteenCategoryById($id, $options);

            header("Location: /admin/canteen_categories");
        }
        require_once('views/admin_canteen_categories/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['canteen_categories_table'] != 5 && $access['canteen_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Canteen::getCanteenCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            Canteen::deleteCanteenCategoryById($id);
            header("Location: /admin/canteen_categories");
        }
        require_once('views/admin_canteen_categories/delete.php');
        return true;
    }


}
?>