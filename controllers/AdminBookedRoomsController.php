<?php
class AdminBookedRoomsController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['booked_rooms_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $booked_rooms = Rooms::getBookedRoomsList();

        require_once('views/admin_booked_rooms/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['booked_rooms_table'] != 4 && $access['booked_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $rooms_list = Rooms::getAdminRoomsList();
        $site_users_list = User::getAllSiteUsersList();

        if (isset($_POST['submit'])) {
            $options['id_room'] = $_POST['id_room'];
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['move_in'] = $_POST['move_in'];
            $options['move_out'] = $_POST['move_out'];

            $errors = false;

            if (!isset($options['move_in']) || empty($options['move_in'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Rooms::bookRoom($options);

                header("Location: /admin/booked_rooms");
            }
        }

        require_once('views/admin_booked_rooms/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['booked_rooms_table'] != 4 && $access['booked_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $rooms_list = Rooms::getAdminRoomsList();
        $site_users_list = User::getAllSiteUsersList();

        $booked_room = Rooms::getBookedRoomById($id);

        if(!$booked_room) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['id_room'] = $_POST['id_room'];
            $options['id_site_user'] = $_POST['id_site_user'];
            $options['move_in'] = $_POST['move_in'];
            $options['move_out'] = $_POST['move_out'];

            Rooms::updateBookedRoomById($id, $options);

            header("Location: /admin/booked_rooms");
        }
        require_once('views/admin_booked_rooms/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['booked_rooms_table'] != 5 && $access['booked_rooms_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $booked_room = Rooms::getBookedRoomById($id);

        if(!$booked_room) header("Location: /404");

        if(isset($_POST['submit'])) {
            Rooms::deleteBookedRoomById($id);
            header("Location: /admin/booked_rooms");
        }
        require_once('views/admin_booked_rooms/delete.php');
        return true;
    }


}
?>