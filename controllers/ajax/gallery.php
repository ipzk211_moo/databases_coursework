
<?php

include_once("../../config/db_params.php");
include_once("../../components/Db.php");


if(isset($_POST["action"]))
{
    $dsn = "mysql:host=localhost;dbname=shtormove_db";
    $db = new PDO($dsn, 'root', 'root');
    $db->exec("set names utf8");

    if(isset($_POST["category"]))
    {

        $category_filter = implode("','", $_POST["category"]);

        $result = $db->query("SELECT * FROM gallery_photos WHERE category IN('".$category_filter."')");

        $output = '';
        $i = 0;
        while ($row = $result->fetch()) {

            $i++;

            $output .= '
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="product-box position-relative mb-40 middle-view">
                                <div class="product-box-wrapper">
                                    <div class="product-img">
                                        <img src="/template/images/gallery_large/gallery-'.$row['id'].'.jpg" class="w-100" alt="'.$row['alt_attribute'].'">
                                       
                                    </div>
    
                                    <div class="product-desc">
                                        <div class="product-desc-top">
                                            <div class="categories">
                                                <a href="javascript:void(0)" class="product-category"><span>'.$row['subtitle'].'</span></a>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="product-title eright-turquoise-color-hover">'.$row['title'].'</a>
    
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
        }
    } else {
        $result = $db->query("SELECT * FROM gallery_photos");

        $output = '';
        $i = 0;
        while ($row = $result->fetch()) {

            $i++;

            $output .= '
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="product-box position-relative mb-40 middle-view">
                                <div class="product-box-wrapper">
                                    <div class="product-img">
                                        <img src="/template/images/gallery_large/gallery-' . $row['id'] . '.jpg" class="w-100" alt="' . $row['alt_attribute'] . '">
                                       
                                    </div>
    
                                    <div class="product-desc">
                                        <div class="product-desc-top">
                                            <div class="categories">
                                                <a href="javascript:void(0)" class="product-category"><span>' . $row['subtitle'] . '</span></a>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="product-title eright-turquoise-color-hover">' . $row['title'] . '</a>
    
                                    </div>
                                </div>
                            </div>
                        </div>
                        ';
            }

        }
        echo $output;

}

?>