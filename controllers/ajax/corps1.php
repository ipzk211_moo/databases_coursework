
<?php

include_once("../../config/db_params.php");
include_once("../../components/Db.php");


if(isset($_POST["action"]))
{

    $dsn = "mysql:host=localhost;dbname=shtormove_db";
    $db = new PDO($dsn, 'root', 'root');
    $db->exec("set names utf8");

    if(isset($_POST["category"]))
    {
        $category_filter = implode("','", $_POST["category"]);

        $result = $db->query("SELECT * FROM corps WHERE sea_line IN('".$category_filter."')");
    }
    else {
        $result = $db->query("SELECT * FROM corps");
    }
    $max_prices_corps_raw = $db->query("SELECT corps.id, MAX(price_per_night) as max_price FROM rooms INNER JOIN corps ON rooms.corps = corps.id GROUP BY corps.id");

    $l = 0;
    while ($row = $max_prices_corps_raw->fetch()) {
        $max_prices_corps[$l]['id'] = $row['id'];
        $max_prices_corps[$l]['max_price'] = $row['max_price'];

        $l++;
    }

    $min_prices_corps_raw = $db->query("SELECT corps.id, MIN(price_per_night) as min_price FROM rooms INNER JOIN corps ON rooms.corps = corps.id GROUP BY corps.id");

    $m = 0;
    while ($row = $min_prices_corps_raw->fetch()) {
        $min_prices_corps[$m]['id'] = $row['id'];
        $min_prices_corps[$m]['min_price'] = $row['min_price'];

        $m++;
    }

    $output = '';
        $i = 0;
        while ($row = $result->fetch()) {
            $i++;

            $max_price = 0;
            $min_price = 0;

            for($j=0; $j<count($max_prices_corps);$j++) {
                if($max_prices_corps[$j]['id'] == $row['id']) {
                    $max_price = $max_prices_corps[$j]['max_price'];
                }
                if($min_prices_corps[$j]['id'] == $row['id']) {
                    $min_price = $min_prices_corps[$j]['min_price'];
                }
            }

            $output .= '<div class="col-xl-4 col-md-4 col-6">
                                    <div class="product-box mb-40">
                                        <div class="product-box-wrapper">
                                            <div class="product-img">
                                                <img src="/template/images/corps/' . $row['id'] . '.jpg" class="w-100" alt="">
            
                                                <p href="javascript:void(0)"
                                                   class="product-img-link quick-view-1">' . mb_substr($row['description'], 0, 30) . "..." . '</p>
                                            </div>
                                            <div class="product-desc pb-20">
                                                <div class="product-desc-top">
                                                    <div class="categories">
                                                        <p class="product-category"><span>' . $row['address'] . '</span></p>
                                                    </div>
                                                </div>
                                                <a href="/corps/' . $row['id'] . '" class="product-title">' . $row['name'] . '</a>
                                                <div class="price-switcher">
                                                    <span class="price switcher-item">Ціновий діапазон від ' . $min_price . ' до ' . $max_price . ' ₴</span>
                                                    <a href="/corps/' . $row['id'] . '" class="add-cart text-capitalize switcher-item">+перейти детальніше</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
        }

        echo $output;
    }

?>