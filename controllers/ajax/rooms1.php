
<?php

include_once("../../config/db_params.php");
include_once("../../components/Db.php");


if(isset($_POST["action"]))
{

    $dsn = "mysql:host=localhost;dbname=shtormove_db";
    $db = new PDO($dsn, 'root', 'root');
    $db->exec("set names utf8");

    if(isset($_POST["category"]))
    {
        $category_filter = implode("','", $_POST["category"]);

        $result = $db->query("SELECT * FROM rooms WHERE corps IN('".$category_filter."')");
    }
    else {
        $result = $db->query("SELECT * FROM rooms");

    }
$output = '';
$i = 0;
while ($row = $result->fetch()) {
    $i++;

    $output .= '<div class="col-xl-4 col-md-4 col-6">
                                            <div class="product-box mb-40">
                                                <div class="product-box-wrapper">
                                                    <div class="product-img">
                                                        <img src="/template/images/rooms/room-'.$row['id'].'-main.jpg" class="w-100" alt="">
                                                        <a href="/rooms/'.$row['id'].' class="d-block">
                                                            <div class="second-img">
                                                                <img src="/template/images/rooms/room-'.$row['id'].'-1.jpg" alt=""
                                                                     class="w-100">
                                                            </div>
                                                        </a>
                                                        <p href="javascript:void(0)"
                                                           class="product-img-link quick-view-1">'.mb_substr($row['description'], 0, 30)."...".'</p>
                                                    </div>

                                                    <div class="product-desc pb-20">
                                                        <div class="product-desc-top">
                                                            <div class="categories">
                                                                <a href="javascript:void(0)"
                                                                   class="product-category"><span>'.$row['design_style'].'</span></a>
                                                            </div>

                                                        </div>
                                                        <a href="/rooms/'.$row['id'].'" class="product-title">Кімната '.$row['room_number'].'</a>
                                                        <div class="price-switcher">
                                                            <span class="price switcher-item">₴'.$row['price_per_night'].'</span>
                                                            <a href="/rooms/'.$row['id'].'"
                                                               class="add-cart text-capitalize switcher-item">+Переглянути Детальніше</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
}

        echo $output;
    }

?>