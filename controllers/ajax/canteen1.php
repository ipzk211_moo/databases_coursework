<?php

include_once("../../config/db_params.php");
include_once("../../components/Db.php");

if(isset($_POST["action"]))
{

    $dsn = "mysql:host=localhost;dbname=shtormove_db";
    $db = new PDO($dsn, 'root', 'root');
    $db->exec("set names utf8");

    if(isset($_POST["category"]))
    {
        $category_filter = implode("','", $_POST["category"]);

        $result = $db->query("SELECT * FROM canteen WHERE category IN('".$category_filter."')");
    }
    else {
        $result = $db->query("SELECT * FROM canteen");
    }

    $result_categories = $db->query('SELECT * FROM canteen_categories ');
    $i = 0;
    while ($row = $result_categories->fetch()) {
        $dishes_category_data[$i]['id'] = $row['id'];
        $dishes_category_data[$i]['category'] = $row['category'];

        $i++;
    }

    $output = '';
    $i = 0;
    while ($row = $result->fetch()) {
        $i++;

        $output .=  '<div class="col-xl-4 col-md-4 col-6">
                                            <div class="product-box mb-40">
                                                <div class="product-box-wrapper">
                                                    <div class="product-img">
                                                        <img src="/template/images/dishes/'.$row['id'].'.jpg" class="w-100" alt="">

                                                        <p href="javascript:void(0)"
                                                           class="product-img-link quick-view-1">'.mb_substr($row['ingredients'], 0, 30)."...".'</p>
                                                    </div>

                                                    <div class="product-desc pb-20">
                                                        <div class="product-desc-top">

                                                            <div class="categories">';
        $res_temp = '';
        foreach($dishes_category_data as $dishes_category) {
            if($dishes_category['id'] == $row['category']) {
                $res_temp = "<p class='product-category'>".$dishes_category['category']."</p>";
            }
        }
        $output .=$res_temp;
        $output .= '  </div>
                        </div>
                        <a href="/canteen/'.$row['id'].'" class="product-title">'.$row['name'].'</a>
                        <div class="price-switcher">
                            <span class="price switcher-item">₴'.$row['price'].'</span>

                            <a href="/canteen/'.$row['id'].'" class="add-cart text-capitalize switcher-item">+переглянути інформацію</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    }

        echo $output;
    }

?>