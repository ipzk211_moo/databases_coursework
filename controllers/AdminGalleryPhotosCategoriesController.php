<?php
class AdminGalleryPhotosCategoriesController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['gallery_photos_categories_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $photos_categories = Gallery::getPhotoCategories();

        require_once('views/admin_gallery_photos_categories/index.php');
        return true;
    }


    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['gallery_photos_categories_table'] != 4 && $access['gallery_photos_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        if (isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            $errors = false;

            if (!isset($options['category']) || empty($options['category'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                Gallery::createGalleryCategory($options);

                header("Location: /admin/gallery_photos_categories");
            }
        }

        require_once('views/admin_gallery_photos_categories/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['gallery_photos_categories_table'] != 4 && $access['gallery_photos_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Gallery::getPhotoCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['category'] = $_POST['category'];

            Gallery::updatePhotoCategoryById($id, $options);

            header("Location: /admin/gallery_photos_categories");
        }
        require_once('views/admin_gallery_photos_categories/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['gallery_photos_categories_table'] != 5 && $access['gallery_photos_categories_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $category = Gallery::getPhotoCategoryById($id);

        if(!$category) header("Location: /404");

        if(isset($_POST['submit'])) {
            Gallery::deletePhotoCategoryById($id);
            header("Location: /admin/gallery_photos_categories");
        }
        require_once('views/admin_gallery_photos_categories/delete.php');
        return true;
    }


}
?>