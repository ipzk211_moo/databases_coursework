<?php
class AdminCanteenController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['canteen_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $all_dishes = Canteen::getAdminCanteenList();

        require_once('views/admin_canteen/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['canteen_table'] != 4 && $access['canteen_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $categories_list = Canteen::getCanteenCategoryList();

        if (isset($_POST['submit'])) {

            $options['name'] = $_POST['name'];
            $options['category'] = $_POST['category'];
            $options['description'] = $_POST['description'];
            $options['ingredients'] = $_POST['ingredients'];

            $options['price'] = $_POST['price'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_trend'] = $_POST['is_trend'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {

                $id = Canteen::createDish($options);

                if ($id) {

                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/dishes/{$id}.jpg");
                    }
                };

                header("Location: /admin/canteen");
            }
        }

        require_once('views/admin_canteen/create.php');
        return true;
    }


    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['canteen_table'] != 4 && $access['canteen_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $categories_list = Canteen::getCanteenCategoryList();

        $dish = Canteen::getDishById($id);

        if(!$dish) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['category'] = $_POST['category'];
            $options['description'] = $_POST['description'];
            $options['ingredients'] = $_POST['ingredients'];

            $options['price'] = $_POST['price'];
            $options['is_showing'] = $_POST['is_showing'];
            $options['is_trend'] = $_POST['is_trend'];

            if(Canteen::updateDishById($id, $options)) {

                if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                    move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/dishes/{$id}.jpg");
                }
            }
            header("Location: /admin/canteen");
        }
        require_once('views/admin_canteen/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['canteen_table'] != 5 && $access['canteen_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $dish = Canteen::getDishById($id);

        if(!$dish) header("Location: /404");

        if(isset($_POST['submit'])) {
            Canteen::deleteDishId($id);
            header("Location: /admin/canteen");
        }
        require_once('views/admin_canteen/delete.php');
        return true;
    }
}
?>