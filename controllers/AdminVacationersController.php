<?php
class AdminVacationersController extends AdminBase {
    public function actionIndex() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['vacationers_table'] == 8){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $vacationers = User::getVacationersList();

        require_once('views/admin_vacationers/index.php');
        return true;
    }

    public function actionCreate() {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);
        if($access['vacationers_table'] != 4 && $access['vacationers_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $gender_list = User::getGenderList();

        if (isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['birthday'] = $_POST['birthday'];
            $options['room'] = $_POST['room'];
            $options['phone'] = $_POST['phone'];
            $options['gender'] = $_POST['gender'];
            $options['date_of_settlement'] = $_POST['date_of_settlement'];
            $options['departure_date'] = $_POST['departure_date'];

            $errors = false;

            if (!isset($options['name']) || empty($options['name'])) {
                $errors[] = 'Заповніть поля';
            }

            if ($errors == false) {
                User::createVacationer($options);

                header("Location: /admin/vacationers");
            }
        }

        require_once('views/admin_vacationers/create.php');
        return true;
    }

    public function actionUpdate($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['vacationers_table'] != 4 && $access['vacationers_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $gender_list = User::getGenderList();

        $vacationer = User::getVacationerById($id);

        if(!$vacationer) header("Location: /404");

        if(isset($_POST['submit'])) {
            $options['name'] = $_POST['name'];
            $options['surname'] = $_POST['surname'];
            $options['birthday'] = $_POST['birthday'];
            $options['room'] = $_POST['room'];
            $options['phone'] = $_POST['phone'];
            $options['gender'] = $_POST['gender'];
            $options['date_of_settlement'] = $_POST['date_of_settlement'];
            $options['departure_date'] = $_POST['departure_date'];

            User::updateVacationerById($id, $options);

            header("Location: /admin/vacationers");
        }
        require_once('views/admin_vacationers/update.php');
        return true;
    }

    public function actionDelete($id) {
        self::checkAdmin();

        $userId = User::checkLogged();
        $access = User::getTableAccessesList($userId);

        if($access['vacationers_table'] != 5 && $access['vacationers_table'] != 7){
            header("Location: /404");
        }

        $footer_data = Site::getFooterData();

        $vacationer = User::getVacationerById($id);

        if(!$vacationer) header("Location: /404");

        if(isset($_POST['submit'])) {
            User::deleteVacationerById($id);
            header("Location: /admin/vacationers");
        }
        require_once('views/admin_vacationers/delete.php');
        return true;
    }


}
?>