<?php

    class SiteController {
        public function actionIndex() {

            $main_page_data = Site::getMainPageData();

            $footer_data = Site::getFooterData();

            $testimonials = Site::getTestimonials();

            if (isset($_POST['submit'])) {
                $options['name'] = $_POST['name'];
                $options['surname'] = $_POST['surname'];
                $options['job'] = $_POST['job'];
                $options['description'] = $_POST['description'];

                $errors = false;

                if (!isset($options['name']) || empty($options['name'])) {
                    $errors[] = 'Заповніть поля';
                }

                if ($errors == false) {
                    $id = Site::createTestimonial($options);

                    if ($id) {

                        if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                            move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/template/images/testimonials/{$id}.jpg");
                        }
                    };

                    header("Location: /");
                }
            }

            require_once('views/site/index.php');
            return true;
        }

        public function actionAbout() {
            $about_page_data = Site::getAboutPageData();

            $footer_data = Site::getFooterData();

            $team_data = Site::getTeam();

            $testimonials = Site::getTestimonials();

            require_once('views/site/about.php');
            return true;
        }

        public function actionContact() {
            $footer_data = Site::getFooterData();

            $userEmail = '';
            $userText = '';
            $userName = '';
            $userSurname = '';
            $userPhone = '';
            $result = false;

            if(isset($_POST['submit'])) {
                $userEmail = $_POST['userEmail'];
                $userText = $_POST['userText'];
                $userName = $_POST['userName'];
                $userSurname = $_POST['userSurname'];
                $userPhone = $_POST['userPhone'];

                $errors = false;

                if (!User::checkEmail($userEmail)) {
                    $errors[] = 'Неправильний email';
                }
                if ($errors == false) {
                    $typeOfEmail = "contact with us";
                    require_once('mail.php');
                }
            }

            require_once('views/site/contact.php');
            return true;

        }


    }
?>


