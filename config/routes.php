<?php
    return array(

        'admin/team/create' => 'adminTeam/create',
        'admin/team/update/([0-9]+)' => 'adminTeam/update/$1',
        'admin/team/delete/([0-9]+)' => 'adminTeam/delete/$1',
        'admin/team' => 'adminTeam/index',

        'admin/canteen_categories/create' => 'adminCanteenCategories/create',
        'admin/canteen_categories/update/([0-9]+)' => 'adminCanteenCategories/update/$1',
        'admin/canteen_categories/delete/([0-9]+)' => 'adminCanteenCategories/delete/$1',
        'admin/canteen_categories' => 'adminCanteenCategories/index',

        'admin/booked_rooms/create' => 'adminBookedRooms/create',
        'admin/booked_rooms/update/([0-9]+)' => 'adminBookedRooms/update/$1',
        'admin/booked_rooms/delete/([0-9]+)' => 'adminBookedRooms/delete/$1',
        'admin/booked_rooms' => 'adminBookedRooms/index',

        'admin/corps_category/create' => 'adminCorpsCategory/create',
        'admin/corps_category/update/([0-9]+)' => 'adminCorpsCategory/update/$1',
        'admin/corps_category/delete/([0-9]+)' => 'adminCorpsCategory/delete/$1',
        'admin/corps_category' => 'adminCorpsCategory/index',

        'admin/corps/create' => 'adminCorps/create',
        'admin/corps/update/([0-9]+)' => 'adminCorps/update/$1',
        'admin/corps/delete/([0-9]+)' => 'adminCorps/delete/$1',
        'admin/corps' => 'adminCorps/index',

        'admin/gallery_photos_categories/create' => 'adminGalleryPhotosCategories/create',
        'admin/gallery_photos_categories/update/([0-9]+)' => 'adminGalleryPhotosCategories/update/$1',
        'admin/gallery_photos_categories/delete/([0-9]+)' => 'adminGalleryPhotosCategories/delete/$1',
        'admin/gallery_photos_categories' => 'adminGalleryPhotosCategories/index',

        'admin/gallery_photos/create' => 'adminGalleryPhotos/create',
        'admin/gallery_photos/update/([0-9]+)' => 'adminGalleryPhotos/update/$1',
        'admin/gallery_photos/delete/([0-9]+)' => 'adminGalleryPhotos/delete/$1',
        'admin/gallery_photos' => 'adminGalleryPhotos/index',

        'admin/ordered_dishes/create' => 'adminOrderedDishes/create',
        'admin/ordered_dishes/update/([0-9]+)' => 'adminOrderedDishes/update/$1',
        'admin/ordered_dishes/delete/([0-9]+)' => 'adminOrderedDishes/delete/$1',
        'admin/ordered_dishes' => 'adminOrderedDishes/index',

        'admin/reviews_corps/create' => 'adminReviewsCorps/create',
        'admin/reviews_corps/update/([0-9]+)' => 'adminReviewsCorps/update/$1',
        'admin/reviews_corps/delete/([0-9]+)' => 'adminReviewsCorps/delete/$1',
        'admin/reviews_corps' => 'adminReviewsCorps/index',

        'admin/reviews_dishes/create' => 'adminReviewsDishes/create',
        'admin/reviews_dishes/update/([0-9]+)' => 'adminReviewsDishes/update/$1',
        'admin/reviews_dishes/delete/([0-9]+)' => 'adminReviewsDishes/delete/$1',
        'admin/reviews_dishes' => 'adminReviewsDishes/index',

        'admin/rooms/create' => 'adminRooms/create',
        'admin/rooms/update/([0-9]+)' => 'adminRooms/update/$1',
        'admin/rooms/delete/([0-9]+)' => 'adminRooms/delete/$1',
        'admin/rooms' => 'adminRooms/index',

        'admin/sea_lines/create' => 'adminSeaLines/create',
        'admin/sea_lines/update/([0-9]+)' => 'adminSeaLines/update/$1',
        'admin/sea_lines/delete/([0-9]+)' => 'adminSeaLines/delete/$1',
        'admin/sea_lines' => 'adminSeaLines/index',

        'admin/site_users/create' => 'adminSiteUsers/create',
        'admin/site_users/update/([0-9]+)' => 'adminSiteUsers/update/$1',
        'admin/site_users/delete/([0-9]+)' => 'adminSiteUsers/delete/$1',
        'admin/site_users' => 'adminSiteUsers/index',

        'admin/testimonials/create' => 'adminTestimonials/create',
        'admin/testimonials/update/([0-9]+)' => 'adminTestimonials/update/$1',
        'admin/testimonials/delete/([0-9]+)' => 'adminTestimonials/delete/$1',
        'admin/testimonials' => 'adminTestimonials/index',

        'admin/vacationers/create' => 'adminVacationers/create',
        'admin/vacationers/update/([0-9]+)' => 'adminVacationers/update/$1',
        'admin/vacationers/delete/([0-9]+)' => 'adminVacationers/delete/$1',
        'admin/vacationers' => 'adminVacationers/index',

        'admin/roles/create' => 'adminRoles/create',
        'admin/roles/update/([0-9]+)' => 'adminRoles/update/$1',
        'admin/roles/delete/([0-9]+)' => 'adminRoles/delete/$1',
        'admin/roles' => 'adminRoles/index',

        'admin/reviews_rooms/create' => 'adminReviewsRooms/create',
        'admin/reviews_rooms/update/([0-9]+)' => 'adminReviewsRooms/update/$1',
        'admin/reviews_rooms/delete/([0-9]+)' => 'adminReviewsRooms/delete/$1',
        'admin/reviews_rooms' => 'adminReviewsRooms/index',

        'admin/canteen/create' => 'adminCanteen/create',
        'admin/canteen/update/([0-9]+)' => 'adminCanteen/update/$1',
        'admin/canteen/delete/([0-9]+)' => 'adminCanteen/delete/$1',
        'admin/canteen' => 'adminCanteen/index',

        'admin' => 'admin/index',
        'statistics' => 'statistics/index',
        'contacts' => 'site/contact',

        'about' => 'site/about',
        'search' => 'search/index',

        'corps/page-([0-9]+)' => 'corps/index/$1',
        'corps/([0-9]+)' => 'corps/view/$1',
        'corps' => 'corps/index',

        'rooms/delete/([0-9]+)' => 'rooms/delete/$1',
        'rooms/page-([0-9]+)' => 'rooms/index/$1',
        'rooms/([0-9]+)' => 'rooms/view/$1',
        'rooms' => 'rooms/index',

        'canteen/delete/([0-9]+)' => 'canteen/delete/$1',
        'canteen/page-([0-9]+)' => 'canteen/index/$1',
        'canteen/([0-9]+)' => 'canteen/view/$1',
        'canteen' => 'canteen/index',

        'gallery/page-([0-9]+)' => 'gallery/index/$1',
        'gallery' => 'gallery/index',

        'user/register' => 'user/register',
        'user/login' => 'user/login',
        'user/logout' => 'user/logout',

        'cabinet/edit' => 'cabinet/edit',
        'cabinet' => 'cabinet/index',

        '404' => 'error/index',

        '' => 'site/index',
    );
?>