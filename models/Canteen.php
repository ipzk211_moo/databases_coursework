<?php

class Canteen
{

    const SHOW_BY_DEFAULT = 9;

    public static function getCanteenList($page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $dishes_data = array();

        $result = $db->query("SELECT * FROM canteen "
            . "WHERE is_showing = '1' "
            . "ORDER BY id DESC "
            . "LIMIT " . self::SHOW_BY_DEFAULT
            . ' OFFSET '. $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $dishes_data[$i]['id'] = $row['id'];
            $dishes_data[$i]['name'] = $row['name'];
            $dishes_data[$i]['category'] = $row['category'];
            $dishes_data[$i]['description'] = $row['description'];
            $dishes_data[$i]['ingredients'] = $row['ingredients'];
            $dishes_data[$i]['price'] = $row['price'];
            $dishes_data[$i]['is_showing'] = $row['is_showing'];
            $dishes_data[$i]['is_trend'] = $row['is_trend'];

            $i++;
        }


        return $dishes_data;
    }

    public static function deleteCanteenCategoryById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM canteen_categories WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateCanteenCategoryById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE canteen_categories SET category = :category WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getCanteenCategoryById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM canteen_categories WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function createCanteenCategory($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO canteen_categories ' .
            '(category) ' .
            'VALUES ' .
            '(:category)';

        $result = $db->prepare($sql);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function deleteDishOrderById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM ordered_dishes WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateDishOrderById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE ordered_dishes SET id_dish = :id_dish, id_site_user = :id_site_user, 
                quantity = :quantity, from_date = :from_date, to_date = :to_date    
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':id_dish', $options['id_dish'], PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':quantity', $options['quantity'], PDO::PARAM_INT);
        $result->bindParam(':from_date', $options['from_date']);
        $result->bindParam(':to_date', $options['to_date']);

        return $result->execute();
    }

    public static function getDishOrderById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM ordered_dishes WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function deleteDishId($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM canteen WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getAdminCanteenList()
    {

        $db = Db::getConnection();
        $dishes_data = array();

        $result = $db->query("SELECT * FROM canteen "
            . "ORDER BY id DESC ");

        $i = 0;
        while ($row = $result->fetch()) {
            $dishes_data[$i]['id'] = $row['id'];
            $dishes_data[$i]['name'] = $row['name'];
            $dishes_data[$i]['category'] = $row['category'];
            $dishes_data[$i]['description'] = $row['description'];
            $dishes_data[$i]['ingredients'] = $row['ingredients'];
            $dishes_data[$i]['price'] = $row['price'];
            $dishes_data[$i]['is_showing'] = $row['is_showing'];
            $dishes_data[$i]['is_trend'] = $row['is_trend'];

            $i++;
        }

        return $dishes_data;
    }

    public static function createDish($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO canteen ' .
            '(name, category, description, ingredients, price, is_showing, is_trend) ' .
            'VALUES ' .
            '(:name, :category, :description, :ingredients, :price, :is_showing, :is_trend)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':ingredients', $options['ingredients'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_trend', $options['is_trend'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;
    }

    public static function updateDishById($id, $options) {
        $db = Db::getConnection();

        $sql = "UPDATE canteen SET name = :name, category = :category, description = :description, ingredients = :ingredients, price = :price, is_showing = :is_showing, is_trend = :is_trend WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':ingredients', $options['ingredients'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_trend', $options['is_trend'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getTotalDishes()
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(id) AS count FROM canteen WHERE is_showing="1"';

        $result = $db->prepare($sql);

        $result->execute();

        $row = $result->fetch();
        return $row['count'];
    }

    public static function getCanteenCategoryList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM canteen_categories ');
        $i = 0;
        while ($row = $result->fetch()) {
            $dishes_category_data[$i]['id'] = $row['id'];
            $dishes_category_data[$i]['category'] = $row['category'];

            $i++;
        }

        return $dishes_category_data;
    }

    public static function getMaxPriceDishOfAll() {
        $db = Db::getConnection();

        $result = $db->query('SELECT MAX(price) as max_price FROM canteen');
        $row = $result->fetch();

        return $row['max_price'];
    }

    public static function getMinPriceDishOfAll() {
        $db = Db::getConnection();

        $result = $db->query('SELECT MIN(price) as min_price FROM canteen');
        $row = $result->fetch();

        return $row['min_price'];
    }

    public static function getDishById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM canteen WHERE id=' . $id);

            $row = $result->fetch();

            if($row['id']) {
                $dish['id'] = $row['id'];
                $dish['name'] = $row['name'];
                $dish['category'] = $row['category'];
                $dish['description'] = $row['description'];
                $dish['ingredients'] = $row['ingredients'];
                $dish['price'] = $row['price'];
                $dish['is_showing'] = $row['is_showing'];
                $dish['is_trend'] = $row['is_trend'];

                return $dish;
            }
            return 0;
        }
    }

    public static function getDishesByCategory($categoryId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM canteen WHERE category=' . $categoryId);
        $i = 0;
        while ($row = $result->fetch()) {

            $dishes_data[$i]['id'] = $row['id'];
            $dishes_data[$i]['name'] = $row['name'];
            $dishes_data[$i]['category'] = $row['category'];
            $dishes_data[$i]['description'] = $row['description'];
            $dishes_data[$i]['ingredients'] = $row['ingredients'];
            $dishes_data[$i]['price'] = $row['price'];
            $dishes_data[$i]['is_trend'] = $row['is_trend'];

            $i++;
        }

        return $dishes_data;
    }

    public static function createDishOrder($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO ordered_dishes ' .
            '(id_dish, id_site_user, quantity, from_date, to_date) ' .
            'VALUES ' .
            '(:id_dish, :id_site_user, :quantity, :from_date, :to_date) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_dish', $options['id_dish'], PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':quantity', $options['quantity'], PDO::PARAM_INT);
        $result->bindParam(':from_date', $options['from_date']);
        $result->bindParam(':to_date', $options['to_date']);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function orderDish($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO ordered_dishes ' .
            '(id_dish, id_site_user, quantity, from_date, to_date) ' .
            'VALUES ' .
            '(:id_dish, :id_site_user, :quantity, :from_date, :to_date) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_dish', $options['id_dish'], PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':quantity', $options['ordered_dish_quantity'], PDO::PARAM_INT);
        $result->bindParam(':from_date', $options['from_date']);
        $result->bindParam(':to_date', $options['ordered_dish_date']);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function getOrderedDishesByUserId($userId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM ordered_dishes WHERE id_site_user=' . $userId.' AND to_date >= CURDATE()');
        $i = 0;
        while ($row = $result->fetch()) {

            $ordered_dishes[$i]['id'] = $row['id'];
            $ordered_dishes[$i]['id_dish'] = $row['id_dish'];
            $ordered_dishes[$i]['quantity'] = $row['quantity'];
            $ordered_dishes[$i]['from_date'] = $row['from_date'];
            $ordered_dishes[$i]['to_date'] = $row['to_date'];

            $i++;
        }
        if (isset($ordered_dishes)) {
            return $ordered_dishes;
        }
        else {
            return 0;
        }
    }

    public static function getOrderedDishes() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM ordered_dishes ');
        $i = 0;
        while ($row = $result->fetch()) {

            $ordered_dishes[$i]['id'] = $row['id'];
            $ordered_dishes[$i]['id_dish'] = $row['id_dish'];
            $ordered_dishes[$i]['id_site_user'] = $row['id_site_user'];
            $ordered_dishes[$i]['quantity'] = $row['quantity'];
            $ordered_dishes[$i]['from_date'] = $row['from_date'];
            $ordered_dishes[$i]['to_date'] = $row['to_date'];

            $i++;
        }
        if (isset($ordered_dishes)) {
            return $ordered_dishes;
        }
        else {
            return 0;
        }
    }


    public static function getListPopularDishes()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT ordered_dishes.id_dish, canteen.name, canteen_categories.category, COUNT(id_dish) as quantity FROM ordered_dishes INNER JOIN canteen ON canteen.id = ordered_dishes.id_dish INNER JOIN canteen_categories ON canteen.category = canteen_categories.id GROUP BY id_dish ORDER BY COUNT(id_dish) DESC');
        $i = 0;
        while ($row = $result->fetch()) {
            $top_dishes[$i]['id'] = $row['id_dish'];
            $top_dishes[$i]['name'] = $row['name'];
            $top_dishes[$i]['quantity'] = $row['quantity'];
            $top_dishes[$i]['category'] = $row['category'];

            $i++;
        }

        return $top_dishes;
    }

    public static function getListPopularDishesByCategory()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT canteen_categories.category, COUNT(ordered_dishes.id_dish) as quantity FROM canteen_categories INNER JOIN canteen ON canteen_categories.id = canteen.category INNER JOIN ordered_dishes ON canteen.id = ordered_dishes.id_dish GROUP BY canteen_categories.category ORDER BY COUNT(ordered_dishes.id_dish) DESC;');
        $i = 0;
        while ($row = $result->fetch()) {
            $top_dishes[$i]['quantity'] = $row['quantity'];
            $top_dishes[$i]['category'] = $row['category'];

            $i++;
        }

        return $top_dishes;
    }

    public static function deleteDishByIdFromOrders($id_user, $id_dish) {
        $db = Db::getConnection();

        $sql = 'DELETE FROM ordered_dishes WHERE id_dish = :id_dish AND id_site_user = :id_site_user';

        $result = $db->prepare($sql);
        $result->bindParam(':id_dish', $id_dish, PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $id_user, PDO::PARAM_INT);
        return $result->execute();
    }


}


?>