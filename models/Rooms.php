<?php

class Rooms
{
    const SHOW_BY_DEFAULT = 9;

    public static function getRoomsList($page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $rooms_data = array();

        $result = $db->query("SELECT * FROM rooms "
            . "WHERE status = '1' "
            . "ORDER BY id DESC "
            . "LIMIT " . self::SHOW_BY_DEFAULT
            . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $rooms_data[$i]['id'] = $row['id'];
            $rooms_data[$i]['room_number'] = $row['room_number'];
            $rooms_data[$i]['corps'] = $row['corps'];
            $rooms_data[$i]['price_per_night'] = $row['price_per_night'];
            $rooms_data[$i]['beds_type'] = $row['beds_type'];
            $rooms_data[$i]['beds_quantity'] = $row['beds_quantity'];
            $rooms_data[$i]['status'] = $row['status'];
            $rooms_data[$i]['description'] = $row['description'];
            $rooms_data[$i]['smoking'] = $row['smoking'];
            $rooms_data[$i]['balcony'] = $row['balcony'];
            $rooms_data[$i]['design_style'] = $row['design_style'];

            $i++;
        }

        return $rooms_data;
    }

    public static function deleteBookedRoomById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM booked_rooms WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateBookedRoomById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE booked_rooms SET id_room = :id_room, id_site_user = :id_site_user, 
                move_in = :move_in, move_out = :move_out WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':id_room', $options['id_room'], PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':move_in', $options['move_in']);
        $result->bindParam(':move_out', $options['move_out']);

        return $result->execute();
    }

    public static function getBookedRoomById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM booked_rooms WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getAdminRoomsList()
    {
        $db = Db::getConnection();
        $rooms_data = array();

        $result = $db->query("SELECT * FROM rooms "
            . "ORDER BY id DESC ");

        $i = 0;
        while ($row = $result->fetch()) {
            $rooms_data[$i]['id'] = $row['id'];
            $rooms_data[$i]['room_number'] = $row['room_number'];
            $rooms_data[$i]['corps'] = $row['corps'];
            $rooms_data[$i]['price_per_night'] = $row['price_per_night'];
            $rooms_data[$i]['beds_type'] = $row['beds_type'];
            $rooms_data[$i]['beds_quantity'] = $row['beds_quantity'];
            $rooms_data[$i]['status'] = $row['status'];
            $rooms_data[$i]['description'] = $row['description'];
            $rooms_data[$i]['smoking'] = $row['smoking'];
            $rooms_data[$i]['balcony'] = $row['balcony'];
            $rooms_data[$i]['design_style'] = $row['design_style'];

            $i++;
        }

        return $rooms_data;
    }

    public static function deleteRoomById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM rooms WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateRoomById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE rooms SET room_number = :room_number, corps = :corps, price_per_night = :price_per_night, beds_type = :beds_type, beds_quantity = :beds_quantity, status = :status, description = :description, smoking = :smoking, balcony = :balcony, design_style = :design_style WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':room_number', $options['room_number'], PDO::PARAM_STR);
        $result->bindParam(':corps', $options['corps'], PDO::PARAM_INT);
        $result->bindParam(':price_per_night', $options['price_per_night']);
        $result->bindParam(':beds_type', $options['beds_type'], PDO::PARAM_INT);
        $result->bindParam(':beds_quantity', $options['beds_quantity'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':smoking', $options['smoking'], PDO::PARAM_INT);
        $result->bindParam(':balcony', $options['balcony'], PDO::PARAM_INT);
        $result->bindParam(':design_style', $options['design_style'], PDO::PARAM_STR);
        return $result->execute();
    }

    public static function createRoom($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO rooms ' .
            '(room_number, corps, price_per_night, beds_type , beds_quantity, status, description, smoking, balcony, design_style) ' .
            'VALUES ' .
            '(:room_number, :corps, :price_per_night, :beds_type , :beds_quantity, :status, :description, :smoking, :balcony, :design_style)';

        $result = $db->prepare($sql);
        $result->bindParam(':room_number', $options['room_number'], PDO::PARAM_STR);
        $result->bindParam(':corps', $options['corps'], PDO::PARAM_INT);
        $result->bindParam(':price_per_night', $options['price_per_night']);
        $result->bindParam(':beds_type', $options['beds_type'], PDO::PARAM_INT);
        $result->bindParam(':beds_quantity', $options['beds_quantity'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':smoking', $options['smoking'], PDO::PARAM_INT);
        $result->bindParam(':balcony', $options['balcony'], PDO::PARAM_INT);
        $result->bindParam(':design_style', $options['design_style'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public function getRoomsListByCorp($corp) {
        $db = Db::getConnection();
        $rooms_data = array();

        $result = $db->query('SELECT * FROM rooms WHERE corps=' . $corp);

        $i = 0;
        while ($row = $result->fetch()) {
            $rooms_data[$i]['id'] = $row['id'];
            $rooms_data[$i]['room_number'] = $row['room_number'];
            $rooms_data[$i]['price_per_night'] = $row['price_per_night'];
            $rooms_data[$i]['beds_type'] = $row['beds_type'];
            $rooms_data[$i]['description'] = $row['description'];
            $rooms_data[$i]['smoking'] = $row['smoking'];
            $rooms_data[$i]['balcony'] = $row['balcony'];
            $rooms_data[$i]['design_style'] = $row['design_style'];

            $i++;
        }

        return $rooms_data;
    }

    public static function getBedTypesList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM bed_type ');
        $i = 0;
        while ($row = $result->fetch()) {
            $bed_types[$i]['id'] = $row['id'];
            $bed_types[$i]['type'] = $row['type'];

            $i++;
        }

        return $bed_types;
    }

    public static function getStatusList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM room_status ');
        $i = 0;
        while ($row = $result->fetch()) {
            $results[$i]['id'] = $row['id'];
            $results[$i]['status'] = $row['status'];

            $i++;
        }

        return $results;
    }

    public static function getTotalRooms()
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(id) AS count FROM rooms WHERE status="1"';

        $result = $db->prepare($sql);

        $result->execute();

        $row = $result->fetch();
        return $row['count'];
    }

    public static function getMinPriceRoomOfCorp($corp) {
        $db = Db::getConnection();

        $result = $db->query('SELECT MIN(price_per_night) as min_price FROM rooms WHERE corps='.$corp);
        $row = $result->fetch();

        return $row['min_price'];
    }

    public static function getMinPriceRoomOfAll() {
        $db = Db::getConnection();

        $result = $db->query('SELECT MIN(price_per_night) as min_price FROM rooms');
        $row = $result->fetch();

        return $row['min_price'];
    }

    public static function getMaxPriceRoomOfCorp($corp) {
        $db = Db::getConnection();

        $result = $db->query('SELECT MAX(price_per_night) as max_price FROM rooms WHERE corps='.$corp);
        $row = $result->fetch();

        return $row['max_price'];
    }

    public static function getMaxPriceRoomOfAll() {
        $db = Db::getConnection();

        $result = $db->query('SELECT MAX(price_per_night) as max_price FROM rooms');
        $row = $result->fetch();

        return $row['max_price'];
    }

    public static function getRoomById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM rooms WHERE id=' . $id);

            $row = $result->fetch();

            if($row['id']) {
                $room['id'] = $row['id'];
                $room['room_number'] = $row['room_number'];
                $room['corps'] = $row['corps'];
                $room['price_per_night'] = $row['price_per_night'];
                $room['beds_type'] = $row['beds_type'];
                $room['beds_quantity'] = $row['beds_quantity'];
                $room['status'] = $row['status'];
                $room['description'] = $row['description'];
                $room['smoking'] = $row['smoking'];
                $room['balcony'] = $row['balcony'];
                $room['design_style'] = $row['design_style'];

                return $room;
            }
            return 0;
        }
    }


    public function getBookedRoomInfoById($roomId) {
        $db = Db::getConnection();
        $room_data = array();

        $result = $db->query('SELECT * FROM booked_rooms WHERE id_room=' . $roomId);

        $i = 0;
        while ($row = $result->fetch()) {
            $room_data[$i]['id'] = $row['id'];
            $room_data[$i]['id_room'] = $row['id_room'];
            $room_data[$i]['id_site_user'] = $row['id_site_user'];
            $room_data[$i]['move_in'] = $row['move_in'];
            $room_data[$i]['move_out'] = $row['move_out'];

            $i++;
        }

        return $room_data;
    }

    public static function bookRoom($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO booked_rooms ' .
            '(id_room, id_site_user, move_in, move_out) ' .
            'VALUES ' .
            '(:id_room, :id_site_user, :move_in, :move_out) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_room', $options['id_room'], PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':move_in', $options['move_in']);
        $result->bindParam(':move_out', $options['move_out']);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function getBookedRoomsByUserId($userId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM booked_rooms WHERE id_site_user=' . $userId.' AND move_out >= CURDATE()');

        $i = 0;

        while ($row = $result->fetch()) {

            $booked_rooms[$i]['id'] = $row['id'];
            $booked_rooms[$i]['id_room'] = $row['id_room'];
            $booked_rooms[$i]['move_in'] = $row['move_in'];
            $booked_rooms[$i]['move_out'] = $row['move_out'];

            $i++;
        }
        if (isset($booked_rooms)) {
            return $booked_rooms;
        }
        else {
            return 0;
        }
    }

    public static function getListPopularRooms()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT booked_rooms.id_room, rooms.room_number, COUNT(id_room) as quantity FROM booked_rooms INNER JOIN rooms ON rooms.id = booked_rooms.id_room GROUP BY id_room ORDER BY COUNT(id_room) DESC');
        $i = 0;
        while ($row = $result->fetch()) {
            $top_rooms[$i]['id'] = $row['id_room'];
            $top_rooms[$i]['room_number'] = $row['room_number'];
            $top_rooms[$i]['quantity'] = $row['quantity'];

            $i++;
        }

        return $top_rooms;
    }

    public static function deleteRoomByIdFromBooking($id_user, $id_room) {
        $db = Db::getConnection();

        $sql = 'DELETE FROM booked_rooms WHERE id_room = :id_room AND id_site_user = :id_site_user';

        $result = $db->prepare($sql);
        $result->bindParam(':id_room', $id_room, PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $id_user, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getBookedRoomsList() {
        $db = Db::getConnection();

        $booked_rooms = array();

        $result = $db->query("SELECT * FROM booked_rooms ");

        $i = 0;

        while ($row = $result->fetch()) {
            $booked_rooms[$i]['id'] = $row['id'];
            $booked_rooms[$i]['id_room'] = $row['id_room'];
            $booked_rooms[$i]['id_site_user'] = $row['id_site_user'];
            $booked_rooms[$i]['move_in'] = $row['move_in'];
            $booked_rooms[$i]['move_out'] = $row['move_out'];

            $i++;
        }

        return $booked_rooms;
    }


}


?>