<?php

class Gallery
{
    const SHOW_BY_DEFAULT = 9;

    public static function getPhotosData($page = 1) {

        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $photos_data = array();

        $result = $db->query("SELECT * FROM gallery_photos "
            . "WHERE is_showing = '1' "
            . "ORDER BY id ASC "
            . "LIMIT " . self::SHOW_BY_DEFAULT
            . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $photos_data[$i]['id'] = $row['id'];
            $photos_data[$i]['title'] = $row['title'];
            $photos_data[$i]['subtitle'] = $row['subtitle'];
            $photos_data[$i]['alt_attribute'] = $row['alt_attribute'];
            $photos_data[$i]['is_showing'] = $row['is_showing'];
            $photos_data[$i]['category'] = $row['category'];

            $i++;
        }

        return $photos_data;
    }

    public static function deletePhotoCategoryById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM gallery_photos_categories WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updatePhotoCategoryById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE gallery_photos_categories SET category = :category WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getPhotoCategoryById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM gallery_photos_categories WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function createGalleryCategory($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO gallery_photos_categories ' .
            '(category) ' .
            'VALUES ' .
            '(:category)';

        $result = $db->prepare($sql);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }

        return 0;
    }

    public static function getAdminPhotosData() {

        $db = Db::getConnection();
        $photos_data = array();

        $result = $db->query("SELECT * FROM gallery_photos "
            . "ORDER BY id DESC ");

        $i = 0;
        while ($row = $result->fetch()) {
            $photos_data[$i]['id'] = $row['id'];
            $photos_data[$i]['title'] = $row['title'];
            $photos_data[$i]['subtitle'] = $row['subtitle'];
            $photos_data[$i]['alt_attribute'] = $row['alt_attribute'];
            $photos_data[$i]['is_showing'] = $row['is_showing'];
            $photos_data[$i]['category'] = $row['category'];

            $i++;
        }

        return $photos_data;
    }

    public static function deletePhotoById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM gallery_photos WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function GetPhotoById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM gallery_photos WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getPhotoCategories() {
        $db = Db::getConnection();

        $categoryList = array();

        $result = $db->query('SELECT * FROM gallery_photos_categories');

        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['category'] = $row['category'];
            $i++;
        }

        return $categoryList;
    }

    public static function getTotalPhotos()
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(id) AS count FROM gallery_photos WHERE is_showing="1"';

        $result = $db->prepare($sql);

        $result->execute();

        $row = $result->fetch();
        return $row['count'];
    }

    public static function createGalleryPhoto($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO gallery_photos ' .
            '(title, subtitle, alt_attribute, category, is_showing) ' .
            'VALUES ' .
            '(:title, :subtitle, :alt_attribute, :category, :is_showing)';

        $result = $db->prepare($sql);
        $result->bindParam(':title', $options['title'], PDO::PARAM_STR);
        $result->bindParam(':subtitle', $options['subtitle'], PDO::PARAM_STR);
        $result->bindParam(':alt_attribute', $options['alt_attribute'], PDO::PARAM_STR);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updatePhotoById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE gallery_photos SET title = :title, subtitle = :subtitle, 
                alt_attribute = :alt_attribute, category = :category, is_showing = :is_showing WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':title', $options['title'], PDO::PARAM_STR);
        $result->bindParam(':subtitle', $options['subtitle'], PDO::PARAM_STR);
        $result->bindParam(':alt_attribute', $options['alt_attribute'], PDO::PARAM_STR);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        return $result->execute();
    }



}