<?php

class Reviews
{
    public static function deleteRoomReviewById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM reviews_rooms WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteDishReviewById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM reviews_dishes WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteCorpReviewById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM reviews_corps WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateCorpReviewById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE reviews_corps SET id_site_user = :id_site_user, id_corp = :id_corp, 
                stars = :stars, review = :review, date = :date, is_showing = :is_showing 
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_corp', $options['id_corp'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function updateDishReviewById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE reviews_dishes SET id_site_user = :id_site_user, id_dish = :id_dish, 
                stars = :stars, review = :review, date = :date, is_showing = :is_showing   
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_dish', $options['id_dish'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function updateRoomReviewById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE reviews_rooms SET id_site_user = :id_site_user, id_room = :id_room, 
                stars = :stars, review = :review, date = :date, is_showing = :is_showing   
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_room', $options['id_room'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function getRoomReviewById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM reviews_rooms WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getCorpReviewById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM reviews_corps WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getDishReviewById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM reviews_dishes WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getCorpReviewsById($corpId) {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_corps "
            . "WHERE is_showing = '1' AND id_corp = " .$corpId." ORDER BY id DESC");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];

            $i++;
        }

        return $reviews;

    }

    public static function getCorpReviews() {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_corps ");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['id_corp'] = $row['id_corp'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];
            $reviews[$i]['is_showing'] = $row['is_showing'];

            $i++;
        }

        return $reviews;

    }

    public static function getRoomReviewsById($roomId) {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_rooms "
            . "WHERE is_showing = '1' AND id_room = " .$roomId." ORDER BY id DESC");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];

            $i++;
        }
        return $reviews;
    }

    public static function getRoomReviews() {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_rooms ");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['id_room'] = $row['id_room'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];
            $reviews[$i]['is_showing'] = $row['is_showing'];

            $i++;
        }
        return $reviews;
    }


    public static function getDishReviewsById($dishId) {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_dishes "
            . "WHERE is_showing = '1' AND id_dish = " .$dishId." ORDER BY id DESC");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];

            $i++;
        }
        return $reviews;
    }

    public static function getDishReviews() {
        $db = Db::getConnection();

        $reviews = array();

        $result = $db->query("SELECT * FROM reviews_dishes ");

        $i = 0;

        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['id_site_user'] = $row['id_site_user'];
            $reviews[$i]['id_dish'] = $row['id_dish'];
            $reviews[$i]['stars'] = $row['stars'];
            $reviews[$i]['review'] = $row['review'];
            $reviews[$i]['date'] = $row['date'];
            $reviews[$i]['is_showing'] = $row['is_showing'];

            $i++;
        }
        return $reviews;
    }

    public static function createReviewCorp($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO reviews_corps ' .
            '(id_site_user, id_corp, stars, review, date, is_showing) ' .
            'VALUES ' .
            '(:id_site_user, :id_corp, :stars, :review, :date, :is_showing) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_corp', $options['id_corp'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function createReviewRoom($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO reviews_rooms ' .
            '(id_site_user, id_room, stars, review, date, is_showing) ' .
            'VALUES ' .
            '(:id_site_user, :id_room, :stars, :review, :date, :is_showing) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_room', $options['id_room'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }

        return 0;

    }

    public static function createReviewDish($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO reviews_dishes ' .
            '(id_site_user, id_dish, stars, review, date, is_showing) ' .
            'VALUES ' .
            '(:id_site_user, :id_dish, :stars, :review, :date, :is_showing) ';

        $result = $db->prepare($sql);
        $result->bindParam(':id_site_user', $options['id_site_user'], PDO::PARAM_INT);
        $result->bindParam(':id_dish', $options['id_dish'], PDO::PARAM_INT);
        $result->bindParam(':stars', $options['stars'], PDO::PARAM_INT);
        $result->bindParam(':review', $options['review']);
        $result->bindParam(':date', $options['date']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }



}


?>