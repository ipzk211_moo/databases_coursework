<?php

class Search
{

    public static function searchEverywhereCanteen($text)
    {
        $db = Db::getConnection();
        if (strlen($text) > 0) {
            $text = '%' . $text . '%';
            $sql = "SELECT * FROM canteen WHERE name like ? OR description like ? OR ingredients like ? ORDER BY id DESC";
            $stmt = $db->prepare($sql);
            $stmt->execute([$text, $text, $text]);
            return $stmt->fetchAll();
        }
    }

    public static function searchEverywhereCorps($text)
    {
        $db = Db::getConnection();
        if (strlen($text) > 0) {
            $text = '%' . $text . '%';
            $sql = "SELECT * FROM corps WHERE name like ? OR description like ? OR address like ? ORDER BY id DESC";
            $stmt = $db->prepare($sql);
            $stmt->execute([$text, $text, $text]);
            return $stmt->fetchAll();
        }
    }

    public static function searchEverywhereRooms($text)
    {
        $db = Db::getConnection();
        if (strlen($text) > 0) {
            $text = '%' . $text . '%';
            $sql = "SELECT * FROM rooms WHERE room_number like ? OR description like ? OR design_style like ? ORDER BY id DESC";
            $stmt = $db->prepare($sql);
            $stmt->execute([$text, $text, $text]);
            return $stmt->fetchAll();
        }
    }




}


?>