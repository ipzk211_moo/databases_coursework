<?php
include_once("Rooms.php");

class Corps
{
    const SHOW_BY_DEFAULT = 9;

    public static function getCorpsList($page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        $db = Db::getConnection();
        $corps_data = array();

        $result = $db->query("SELECT * FROM corps "
            . "WHERE is_showing = '1' "
            . "ORDER BY id DESC "
            . "LIMIT " . self::SHOW_BY_DEFAULT
            . ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {
            $corps_data[$i]['id'] = $row['id'];
            $corps_data[$i]['name'] = $row['name'];
            $corps_data[$i]['address'] = $row['address'];
            $corps_data[$i]['description'] = $row['description'];
            $corps_data[$i]['is_popular'] = $row['is_popular'];
            $corps_data[$i]['is_showing'] = $row['is_showing'];
            $corps_data[$i]['sea_line'] = $row['sea_line'];
            $corps_data[$i]['category'] = $row['category'];
            $corps_data[$i]['max_price'] = Rooms::getMaxPriceRoomOfCorp($row['id']);
            $corps_data[$i]['min_price'] = Rooms::getMinPriceRoomOfCorp($row['id']);

            $i++;
        }

        return $corps_data;
    }

    public static function updateCorpCategoryById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE corps_category SET category = :category WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function deleteCorpCategoryById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM corps_category WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getCorpCategoryById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM corps_category WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function createCorpsCategory($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO corps_category ' .
            '(category) ' .
            'VALUES ' .
            '(:category)';

        $result = $db->prepare($sql);
        $result->bindParam(':category', $options['category'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function deleteSeaLineById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM sea_lines WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateSeaLineById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE sea_lines SET line = :line WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':line', $options['line'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getSeaLineById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM sea_lines WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function createSeaLine($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO sea_lines ' .
            '(line) ' .
            'VALUES ' .
            '(:line)';

        $result = $db->prepare($sql);
        $result->bindParam(':line', $options['line'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function deleteCorpById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM corps WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function getAdminCorpsList()
    {
        $db = Db::getConnection();
        $corps_data = array();

        $result = $db->query("SELECT * FROM corps "
            . "ORDER BY id DESC ");

        $i = 0;
        while ($row = $result->fetch()) {
            $corps_data[$i]['id'] = $row['id'];
            $corps_data[$i]['name'] = $row['name'];
            $corps_data[$i]['address'] = $row['address'];
            $corps_data[$i]['description'] = $row['description'];
            $corps_data[$i]['is_popular'] = $row['is_popular'];
            $corps_data[$i]['is_showing'] = $row['is_showing'];
            $corps_data[$i]['sea_line'] = $row['sea_line'];
            $corps_data[$i]['category'] = $row['category'];
            $corps_data[$i]['max_price'] = Rooms::getMaxPriceRoomOfCorp($row['id']);
            $corps_data[$i]['min_price'] = Rooms::getMinPriceRoomOfCorp($row['id']);

            $i++;
        }

        return $corps_data;
    }

    public static function updateCorpById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE corps SET name = :name, address = :address, description = :description, is_showing = :is_showing, is_popular = :is_popular, sea_line = :sea_line, category = :category WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':address', $options['address'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_popular', $options['is_popular'], PDO::PARAM_INT);
        $result->bindParam(':sea_line', $options['sea_line'], PDO::PARAM_INT);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createCorp($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO corps ' .
            '(name, address, description, is_showing, is_popular, sea_line, category) ' .
            'VALUES ' .
            '(:name, :address, :description, :is_showing, :is_popular, :sea_line, :category)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':address', $options['address'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':is_showing', $options['is_showing'], PDO::PARAM_INT);
        $result->bindParam(':is_popular', $options['is_popular'], PDO::PARAM_INT);
        $result->bindParam(':sea_line', $options['sea_line'], PDO::PARAM_INT);
        $result->bindParam(':category', $options['category'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function getBedTypesList()
    {

        $db = Db::getConnection();

        $bed_types = array();

        $result = $db->query('SELECT * FROM bed_type ');
        $i = 0;
        while ($row = $result->fetch()) {
            $bed_types[$i]['id'] = $row['id'];
            $bed_types[$i]['type'] = $row['type'];

            $i++;
        }

        return $bed_types;
    }

    public static function getTotalCorps()
    {
        $db = Db::getConnection();

        $sql = 'SELECT count(id) AS count FROM corps WHERE is_showing="1"';

        $result = $db->prepare($sql);

        $result->execute();

        $row = $result->fetch();
        return $row['count'];
    }

    public static function getSeaLines() {
        $db = Db::getConnection();

        $sea_lines = array();

        $result = $db->query('SELECT * FROM sea_lines ');
        $i = 0;
        while ($row = $result->fetch()) {
            $sea_lines[$i]['id'] = $row['id'];
            $sea_lines[$i]['line'] = $row['line'];

            $i++;
        }

        return $sea_lines;
    }

    public static function getCorpsCategories() {
        $db = Db::getConnection();

        $corps_categories = array();

        $result = $db->query('SELECT * FROM corps_category ');
        $i = 0;
        while ($row = $result->fetch()) {
            $corps_categories[$i]['id'] = $row['id'];
            $corps_categories[$i]['category'] = $row['category'];

            $i++;
        }

        return $corps_categories;
    }



    public static function getCorpById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM corps WHERE id=' . $id);

            $row = $result->fetch();
            if ($row['id']) {

                $corp['id'] = $row['id'];
                $corp['name'] = $row['name'];
                $corp['address'] = $row['address'];
                $corp['description'] = $row['description'];
                $corp['is_showing'] = $row['is_showing'];
                $corp['is_popular'] = $row['is_popular'];
                $corp['sea_line'] = $row['sea_line'];
                $corp['category'] = $row['category'];
                $corp['max_price'] = Rooms::getMaxPriceRoomOfCorp($row['id']);
                $corp['min_price'] = Rooms::getMinPriceRoomOfCorp($row['id']);

                return $corp;
            }
        }
    }

}


?>