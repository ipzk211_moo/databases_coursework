<?php

class Site
{

    public static function getTestimonialById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM testimonials WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function deleteTestimonialById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM testimonials WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateTestimonialById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE testimonials SET name = :name, surname = :surname, 
                description = :description, job = :job WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'],PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':job', $options['job'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function getMainPageData() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM main_page_info ');
        $row = $result->fetch();
        $main_page_data['main_name'] = $row['main_name'];
        $main_page_data['main_description'] = $row['main_description'];
        $main_page_data['about_name'] = $row['about_name'];
        $main_page_data['about_description'] = $row['about_description'];
        $main_page_data['services_name'] = $row['services_name'];
        $main_page_data['services_description'] = $row['services_description'];
        $main_page_data['services_array'] = $row['services_array'];
        $main_page_data['gallery_name'] = $row['gallery_name'];
        $main_page_data['gallery_description'] = $row['gallery_description'];
        $main_page_data['gallery_array'] = $row['gallery_array'];
        $main_page_data['testimonials_name'] = $row['testimonials_name'];

        return $main_page_data;
    }

    public static function getAboutPageData() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM about_page_info ');
        $row = $result->fetch();
        $about_page_data['main_title'] = $row['main_title'];
        $about_page_data['main_subtitle'] = $row['main_subtitle'];
        $about_page_data['about_main_title'] = $row['about_main_title'];
        $about_page_data['about_main_subtitle'] = $row['about_main_subtitle'];
        $about_page_data['about_description'] = $row['about_description'];
        $about_page_data['about_ad1_subtitle'] = $row['about_ad1_subtitle'];
        $about_page_data['about_ad1_description'] = $row['about_ad1_description'];
        $about_page_data['about_ad2_subtitle'] = $row['about_ad2_subtitle'];
        $about_page_data['about_ad2_description'] = $row['about_ad2_description'];
        $about_page_data['team_title'] = $row['team_title'];
        $about_page_data['team_subtitle'] = $row['team_subtitle'];
        $about_page_data['ad_title'] = $row['ad_title'];
        $about_page_data['ad_subtitle'] = $row['ad_subtitle'];
        $about_page_data['ad_specification'] = $row['ad_specification'];
        $about_page_data['clients_title'] = $row['clients_title'];
        $about_page_data['clients_subtitle'] = $row['clients_subtitle'];
        $about_page_data['reservation_title'] = $row['reservation_title'];
        $about_page_data['reservation_subtitle'] = $row['reservation_subtitle'];
        $about_page_data['reservation_description'] = $row['reservation_description'];
        $about_page_data['reservation_features'] = $row['reservation_features'];
        $about_page_data['reservation_video_link'] = $row['reservation_video_link'];

        return $about_page_data;
    }

    public static function getFooterData() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM footer_info ');
        $row = $result->fetch();
        $footer_data['address'] = $row['address'];
        $footer_data['phone'] = $row['phone'];
        $footer_data['developer'] = $row['developer'];
        $footer_data['facebook_link'] = $row['facebook_link'];
        $footer_data['twitter_link'] = $row['twitter_link'];
        $footer_data['linkedin_link'] = $row['linkedin_link'];
        $footer_data['instagram_link'] = $row['instagram_link'];
        $footer_data['developer_link'] = $row['developer_link'];

        return $footer_data;
    }

    public static function getTeam() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM team WHERE publicly_showing="1"');

        $i = 0;
        while ($row = $result->fetch()) {
            $team_data[$i]['id'] = $row['id'];
            $team_data[$i]['name'] = $row['name'];
            $team_data[$i]['surname'] = $row['surname'];
            $team_data[$i]['about_person'] = $row['about_person'];
            $team_data[$i]['job'] = $row['job'];
            $i++;
        }

        return $team_data;
    }

    public static function getTestimonials() {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM testimonials ');

        $i = 0;
        while ($row = $result->fetch()) {
            $testimonials[$i]['id'] = $row['id'];
            $testimonials[$i]['name'] = $row['name'];
            $testimonials[$i]['surname'] = $row['surname'];
            $testimonials[$i]['description'] = $row['description'];
            $testimonials[$i]['job'] = $row['job'];
            $i++;
        }

        return $testimonials;
    }

    public static function createTestimonial($options) {
        $db = Db::getConnection();

        $sql = "INSERT INTO `testimonials` (`name` , `surname` , `description`, `job`) VALUES (:name, :surname, :description, :job)";

        $result = $db->prepare($sql);

        $result->bindParam(':name', $options['name'],PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':description', $options['description']);
        $result->bindParam(':job', $options['job'], PDO::PARAM_STR);


        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }






}