<?php

class User {
    public static function register($name, $surname, $gender, $birthday, $phone, $email, $password) {
        $db = Db::getConnection();
        $sql1 = 'INSERT INTO vacationers (name, surname, gender, birthday, phone) '
            . 'VALUES (:name, :surname, :gender, :birthday, :phone)';

        $result1 = $db->prepare($sql1);
        $result1->bindParam(':name', $name, PDO::PARAM_STR);
        $result1->bindParam(':surname', $surname, PDO::PARAM_STR);
        $result1->bindParam(':gender', $gender, PDO::PARAM_INT);
        $result1->bindParam(':birthday', $birthday);
        $result1->bindParam(':phone', $phone, PDO::PARAM_STR);

        $result1->execute();

        $vacationerId = User::getVacationerIdByNameSurnameBirthday($name, $surname, $birthday);

        $sql1 = 'INSERT INTO site_users (role, email, password, id_vacationer) '
            . 'VALUES ("2", :email, :password, :id_vacationer)';

        $result2 = $db->prepare($sql1);
        $result2->bindParam(':email', $email, PDO::PARAM_STR);
        $result2->bindParam(':password', $password, PDO::PARAM_STR);
        $result2->bindParam(':id_vacationer', $vacationerId['id'], PDO::PARAM_INT);

        return $result2->execute();

    }

    public static function getRolesActionsList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM roles_actions ');
        $i = 0;
        while ($row = $result->fetch()) {
            $results[$i]['id'] = $row['id'];
            $results[$i]['action'] = $row['action'];

            $i++;
        }

        return $results;
    }

    public static function updateSiteUserById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE site_users SET role = :role, email = :email, 
                password = :password WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);

        return $result->execute();
    }

    public static function updateSiteUserEmployeeById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE site_users SET role = :role, email = :email, 
                password = :password, id_employee = :id_employee, id_vacationer = NULL WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);
        $result->bindParam(':id_employee', $options['id_employee'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function updateSiteUserVacationerById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE site_users SET role = :role, email = :email, 
                password = :password, id_vacationer = :id_vacationer, id_employee = NULL WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);
        $result->bindParam(':id_vacationer', $options['id_vacationer'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function getSiteUserById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM site_users WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getVacationerById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM vacationers WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function deleteRoleById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM rolles WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteTeamUserById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM team WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteSiteUserById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM site_users WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function deleteVacationerById($id) {
        $db = Db::getConnection();
        $sql = 'DELETE FROM vacationers WHERE id = :id';
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }


    public static function getGenderList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM gender ');
        $i = 0;
        while ($row = $result->fetch()) {
            $results[$i]['id'] = $row['id'];
            $results[$i]['gender'] = $row['gender'];

            $i++;
        }

        return $results;
    }


    public static function checkName($name) {
        if(strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password) {
        if(strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    public static function checkEmail($email) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkEmailExist($email) {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM site_users WHERE email = :email';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if($result->fetchColumn()) {
            return true;
        }
        return false;
    }

    public static function checkUserData($email, $password) {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM site_users WHERE email = :email AND password = :password';

        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->execute();

        $user = $result->fetch();

        if ($user) {
            return $user['id'];
        }
        return false;

    }

    public static function auth($userId) {

        $_SESSION['userLoggedId'] = $userId;
    }

    public static function checkLogged() {
        if(isset($_SESSION['userLoggedId'])) {
            return $_SESSION['userLoggedId'];
        }

        header("Location: /user/login");
    }

    public static function isGuest() {
        if(isset($_SESSION['userLoggedId'])) {
            return false;
        }
        return true;
    }

    public static function getUserById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM site_users WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getRoleById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM rolles WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getTeamMemberById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM team WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public function getVacationerDataById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM vacationers WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getEmployeeDataById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM team WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }

    public static function getVacationerIdByNameSurnameBirthday($name, $surname, $birthday) {
        if($name && $surname && $birthday) {
            $db = Db::getConnection();

            $sql = 'SELECT id FROM vacationers WHERE name = :name AND surname = :surname AND birthday = :birthday';

            $result = $db->prepare($sql);
            $result->bindParam(':name', $name, PDO::PARAM_STR);
            $result->bindParam(':surname', $surname, PDO::PARAM_STR);
            $result->bindParam(':birthday', $birthday);

            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        }
    }


    public static function edit($temp, $site_user_id, $id, $name, $surname, $phone, $email, $password)
    {

        $db = Db::getConnection();
        $sql1 = "";
        if($temp == 1) {
            $sql1 = "UPDATE vacationers
            SET name = :name, surname = :surname, phone = :phone
            WHERE id = :id";
        }
        else if($temp == 2){
            $sql1 = "UPDATE team
            SET name = :name, surname = :surname, phone = :phone
            WHERE id = :id";
        }

        $result1 = $db->prepare($sql1);
        $result1->bindParam(':id', $id, PDO::PARAM_INT);
        $result1->bindParam(':name', $name, PDO::PARAM_STR);
        $result1->bindParam(':surname', $surname, PDO::PARAM_STR);
        $result1->bindParam(':phone', $phone, PDO::PARAM_STR);

        $result1->execute();

        $sql2 = "UPDATE site_users
            SET email = :email, password = :password
            WHERE id = :id";

        $result2 = $db->prepare($sql2);
        $result2->bindParam(':email', $email, PDO::PARAM_STR);
        $result2->bindParam(':password', $password, PDO::PARAM_STR);
        $result2->bindParam(':id', $site_user_id, PDO::PARAM_INT);

        return $result2->execute();
    }

    public static function getAllSiteUsersList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM site_users ');
        $i = 0;
        while ($row = $result->fetch()) {
            $users[$i]['id'] = $row['id'];
            $users[$i]['role'] = $row['role'];
            $users[$i]['email'] = $row['email'];
            $users[$i]['password'] = $row['password'];
            $users[$i]['id_employee'] = $row['id_employee'];
            $users[$i]['id_vacationer'] = $row['id_vacationer'];

            $i++;
        }

        return $users;
    }


    public static function getSiteUsersData() {
        $db = Db::getConnection();

        $site_users_data = array();

        $result = $db->query("SELECT * FROM site_users ");

        $i = 0;
        while ($row = $result->fetch()) {
            $site_users_data[$i]['id'] = $row['id'];
            if($row['id_employee'] != NULL) {
                $site_users_data[$i]['name'] = User::getEmployeeNameById($row['id_employee']);

                $site_users_data[$i]['surname'] = User::getEmployeeSurnameById($row['id_employee']);
            }
            else if($row['id_vacationer'] != NULL) {
                $site_users_data[$i]['name'] = User::getVacationerNameById($row['id_vacationer']);
                $site_users_data[$i]['surname'] = User::getVacationerSurnameById($row['id_vacationer']);
            }

            $i++;
        }

        return $site_users_data;

    }

    public static function getVacationerNameById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM vacationers WHERE id='.$id);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $row = $result->fetch();
        return $row['name'];
    }

    public static function getEmployeeNameById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT name FROM team WHERE id='.$id);
        $row = $result->fetch();
        return $row['name'];
    }

    public static function getVacationerSurnameById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT surname FROM vacationers WHERE id='.$id);
        $row = $result->fetch();
        return $row['surname'];
    }

    public static function getEmployeeSurnameById($id) {
        $db = Db::getConnection();

        $result = $db->query('SELECT surname FROM team WHERE id='.$id);
        $row = $result->fetch();
        return $row['surname'];
    }

    public static function getTableAccessesList($userId) {
        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM rolles INNER JOIN site_users ON site_users.role = rolles.id WHERE site_users.id='.$userId);

        $row = $result->fetch();
        $access_data['id'] = $row['id'];
        $access_data['name'] = $row['name'];
        $access_data['team_table'] = $row['team_table'];
        $access_data['canteen_table'] = $row['canteen_table'];
        $access_data['gallery_photos_table'] = $row['gallery_photos_table'];
        $access_data['corps_table'] = $row['corps_table'];
        $access_data['rooms_table'] = $row['rooms_table'];
        $access_data['vacationers_table'] = $row['vacationers_table'];
        $access_data['testimonials_table'] = $row['testimonials_table'];
        $access_data['site_users_table'] = $row['site_users_table'];
        $access_data['sea_lines_table'] = $row['sea_lines_table'];
        $access_data['roles_table'] = $row['roles_table'];
        $access_data['reviews_rooms_table'] = $row['reviews_rooms_table'];
        $access_data['reviews_dishes_table'] = $row['reviews_dishes_table'];
        $access_data['reviews_corps_table'] = $row['reviews_corps_table'];
        $access_data['ordered_dishes_table'] = $row['ordered_dishes_table'];
        $access_data['booked_rooms_table'] = $row['booked_rooms_table'];
        $access_data['gallery_photos_categories_table'] = $row['gallery_photos_categories_table'];
        $access_data['corps_category_table'] = $row['corps_category_table'];
        $access_data['canteen_categories_table'] = $row['canteen_categories_table'];

        return $access_data;

    }

    public static function getTeamList() {
        $db = Db::getConnection();

        $team_members = array();

        $result = $db->query("SELECT * FROM team ");

        $i = 0;

        while ($row = $result->fetch()) {
            $team_members[$i]['id'] = $row['id'];
            $team_members[$i]['name'] = $row['name'];
            $team_members[$i]['surname'] = $row['surname'];
            $team_members[$i]['gender'] = $row['gender'];
            $team_members[$i]['birthday'] = $row['birthday'];
            $team_members[$i]['phone'] = $row['phone'];
            $team_members[$i]['about_person'] = $row['about_person'];
            $team_members[$i]['job'] = $row['job'];
            $team_members[$i]['publicly_showing'] = $row['publicly_showing'];

            $i++;
        }

        return $team_members;
    }

    public static function getRolesList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM rolles ');
        $i = 0;
        while ($row = $result->fetch()) {
            $roles[$i]['id'] = $row['id'];
            $roles[$i]['name'] = $row['name'];
            $roles[$i]['team_table'] = $row['team_table'];
            $roles[$i]['canteen_table'] = $row['canteen_table'];
            $roles[$i]['gallery_photos_table'] = $row['gallery_photos_table'];
            $roles[$i]['corps_table'] = $row['corps_table'];
            $roles[$i]['rooms_table'] = $row['rooms_table'];
            $roles[$i]['vacationers_table'] = $row['vacationers_table'];
            $roles[$i]['testimonials_table'] = $row['testimonials_table'];
            $roles[$i]['site_users_table'] = $row['site_users_table'];
            $roles[$i]['sea_lines_table'] = $row['sea_lines_table'];
            $roles[$i]['roles_table'] = $row['roles_table'];
            $roles[$i]['reviews_rooms_table'] = $row['reviews_rooms_table'];
            $roles[$i]['reviews_corps_table'] = $row['reviews_corps_table'];
            $roles[$i]['reviews_dishes_table'] = $row['reviews_dishes_table'];
            $roles[$i]['ordered_dishes_table'] = $row['ordered_dishes_table'];
            $roles[$i]['booked_rooms_table'] = $row['booked_rooms_table'];
            $roles[$i]['gallery_photos_categories_table'] = $row['gallery_photos_categories_table'];
            $roles[$i]['corps_category_table'] = $row['corps_category_table'];
            $roles[$i]['canteen_categories_table'] = $row['canteen_categories_table'];

            $i++;
        }

        return $roles;
    }

    public static function getVacationersList()
    {

        $db = Db::getConnection();

        $result = $db->query('SELECT * FROM vacationers ');
        $i = 0;
        while ($row = $result->fetch()) {
            $vacationers[$i]['id'] = $row['id'];
            $vacationers[$i]['name'] = $row['name'];
            $vacationers[$i]['surname'] = $row['surname'];
            $vacationers[$i]['birthday'] = $row['birthday'];
            $vacationers[$i]['room'] = $row['room'];
            $vacationers[$i]['phone'] = $row['phone'];
            $vacationers[$i]['gender'] = $row['gender'];
            $vacationers[$i]['date_of_settlement'] = $row['date_of_settlement'];
            $vacationers[$i]['departure_date'] = $row['departure_date'];

            $i++;
        }

        return $vacationers;
    }

    public static function createTeamUser($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO team ' .
            '(name, surname, gender, birthday, phone, about_person, job, publicly_showing) ' .
            'VALUES ' .
            '(:name, :surname, :gender, :birthday, :phone, :about_person, :job, :publicly_showing)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':gender', $options['gender'], PDO::PARAM_INT);
        $result->bindParam(':birthday', $options['birthday']);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':about_person', $options['about_person']);
        $result->bindParam(':job', $options['job'], PDO::PARAM_STR);
        $result->bindParam(':publicly_showing', $options['publicly_showing'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function createSiteUser($options) {
        $db = Db::getConnection();


        $sql = 'INSERT INTO site_users ' .
            '(role, email, password) ' .
            'VALUES ' .
            '(:role, :email, :password)';

        $result = $db->prepare($sql);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function createSiteUserEmployee($options) {
        $db = Db::getConnection();


        $sql = 'INSERT INTO site_users ' .
            '(role, email, password, id_employee) ' .
            'VALUES ' .
            '(:role, :email, :password, :id_employee)';

        $result = $db->prepare($sql);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);
        $result->bindParam(':id_employee', $options['id_employee'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function createSiteUserVacationer($options) {
        $db = Db::getConnection();


        $sql = 'INSERT INTO site_users ' .
            '(role, email, password, id_vacationer) ' .
            'VALUES ' .
            '(:role, :email, :password, :id_vacationer)';

        $result = $db->prepare($sql);
        $result->bindParam(':role', $options['role'], PDO::PARAM_INT);
        $result->bindParam(':email', $options['email'], PDO::PARAM_STR);
        $result->bindParam(':password', $options['password'], PDO::PARAM_STR);
        $result->bindParam(':id_vacationer', $options['id_vacationer'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateTeamUserById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE team SET name = :name, surname = :surname, 
                gender = :gender, birthday = :birthday, phone = :phone, about_person = :about_person, job = :job, 
                publicly_showing = :publicly_showing WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':gender', $options['gender'], PDO::PARAM_INT);
        $result->bindParam(':birthday', $options['birthday']);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':about_person', $options['about_person']);
        $result->bindParam(':job', $options['job'], PDO::PARAM_STR);
        $result->bindParam(':publicly_showing', $options['publicly_showing'], PDO::PARAM_INT);
        return $result->execute();
    }

    public static function createVacationer($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO vacationers ' .
            '(name, surname, birthday, room, phone, gender, date_of_settlement, departure_date) ' .
            'VALUES ' .
            '(:name, :surname, :birthday, :room, :phone, :gender, :date_of_settlement, :departure_date)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':birthday', $options['birthday']);
        $result->bindParam(':room', $options['room'], PDO::PARAM_INT);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':gender', $options['gender'], PDO::PARAM_INT);
        $result->bindParam(':date_of_settlement', $options['date_of_settlement']);
        $result->bindParam(':departure_date', $options['departure_date']);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }

    public static function updateVacationerById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE vacationers SET name = :name, surname = :surname, 
                birthday = :birthday, room = :room, phone = :phone, gender = :gender, date_of_settlement = :date_of_settlement, 
                departure_date = :departure_date WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':surname', $options['surname'], PDO::PARAM_STR);
        $result->bindParam(':birthday', $options['birthday']);
        $result->bindParam(':room', $options['room'], PDO::PARAM_INT);
        $result->bindParam(':phone', $options['phone'], PDO::PARAM_STR);
        $result->bindParam(':gender', $options['gender'], PDO::PARAM_INT);
        $result->bindParam(':date_of_settlement', $options['date_of_settlement']);
        $result->bindParam(':departure_date', $options['departure_date']);

        return $result->execute();
    }

    public static function updateRoleById($id, $options) {
        $db = Db::getConnection();
        $sql = "UPDATE rolles SET name = :name, team_table = :team_table, canteen_table = :canteen_table, 
                gallery_photos_table = :gallery_photos_table, corps_table = :corps_table, rooms_table = :rooms_table, 
                vacationers_table = :vacationers_table, testimonials_table = :testimonials_table, site_users_table = :site_users_table, 
                sea_lines_table = :sea_lines_table, roles_table = :roles_table, reviews_rooms_table = :reviews_rooms_table, 
                reviews_corps_table = :reviews_corps_table, reviews_dishes_table = :reviews_dishes_table, 
                ordered_dishes_table = :ordered_dishes_table, 
                booked_rooms_table = :booked_rooms_table,
                gallery_photos_categories_table = :gallery_photos_categories_table, corps_category_table = :corps_category_table, canteen_categories_table = :canteen_categories_table 
                WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':team_table', $options['team_table'], PDO::PARAM_INT);
        $result->bindParam(':canteen_table', $options['canteen_table'], PDO::PARAM_INT);
        $result->bindParam(':gallery_photos_table', $options['gallery_photos_table'], PDO::PARAM_INT);
        $result->bindParam(':corps_table', $options['corps_table'], PDO::PARAM_INT);
        $result->bindParam(':rooms_table', $options['rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':vacationers_table', $options['vacationers_table'], PDO::PARAM_INT);
        $result->bindParam(':testimonials_table', $options['testimonials_table'], PDO::PARAM_INT);
        $result->bindParam(':site_users_table', $options['site_users_table'], PDO::PARAM_INT);
        $result->bindParam(':sea_lines_table', $options['sea_lines_table'], PDO::PARAM_INT);
        $result->bindParam(':roles_table', $options['roles_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_rooms_table', $options['reviews_rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_corps_table', $options['reviews_corps_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_dishes_table', $options['reviews_dishes_table'], PDO::PARAM_INT);
        $result->bindParam(':ordered_dishes_table', $options['ordered_dishes_table'], PDO::PARAM_INT);
        $result->bindParam(':booked_rooms_table', $options['booked_rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':gallery_photos_categories_table', $options['gallery_photos_categories_table'], PDO::PARAM_INT);
        $result->bindParam(':corps_category_table', $options['corps_category_table'], PDO::PARAM_INT);
        $result->bindParam(':canteen_categories_table', $options['canteen_categories_table'], PDO::PARAM_INT);

        return $result->execute();
    }

    public static function createRole($options) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO rolles ' .
            '(name, team_table, canteen_table, gallery_photos_table, corps_table, rooms_table, vacationers_table, testimonials_table, site_users_table, sea_lines_table, roles_table, reviews_rooms_table, reviews_corps_table, reviews_dishes_table, ordered_dishes_table, booked_rooms_table, gallery_photos_categories_table, corps_category_table, canteen_categories_table) ' .
            'VALUES ' .
            '(:name, :team_table, :canteen_table, :gallery_photos_table, :corps_table, :rooms_table, :vacationers_table, :testimonials_table, :site_users_table, :sea_lines_table, :roles_table, :reviews_rooms_table, :reviews_corps_table, :reviews_dishes_table, :ordered_dishes_table, :booked_rooms_table, :gallery_photos_categories_table, :corps_category_table, :canteen_categories_table)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':team_table', $options['team_table'], PDO::PARAM_INT);
        $result->bindParam(':canteen_table', $options['canteen_table'], PDO::PARAM_INT);
        $result->bindParam(':gallery_photos_table', $options['gallery_photos_table'], PDO::PARAM_INT);
        $result->bindParam(':corps_table', $options['corps_table'], PDO::PARAM_INT);
        $result->bindParam(':rooms_table', $options['rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':vacationers_table', $options['vacationers_table'], PDO::PARAM_INT);
        $result->bindParam(':testimonials_table', $options['testimonials_table'], PDO::PARAM_INT);
        $result->bindParam(':site_users_table', $options['site_users_table'], PDO::PARAM_INT);
        $result->bindParam(':sea_lines_table', $options['sea_lines_table'], PDO::PARAM_INT);
        $result->bindParam(':roles_table', $options['roles_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_rooms_table', $options['reviews_rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_corps_table', $options['reviews_corps_table'], PDO::PARAM_INT);
        $result->bindParam(':reviews_dishes_table', $options['reviews_dishes_table'], PDO::PARAM_INT);
        $result->bindParam(':ordered_dishes_table', $options['ordered_dishes_table'], PDO::PARAM_INT);
        $result->bindParam(':booked_rooms_table', $options['booked_rooms_table'], PDO::PARAM_INT);
        $result->bindParam(':gallery_photos_categories_table', $options['gallery_photos_categories_table'], PDO::PARAM_INT);
        $result->bindParam(':corps_category_table', $options['corps_category_table'], PDO::PARAM_INT);
        $result->bindParam(':canteen_categories_table', $options['canteen_categories_table'], PDO::PARAM_INT);

        if($result->execute()) {
            return $db->lastInsertId();
        }
        return 0;

    }


}


?>