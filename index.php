<?php
//ini_set('display_errors',0);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set('date.timezone', 'Europe/Kiev');

session_start();

require_once('components/Autoload.php');

$router = new Router();
$router->run();
?>