<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - панель адміністратора</title>

    <link rel="icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/meanmenu.css">
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/default.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
</head>
<body>
    <?php include 'views/layouts/header_admin.php'; ?>

<main>
    <section class="cart-body mb-90 gray-border-top pt-35">
        <div class="has-breadcrumb-content">
            <div class="container container-1430">
                <div class="breadcrumb-content" style="flex-direction: column;">

                    <h2 class="page-name mt-40">Панель адміністратора</h2>
                </div>

                <div class="cart-body-content">
                    <div class="row">
                        <div class="col-xl-10">
                            <div class="product-content">
                                <div class="col-md-4">
                                    <div class="service-box service-box-2">
                                        <div class="service-logo text-center">
                                            <img src="/template/images/logo/icon-2.jpg" alt="" class="service-img">
                                        </div>
                                        <div class="service-content text-center">
                                            <h6 class="title"><a href="/statistics/">Переглянути статистику</a></h6>
                                        </div>
                                    </div>
                                </div>
                                <h3>Управління таблицями</h3>
                                    <div class="table-responsive">
                                        <table class="table table-2">

                                            <tbody>
                                            <?php if($user_accesses['team_table'] != 8): ?>
                                            <tr>
                                                <td>
                                                    <div class="table-data">
                                                        <h6><a href="/admin/team" class="title">Управління таблицею team</a></h6>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="table-data">
                                                        <a href="/admin/team" class="red-color-hover">Перейти</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['canteen_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/canteen" class="title">Управління таблицею canteen</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/canteen" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['gallery_photos_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/gallery_photos" class="title">Управління таблицею gallery_photos</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/gallery_photos" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['corps_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/corps" class="title">Управління таблицею corps</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/corps" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['rooms_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/rooms" class="title">Управління таблицею rooms</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/rooms" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['vacationers_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/vacationers" class="title">Управління таблицею vacationers</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/vacationers" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['testimonials_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/testimonials" class="title">Управління таблицею testimonials</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/testimonials" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['site_users_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/site_users" class="title">Управління таблицею site_users</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/site_users" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['sea_lines_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/sea_lines" class="title">Управління таблицею sea_lines</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/sea_lines" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['roles_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/roles" class="title">Управління таблицею roles</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/roles" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['reviews_rooms_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/reviews_rooms" class="title">Управління таблицею reviews_rooms</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/reviews_rooms" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['reviews_dishes_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/reviews_dishes" class="title">Управління таблицею reviews_dishes</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/reviews_dishes" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['reviews_corps_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/reviews_corps" class="title">Управління таблицею reviews_corps</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/reviews_corps" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['ordered_dishes_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/ordered_dishes" class="title">Управління таблицею ordered_dishes</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/ordered_dishes" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['booked_rooms_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/booked_rooms" class="title">Управління таблицею booked_rooms</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/booked_rooms" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['gallery_photos_categories_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/gallery_photos_categories" class="title">Управління таблицею gallery_photos_categories</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/gallery_photos_categories" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['corps_category_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/corps_category" class="title">Управління таблицею corps_category</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/corps_category" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            <?php if($user_accesses['canteen_categories_table'] != 8): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <h6><a href="/admin/canteen_categories" class="title">Управління таблицею canteen_categories</a></h6>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <a href="/admin/canteen_categories" class="red-color-hover">Перейти</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>




    <?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>