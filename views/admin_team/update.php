<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - оновлення запису team</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Оновлення наявного запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="name">Імя</label>
                            <input type="text" name="name" value='<?php echo $member["name"]; ?>'>

                            <label for="surname">Прізвище</label>
                            <input type="text" name="surname" value='<?php echo $member["surname"]; ?>'>

                            <label for="image">Фото</label>
                            <input type="file" name="image">

                            <label for="gender">Стать</label>

                            <select name="gender">
                                <?php if(is_array($gender_list)): ?>
                                    <?php foreach($gender_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>" <?php if($member['gender'] == $one['id']) echo ' selected="selected"'?>><?php echo $one['gender']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>


                            <label for="birthday">Дата народження</label>
                            <input type="date" name="birthday" value='<?php echo $member["birthday"]; ?>'>

                            <label for="phone">Телефон</label>
                            <input type="text" name="phone" value='<?php echo $member["phone"]; ?>'>


                            <label for="about_person">Про людину</label>
                            <textarea name="about_person" class="textarea-grey-color" cols="75" rows="5"><?php echo $member['about_person']; ?></textarea>

                            <label for="job">Посада</label>
                            <input type="text" name="job" value='<?php echo $member["job"]; ?>'>

                            <label for="publicly_showing">Чи відображається</label>
                            <input type="number" name="publicly_showing" min="0" max="1" value='<?php echo $member["publicly_showing"]; ?>'>

                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Редагувати" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>