<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - додавання запису reviews_corps</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Створення нового запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="id_site_user">Користувач</label>
                            <select name="id_site_user">
                                <?php if(is_array($site_users_list)): ?>
                                    <?php foreach($site_users_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['email']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="id_corp">Корпус</label>
                            <select name="id_corp">
                                <?php if(is_array($corps_list)): ?>
                                    <?php foreach($corps_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="stars">Зірочки</label>
                            <input type="number" name="stars" min="0" max="5">

                            <label for="review">Відгук</label>
                            <textarea name="review" class="textarea-grey-color" cols="75" rows="5"></textarea>

                            <label for="date">Дата</label>
                            <input type="date" name="date" >

                            <label for="is_showing">Чи відображається</label>
                            <input type="number" name="is_showing" min="0" max="1">


                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Створити" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>