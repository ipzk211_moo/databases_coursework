<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Штормове пансіонат - пошук</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,800" rel="stylesheet" />
    <link href="/template/css/search/main.css" rel="stylesheet" />

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

<?php require_once('views/layouts/header.php'); ?>


<div class="s004">
    <form method="post">
        <fieldset>
            <legend>ЩО ВИ ХОЧЕТЕ ЗНАЙТИ?</legend>
            <div class="inner-form">
                <div class="input-field">
                    <input class="form-control" id="searched_data" name="searched_data" type="text" placeholder="Введіть для пошуку...">
                    <input type="submit" name="submit" value="&#128269;" class="btn-search">

                </div>
            </div>

        </fieldset>
    </form>
</div>

<div class="shop-body mb-90">
    <div class="container-fluid">
            <h1 class="page-name">Результати пошуку:</h1>
            <ul class="custom-result-ul l3">

                <?php if(!empty($result_search_canteen)): ?>
                    <?php foreach($result_search_canteen as $result):?>
                        <li class="js2">
                            <a href="/canteen/<?php echo($result['id']); ?>" class="product-title"><?php echo($result['name']); ?></a>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if(!empty($result_search_corps)): ?>
                    <?php foreach($result_search_corps as $result):?>
                        <li class="js2">
                            <a href="/corps/<?php echo($result['id']); ?>" class="product-title"><?php echo($result['name']); ?></a>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if(!empty($result_search_rooms)): ?>
                    <?php foreach($result_search_rooms as $result):?>
                        <li class="js2">
                            <a href="/rooms/<?php echo($result['id']); ?>" class="product-title"><?php echo($result['room_number']); ?></a>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if(empty($result_search_canteen) && empty($result_search_corps) && empty($result_search_rooms)): ?>
                    <h4>Результатів на даний момент не знайдено...</h4>
                <?php endif; ?>


            </ul>


    </div>
</div>



<?php require_once('views/layouts/footer.php'); ?>

<script src="/template/js/search/extention/choices.js"></script>
<script>
    var textPresetVal = new Choices('#choices-text-preset-values',
        {
            removeItemButton: true,
        });

</script>

<script>
    $(document).ready(function(){
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

        $(".zoom").hover(function(){

            $(this).addClass('transition');
        }, function(){

            $(this).removeClass('transition');
        });
    });

</script>




<script src="/template/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/isotope.pkgd.min.js"></script>
<script src="/template/js/one-page-nav-min.js"></script>
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.meanmenu.min.js"></script>
<script src="/template/js/ajax-form.js"></script>
<script src="/template/js/fontawesome.min.js"></script>
<script src="/template/js/wow.min.js"></script>
<script src="/template/js/jquery-ui.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/imagesloaded.pkgd.min.js"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script src="/template/js/plugins.js"></script>
<script src="/template/js/main.js"></script>
</body>

</html>