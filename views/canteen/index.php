<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Штормове пансіонат - столова</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">


    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <!--    <link rel="icon" href="/template/images/favicon.ico">-->

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

<?php require_once('views/layouts/header.php'); ?>





<div class="shop-body mb-90">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <h1 class="page-name">Столова</h1>

        </nav>
        <div class="shop-wrapper">
            <div class="row">
                <div class="col-xl-3 ">

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр страв за категоріями</h4>
                        <div class="size-link category-list">
                            <?php foreach($dishes_category_data as $category):?>
                                <a href="javascript:void(0)" class="clickable-filter-category" is-selected="no" id="<?php echo($category['id']); ?>"><?php echo($category['category']); ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>


                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр за ціною</h4>
                        <div class="size-link category-list">
                            <input type="range" class="clickable-filter-price" id="min_price" value="<?php echo($dish_min_price); ?>" min="<?php echo($dish_min_price); ?>" max="<?php echo($dish_max_price); ?>" oninput="this.nextElementSibling.value = this.value">
                            <output><?php echo($dish_min_price); ?></output> грн
                            <br>

                            <input type="range" class="clickable-filter-price" id="max_price" value="<?php echo($dish_max_price); ?>" min="<?php echo($dish_min_price); ?>" max="<?php echo($dish_max_price); ?>" oninput="this.nextElementSibling.value = this.value">
                            <output><?php echo($dish_max_price); ?></output> грн
                        </div>
                    </div>

                </div>
                <!-- /. widget -->
                <div class="col-xl-9">

                    <!-- /. filter heading -->
                    <div class="filter-content">
                        <div class="tab-content">

                            <div class="tab-pane fade active show" id="shop-tab-2">
                                <div class="product-wrapper mt-55">
                                    <div id="updated-data-filtered" class="row">


                                        <?php foreach($dishes_data as $dish):?>
                                        <div class="col-xl-4 col-md-4 col-6">
                                            <div class="product-box mb-40">
                                                <div class="product-box-wrapper">
                                                    <div class="product-img">
                                                        <img src="/template/images/dishes/<?php echo($dish['id']); ?>.jpg" class="w-100" alt="">

                                                        <p href="javascript:void(0)"
                                                           class="product-img-link quick-view-1"><?php echo(mb_substr($dish['ingredients'], 0, 30)."..."); ?></p>
                                                    </div>

                                                    <div class="product-desc pb-20">
                                                        <div class="product-desc-top">

                                                            <div class="categories">
                                                                    <?php foreach($dishes_category_data as $dishes_category):?>
                                                                        <?php if($dishes_category['id'] == $dish['category']): ?>
                                                                        <p class="product-category"><?php echo($dishes_category['category']); ?></p>
                                                                            <?php break; ?>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                            </div>

                                                        </div>
                                                        <a href="/canteen/<?php echo($dish['id']); ?>" class="product-title"><?php echo($dish['name']); ?></a>
                                                        <div class="price-switcher">
                                                            <span class="price switcher-item">₴<?php echo($dish['price']); ?></span>

                                                            <a href="/canteen/<?php echo($dish['id']); ?>" class="add-cart text-capitalize switcher-item">+переглянути інформацію</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="text-center mt-20">
                                        <?php echo $pagination->get(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php require_once('views/layouts/footer.php'); ?>
<script>
    $(document).ready(function(){
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

        $(".zoom").hover(function(){

            $(this).addClass('transition');
        }, function(){

            $(this).removeClass('transition');
        });
    });

</script>


<script>

    let filterCategory1 = document.querySelectorAll('.clickable-filter-category');

    filterCategory1.forEach(el => el.addEventListener('click', function () {

        renewRanges();

        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');



        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))


    let filterCategory5 = document.querySelectorAll('.clickable-filter-price');

    filterCategory5.forEach(el => el.addEventListener('change', function () {
        renewFilter(filterCategory1);

        let pagin = document.getElementById("pagination");
        pagin.remove();

    }))

    function renewRanges() {
        document.getElementById("min_price").value = document.getElementById("min_price").min;
        document.getElementById("max_price").value = document.getElementById("max_price").max;
    }

    function renewFilter(filterCategory) {
        filterCategory.forEach(el => {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected', 'no');
        });
    }

</script>

<script>
    $(document).ready(function(){
        $(".clickable-filter-category").click(function() {
            filter_data1();

            function filter_data1()
            {
                var action = 'fetch_data';

                var category = get_filter1('.clickable-filter-category');

                $.ajax({
                    url:"../../controllers/ajax/canteen1.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }

            function get_filter1(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }
        });



        $('input[type=range]').on('input', function () {

            filter_data2();

            function filter_data2()
            {
                var action = 'fetch_data';

                var price_range = [document.getElementById("min_price").value, document.getElementById("max_price").value];

                $.ajax({
                    url:"../../controllers/ajax/canteen2.php",
                    method:"POST",
                    data:{action:action, price_range:price_range},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }
        });

    });
</script>


<script src="/template/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/isotope.pkgd.min.js"></script>
<script src="/template/js/one-page-nav-min.js"></script>
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.meanmenu.min.js"></script>
<script src="/template/js/ajax-form.js"></script>
<script src="/template/js/fontawesome.min.js"></script>
<script src="/template/js/wow.min.js"></script>
<script src="/template/js/jquery-ui.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/imagesloaded.pkgd.min.js"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script src="/template/js/plugins.js"></script>
<script src="/template/js/main.js"></script>
</body>

</html>