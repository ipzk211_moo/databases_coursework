<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - вхід</title>

    <link rel="icon" href="/template/images/favicon.ico">


    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/meanmenu.css">
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/default.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <link rel="stylesheet" href="/template/css/login-styles.css">
</head>
<body>
    <?php include 'views/layouts/header.php'; ?>
    <?php if (isset($errors) && is_array($errors)): ?>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li> - <?php echo $error; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <div class="overlay">
        <form method="post">
            <div class="con">
                <header class="head-form">
                    <h2>Увійти</h2>
                    <p>будь ласка, введіть свою пошту і пароль</p>
                </header>
                <br>
                <div class="field-set">
                    <span class="input-item">
                    </span>
                    <input class="form-input" id="email" type="email" name="email" placeholder="Пошта" required>
                    <br>
                    <span class="input-item">
                    </span>
                    <input class="form-input" id="password" type="password" name="password" placeholder="Пароль" required>
                    <span>
                    </span>

                    <input type="submit" name="submit" class="btn btn-default theme-btn-2 w-100 login-btn" value="Увійти" />
                </div>

            </div>
        </form>
        <div class="other-submits">
            <button class="btn submits sign-up signup-btn-custom"><a href="/user/register/">Зареєструватись</a></button>
        </div>
    </div>

    <?php include 'views/layouts/footer.php'; ?>


    <script>
        function show() {
            var p = document.getElementById('pwd');
            p.setAttribute('type', 'text');
        }

        function hide() {
            var p = document.getElementById('pwd');
            p.setAttribute('type', 'password');
        }

        var pwShown = 0;

        document.getElementById("eye").addEventListener("click", function () {
            if (pwShown == 0) {
                pwShown = 1;
                show();
            } else {
                pwShown = 0;
                hide();
            }
        }, false);

    </script>
</body>
</html>