<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - редагувати дані</title>

    <link rel="icon" href="/template/images/favicon.ico">


    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/meanmenu.css">
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/default.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <link rel="stylesheet" href="/template/css/login-styles.css">
</head>
<body>
    <?php include 'views/layouts/header.php'; ?>

    <section class="login-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <?php if($result): ?>
                    <p>Дані відредаговані!</p>
                <?php else: ?>
                    <?php if(isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach($errors as $error): ?>
                                <li> - <?php echo $error; ?> </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <div class="col-lg-8 offset-lg-2">
                        <div class="basic-login">
                            <h3 class="text-center mb-60">Редагування даних</h3>
                            <form method="post">
                                <label for="name">Ім'я <span>*</span></label>
                                <input class="form-input" id="name" type="text" name="name" placeholder="" value="<?php echo $name; ?>" />

                                <label for="surname">Прізвище <span>*</span></label>
                                <input class="form-input" id="surname" type="text" name="surname" placeholder="" value="<?php echo $surname; ?>" />

                                <label for="phone">Телефон <span>*</span></label>
                                <input class="form-input" id="phone" type="text" name="phone" placeholder="" value="<?php echo $phone; ?>" />

                                <label for="email-id">Email <span>*</span></label>
                                <input class="form-input" id="email-id" type="email" name="email" placeholder="" value="<?php echo $email; ?>" />

                                <label for="password">Пароль <span>*</span></label>
                                <input class="form-input" id="password" type="password" name="password" placeholder="" value="<?php echo $password; ?>" />

                                <div class="mt-10"></div>
                                <input type="submit" name="submit" class="btn btn-default theme-btn-2 w-100 login-btn" value="Змінити" />
                            </form>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <?php include 'views/layouts/footer.php'; ?>
</body>
</html>