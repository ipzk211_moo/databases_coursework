<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - кабінет користувача</title>

    <link rel="icon" href="/template/images/favicon.ico">


    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/meanmenu.css">
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/default.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
</head>
<body>
    <?php include 'views/layouts/header.php'; ?>
    <main>


        <section class="service mt-100 mb-100">
            <div class="container container-1430">
                <h1>Кабінет користувача (<b><?php echo $user_extended_data['name']; ?> <?php echo $user_extended_data['surname']; ?></b>) - <em><?php echo $role_name ?></em></h1>
                    <h3><?php if($isAdmin == true) { echo("<a href='/admin/' class='color-red red-text-color underlined-text'>(Перейти в Адмінпанель)</a>"); } ?></h3>
                    <div class="row service-row">
                        <div class="service-box service-box-2">


                            <div class="col-md-4">
                                <div class="service-box service-box-2">
                                    <div class="service-logo text-center">
                                        <img src="/template/images/logo/icon-1.jpg" alt="" class="service-img">
                                    </div>
                                    <div class="service-content text-center">
                                        <h6 class="title"><a href="/cabinet/edit">Редагувати дані</a></h6>
                                    </div>
                                </div>
                            </div>


                            <h2>Загальна інформація</h2>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Імя</th>
                                    <th scope="col">Прізвище</th>
                                    <th scope="col">Стать</th>
                                    <th scope="col">Дата народження</th>
                                    <th scope="col">Телефон</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><?php echo $user_extended_data['name']; ?></td>
                                    <td><?php echo $user_extended_data['surname']; ?></td>
                                    <td>
                                        <?php
                                        if($user_extended_data['gender']==1) {
                                            echo('жінка');
                                        }
                                        else {
                                            echo('чоловік');
                                        }
                                        ?></td>
                                    <td><?php echo $user_extended_data['birthday']; ?></td>
                                    <td>+<?php echo $user_extended_data['phone']; ?></td>
                                </tr>

                                </tbody>
                            </table>

                            <h2>Замовлені страви</h2>
                            <?php if($ordered_dishes != 0): ?>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Страва</th>
                                        <th scope="col">Кількість</th>
                                        <th scope="col">Дата для приготування страви поваром</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach($ordered_dishes as $one_dish):?>
                                    <tr>
                                        <?php $dish_info = Canteen::getDishById($one_dish['id_dish']); ?>
                                        <td><?php echo $dish_info['name']; ?></td>
                                        <td><?php echo $one_dish['quantity']; ?></td>

                                        <td><?php echo $one_dish['to_date']; ?></td>
                                        <td><a href="/canteen/delete/<?php echo($one_dish['id_dish']); ?>">Видалити</a></td>
                                    </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>

                            <?php else: ?>
                            <p>Немає замовлених страв</p>
                            <?php endif; ?>




                            <h2>Заброньовані номери</h2>
                            <?php if($booked_rooms != 0): ?>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Кімната</th>
                                        <th scope="col">Дата заселення</th>
                                        <th scope="col">Дата виїзду</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php foreach($booked_rooms as $one_room):?>
                                        <tr>
                                            <?php $room_info = Rooms::getRoomById($one_room['id_room']); ?>
                                            <td><?php echo $room_info['room_number']; ?></td>
                                            <td><?php echo $one_room['move_in']; ?></td>
                                            <td><?php echo $one_room['move_out']; ?></td>
                                            <td><a href="/rooms/delete/<?php echo($one_room['id_room']); ?>">Видалити</a></td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>

                            <?php else: ?>
                                <p>Немає заброньованих номерів</p>
                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php include 'views/layouts/footer.php'; ?>
</body>
</html>