<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - додавання запису rooms</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Створення нового запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="room_number">Номер кімнати</label>
                            <input type="text" name="room_number" >

                            <label for="corps">Корпус</label>
                            <select name="corps">
                                <?php if(is_array($corps_list)): ?>
                                    <?php foreach($corps_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="price_per_night">Ціна</label>
                            <input type="number" name="price_per_night" min="0" step=".01">

                            <label for="beds_type">Тип ліжка</label>
                            <select name="beds_type">
                                <?php if(is_array($beds_type_list)): ?>
                                    <?php foreach($beds_type_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['type']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="beds_quantity">Кількість ліжок</label>
                            <input type="number" name="beds_quantity" min="0">

                            <label for="status">Статус</label>
                            <select name="status">
                                <?php if(is_array($status_list)): ?>
                                    <?php foreach($status_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['status']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="description">Опис</label>
                            <textarea name="description" class="textarea-grey-color" cols="75" rows="5"></textarea>

                            <label for="smoking">Чи доволено палити</label>
                            <input type="number" name="smoking" min="0" max="1">

                            <label for="balcony">Чи є балкон</label>
                            <input type="number" name="balcony" min="0" max="1">

                            <label for="design_style">Стиль дизайну</label>
                            <input type="text" name="design_style" >

                            <label for="userfile[]">Оберіть 4 фото (ЧОТИРИ РІЗНИХ ФОТО)</label>
                            <input type="file" name="userfile[]" multiple>

                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Створити" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>