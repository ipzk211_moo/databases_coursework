<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - додавання запису team</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Створення нового запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="name">Назва</label>
                            <input type="text" name="name" >

                            <label for="team_table">team</label>
                            <select name="team_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="canteen_table">canteen</label>
                            <select name="canteen_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="gallery_photos_table">gallery_photos</label>
                            <select name="gallery_photos_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="corps_table">corps</label>
                            <select name="corps_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="rooms_table">rooms</label>
                            <select name="rooms_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="vacationers_table">vacationers</label>
                            <select name="vacationers_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="testimonials_table">testimonials</label>
                            <select name="testimonials_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="site_users_table">site_users</label>
                            <select name="site_users_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="sea_lines_table">sea_lines</label>
                            <select name="sea_lines_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="roles_table">roles</label>
                            <select name="roles_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="reviews_rooms_table">reviews_rooms</label>
                            <select name="reviews_rooms_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="reviews_corps_table">reviews_corps</label>
                            <select name="reviews_corps_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="reviews_dishes_table">reviews_dishes</label>
                            <select name="reviews_dishes_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="ordered_dishes_table">ordered_dishes</label>
                            <select name="ordered_dishes_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="booked_rooms_table">booked_rooms</label>
                            <select name="booked_rooms_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="gallery_photos_categories_table">gallery_photos_categories</label>
                            <select name="gallery_photos_categories_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="corps_category_table">corps_category</label>
                            <select name="corps_category_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="canteen_categories_table">canteen_categories</label>
                            <select name="canteen_categories_table">
                                <?php if(is_array($actions)): ?>
                                    <?php foreach($actions as $one): ?>
                                        <option value="<?php echo $one['id']; ?>"><?php echo $one['action']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Створити" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>