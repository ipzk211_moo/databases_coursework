<header>
    <div class="header">
        <div class="container-header">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                    <div class="full">
                        <div class="center-desk">
                            <div class="logo"> <a href="/"><img class="header-logo-img" src="/template/images/logo/logo.png" alt="logo"></a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                    <div class="menu-area">
                        <div class="limit-box">
                            <nav class="main-menu">
                                <ul class="menu-area-main">
                                    <li class=""> <a href="/">Головна</a> </li>
                                    <li> <a href="/about/">Про нас</a> </li>
                                    <li> <a href="/corps/">Корпуси</a> </li>
                                    <li> <a href="/rooms/">Номери</a> </li>
                                    <li> <a href="/canteen/">Столова</a> </li>
                                    <li> <a href="/gallery/">Галерея</a> </li>
                                    <li> <a href="/contacts/">Контакти</a> </li>


                                    <?php if(User::isGuest()): ?>
                                        <li><a href="/user/login/">Вхід</a></li>
                                        <li><a href="/user/register/">Реєстрація</a></li>
                                    <?php else: ?>
                                        <li><a href="/cabinet/">Акаунт</a></li>
                                        <li><a href="/user/logout/">Вихід</a></li>
                                    <?php endif; ?>


                                    <li class="mean-last"> <a href="/search/"><img src="/template/images/search_icon.png" alt="search" /></a> </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>



