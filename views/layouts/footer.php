
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-width-middle">
                    <div class="address">
                        <h3>Адреса</h3>
                        <i><img src="/template/icon/3.png"><?php echo($footer_data['address']); ?></i>
                    </div>
                </div>

                <div class="col-width-middle">
                    <div class="address">
                        <h3>Телефон</h3>
                        <i><img src="/template/icon/1.png"><?php echo($footer_data['phone']); ?></i>
                    </div>
                </div>
                <div class="col-width-middle">
                    <div class="address">
                        <h3>Соціальні мережі</h3>
                        <ul class="contant_icon">
                            <li><a href="<?php echo($footer_data['facebook_link']); ?>" target="_blank"><img src="/template/icon/fb.png" alt="icon"/></a></li>
                            <li><a href="<?php echo($footer_data['twitter_link']); ?>" target="_blank"><img src="/template/icon/tw.png" alt="icon"/></a></li>
                            <li><a href="<?php echo($footer_data['linkedin_link']); ?>" target="_blank"><img src="/template/icon/lin (2).png" alt="icon"/></a></li>
                            <li><a href="<?php echo($footer_data['instagram_link']); ?>" target="_blank"><img src="/template/icon/instagram.png" alt="icon"/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
<div class="copyright">
    <p>Copyright <?php echo date("Y"); ?>. By <a href="<?php echo($footer_data['developer_link']); ?>" target="_blank"><?php echo($footer_data['developer']); ?></a></p>
</div>
