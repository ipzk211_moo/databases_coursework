<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - статистика</title>

    <link rel="icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/animate.min.css">
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/meanmenu.css">
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/default.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Категорія страви', 'Кількість замовлень'],

                <?php for($i=0;$i<count($rating_popular_dishes_category);$i++): ?>
                ["<?php echo($rating_popular_dishes_category[$i]['category']); ?>", <?php echo($rating_popular_dishes_category[$i]['quantity']); ?>],
                <?php endfor; ?>
            ]);

            var options = {
                title: 'Категорії страв у замовленнях'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }

    </script>

    <script type="text/javascript">
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
            var data = new google.visualization.arrayToDataTable([
                ['Страва', 'Кількість замовлень'],
                <?php for($i=0;$i<count($rating_popular_dishes);$i++): ?>
                <?php if($i<5):  ?>
                ["<?php echo($rating_popular_dishes[$i]['name']); ?>", <?php echo($rating_popular_dishes[$i]['quantity']); ?>],
                <?php endif; ?>
                <?php endfor; ?>
            ]);

            var options = {
                width: 800,
                legend: { position: 'none' },
                chart: {
                    title: 'Найпопулярніша страва столової у замовленнях',
                    subtitle: 'кількість замовлень страви' },
                axes: {
                    y: {
                        0: { side: 'top', label: 'Кількість замовлень'} // Top x-axis.
                    }
                },
                bar: { groupWidth: "60%" }
            };

            var chart = new google.charts.Bar(document.getElementById('top_x_div'));
            chart.draw(data, google.charts.Bar.convertOptions(options));
        };
    </script>


    <script type="text/javascript">
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawStuff1);

        function drawStuff1() {
            var data = new google.visualization.arrayToDataTable([
                ['Номер', 'Кількість бронювань'],

                <?php for($i=0;$i<count($rating_popular_rooms);$i++): ?>
                <?php if($i<5):  ?>
                ["<?php echo($rating_popular_rooms[$i]['room_number']); ?>", <?php echo($rating_popular_rooms[$i]['quantity']); ?>],

                <?php endif; ?>
                <?php endfor; ?>
            ]);

            var options = {
                title: 'Найпопулярніший номер пансіонату у бронюваннях',
                width: 900,
                legend: { position: 'none' },
                chart: { title: 'Найпопулярніший номер пансіонату у бронюваннях',
                    subtitle: 'кількість бронювань кімнати' },
                bars: 'horizontal',
                axes: {
                    x: {
                        0: { side: 'top', label: 'Кількість бронювань'}
                    }
                },
                bar: { groupWidth: "90%" }
            };

            var chart = new google.charts.Bar(document.getElementById('top_x_div2'));
            chart.draw(data, options);
        };
    </script>


</head>
<body>
    <?php include 'views/layouts/header_admin.php'; ?>

    <main>
        <section class="cart-body mb-90 gray-border-top pt-35">
            <div class="has-breadcrumb-content">
                <div class="container container-1430">
                    <div class="breadcrumb-content" style="flex-direction: column;">

                        <h2 class="page-name mt-40">Статистика</h2>
                    </div>

                    <div class="cart-body-content">
                        <div class="row">
                            <div class="col-xl-10">

                                <div id="top_x_div" style="width: 600px; height: 500px;"></div>

                                <div id="piechart" style="width: 900px; height: 500px;"></div>

                                <div id="top_x_div2" style="width: 900px; height: 500px;"></div>
                                <br>
                                <br>
                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


    <?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>