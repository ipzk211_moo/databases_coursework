<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <title>Штормове пансіонат - про нас</title>


    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <link rel="stylesheet" href="/template/css/fontawesome.css">
    <link rel="stylesheet" href="/template/css/templatemo-eduwell-style.css">
    <link rel="stylesheet" href="/template/css/owl.css">
    <link rel="stylesheet" href="/template/css/lightbox.css">


    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">

    <link rel="icon" href="/template/images/favicon.ico">
    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

</head>

<body>


<?php require_once('views/layouts/header.php'); ?>

<section class="page-heading">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="header-text">
                    <h4><?php echo($about_page_data['main_subtitle']); ?></h4>
                    <h1><?php echo($about_page_data['main_title']); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="get-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="left-image">
                    <img src="/template/images/about-left-image.png" alt="">
                </div>
            </div>
            <div class="col-lg-6 align-self-center">
                <div class="section-heading">
                    <h6><?php echo($about_page_data['about_main_subtitle']); ?></h6>
                    <h4><?php echo($about_page_data['about_main_title']); ?></h4>
                    <p><?php echo($about_page_data['about_description']); ?></p>

                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="info-item">
                            <div class="icon">
                                <img src="/template/images/service-icon-01.png" alt="">
                            </div>
                            <h4><?php echo($about_page_data['about_ad1_subtitle']); ?></h4>
                            <p><?php echo($about_page_data['about_ad1_description']); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="info-item">
                            <div class="icon">
                                <img src="/template/images/service-icon-02.png" alt="">
                            </div>
                            <h4><?php echo($about_page_data['about_ad2_subtitle']); ?></h4>
                            <p><?php echo($about_page_data['about_ad2_description']); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-team">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section-heading">
                    <h6><?php echo($about_page_data['team_subtitle']); ?></h6>
                    <h4><?php echo($about_page_data['team_title']); ?></h4>
                </div>
            </div>
            <div class="col-lg-10 offset-lg-1">
                <div class="naccs">
                    <div class="tabs">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="menu">

                                    <?php for($i=0;$i<count($team_data);$i++): ?>
                                    <div class="<?php if($i==0) echo('active'); ?>">
                                        <img src="/template/images/team-member-thumb-<?php echo($team_data[$i]['id']); ?>.jpg" alt="співробітник">
                                        <h4><?php echo($team_data[$i]['name']); ?> <?php echo($team_data[$i]['surname']); ?></h4>
                                        <span><?php echo($team_data[$i]['job']); ?></span>
                                    </div>
                                    <?php endfor; ?>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <ul class="nacc">

                                    <?php for($i=0;$i<count($team_data);$i++): ?>
                                    <li class="<?php if($i==0) echo('active'); ?>">
                                        <div>
                                            <div class="left-content">
                                                <h4><?php echo($team_data[$i]['name']); ?> <?php echo($team_data[$i]['surname']); ?></h4>
                                                <p><?php echo($team_data[$i]['about_person']); ?></p>
                                                <span><a href="javascript:void(0)">Професіоналізм</a></span>
                                                <span><a href="javascript:void(0)">Мотивація</a></span>
                                                <span class="last-span"><a href="javascript:void(0)">Успіх</a></span>
                                            </div>
                                            <div class="right-image">
                                                <img src="/template/images/team-member-<?php echo($team_data[$i]['id']); ?>.jpg" alt="співробітник">
                                            </div>
                                        </div>
                                    </li>
                                    <?php endfor; ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="simple-cta">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 offset-lg-1">
                <div class="left-image">
                    <img src="/template/images/cta-left-image.png" alt="">
                </div>
            </div>
            <div class="col-lg-5 align-self-center">
                <h6><?php echo($about_page_data['ad_subtitle']); ?></h6>
                <h4><?php echo($about_page_data['ad_title']); ?></h4>
                <p><?php echo($about_page_data['ad_specification']); ?></p>

            </div>
        </div>
    </div>
</section>

<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h6><?php echo($about_page_data['clients_subtitle']); ?></h6>
                    <h4><?php echo($about_page_data['clients_title']); ?></h4>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="owl-testimonials owl-carousel" style="position: relative; z-index: 5;">

                    <?php for($i=0;$i<count($testimonials);$i++): ?>

                    <div class="item">
                        <p>“<?php echo($testimonials[$i]['description']); ?>”</p>
                        <h4><?php echo($testimonials[$i]['name']); ?> <?php echo($testimonials[$i]['surname']); ?></h4>
                        <span><?php echo($testimonials[$i]['job']); ?></span>
                        <img src="/template/images/quote.png" alt="">
                    </div>

                    <?php endfor; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-us our-office">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="left-info">
                    <div class="section-heading">
                        <h6><?php echo($about_page_data['reservation_subtitle']); ?></h6>
                        <h4><?php echo($about_page_data['reservation_title']); ?></h4>
                    </div>
                    <p><?php echo($about_page_data['reservation_description']); ?></p>
                    <ul>
                        <?php echo($about_page_data['reservation_features']); ?>
                    </ul>
                    <div class="main-button-gradient">
                        <a href="/rooms">Забронювати номери</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div id="video">
                    <div class="thumb">
                        <img src="/template/images/video-thumb.jpg" alt="">
                        <div class="play-button">
                            <a rel="nofollow" href="<?php echo($about_page_data['reservation_video_link']); ?>" target="_blank"><i class="fa fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <ul class="social-icons">
                    <li><a href="<?php echo($footer_data['facebook_link']); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="<?php echo($footer_data['twitter_link']); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="<?php echo($footer_data['linkedin_link']); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="<?php echo($footer_data['instagram_link']); ?>" target="_blank"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-12">
                <p class="copyright copyright-custom">Copyright <?php echo date("Y"); ?>.

                    <br>By <a href="https://telegram.me/Oleennaa" target="_blank">Olena Machushnyk</a></p>
            </div>
        </div>
    </div>
</section>

<!-- Scripts -->
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/template/js/isotope.min.js"></script>
<script src="/template/js/owl-carousel.js"></script>
<script src="/template/js/lightbox.js"></script>
<script src="/template/js/tabs.js"></script>
<script src="/template/js/video.js"></script>
<script src="/template/js/slick-slider.js"></script>
<script src="/template/js/custom1.js"></script>
<script>
    //according to loftblog tut
    $('.nav li:first').addClass('active');

    var showSection = function showSection(section, isAnimate) {
        var
            direction = section.replace(/#/, ''),
            reqSection = $('.section').filter('[data-section="' + direction + '"]'),
            reqSectionPos = reqSection.offset().top - 0;

        if (isAnimate) {
            $('body, html').animate({
                    scrollTop: reqSectionPos },
                800);
        } else {
            $('body, html').scrollTop(reqSectionPos);
        }

    };

    var checkSection = function checkSection() {
        $('.section').each(function () {
            var
                $this = $(this),
                topEdge = $this.offset().top - 80,
                bottomEdge = topEdge + $this.height(),
                wScroll = $(window).scrollTop();
            if (topEdge < wScroll && bottomEdge > wScroll) {
                var
                    currentId = $this.data('section'),
                    reqLink = $('a').filter('[href*=\\#' + currentId + ']');
                reqLink.closest('li').addClass('active').
                siblings().removeClass('active');
            }
        });
    };



    $(window).scroll(function () {
        checkSection();
    });
</script>

</body>
</html>