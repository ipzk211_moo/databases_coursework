<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>Штормове пансіонат - головна</title>
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <link rel="icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body class="main-layout">

<div class="loader_bg">
    <div class="loader"><img src="/template/images/loading.gif" alt="#" /></div>
</div>

<?php require_once('views/layouts/header.php'); ?>


<section class="slider_section">
    <div id="myCarousel" class="carousel slide banner-main" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide img-slides" src="/template/images/banner1.jpg" alt="First slide">
                <div class="carousel-caption relative">
                    <div class="text-on-slider">
                        <h1><?php echo($main_page_data['main_name']); ?></h1>
                        <p><?php echo($main_page_data['main_description']); ?></p>
                    </div>
                    <a  href="/about/">Дізнатися більше</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide img-slides" src="/template/images/banner2.jpg" alt="Second slide">
                <div class="carousel-caption relative">
                    <div class="text-on-slider">
                        <h1><?php echo($main_page_data['main_name']); ?></h1>
                        <p><?php echo($main_page_data['main_description']); ?></p>
                    </div>
                    <a  href="/about/">Дізнатися більше</a>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide img-slides" src="/template/images/banner3.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption relative">
                        <div class="text-on-slider">
                            <h1><?php echo($main_page_data['main_name']); ?></h1>
                            <p><?php echo($main_page_data['main_description']); ?></p>
                        </div>
                        <a  href="/about/">Дізнатися більше</a>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <i class='fa fa-angle-left'></i>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <i class='fa fa-angle-right'></i>
        </a>
    </div>
</section>
<!-- about  -->
<div id="about" class="about top_layer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h2><?php echo($main_page_data['about_name']); ?></h2>
                    <span><?php echo($main_page_data['about_description']); ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="img-box">
                    <figure><img src="/template/images/about.png" alt="img"/></figure>
                    <a href="/about/">Прочитати більше</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end abouts -->
<!-- service -->
<div id="service" class="service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h2><?php echo($main_page_data['services_name']); ?> </h2>
                    <span><?php echo($main_page_data['services_description']); ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container margin-r-l">
        <div class="row">
            <?php $services_array = json_decode($main_page_data['services_array']); ?>

            <?php for($i=0;$i<count($services_array);$i++): ?>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 thumb">
                    <div class="service-box">
                        <figure>
                            <a href="/template/images/<?php echo($services_array[$i][0]); ?>.jpg" class="fancybox" rel="ligthbox">
                                <img  src="/template/images/<?php echo($services_array[$i][0]); ?>.jpg" class="zoom img-fluid "  alt="">
                            </a>
                            <span class="hoverle">
                            <a href="/template/images/<?php echo($services_array[$i][0]); ?>.jpg" class="fancybox" rel="ligthbox"><?php echo($services_array[$i][1]); ?></a>
                            </span>
                        </figure>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>
</div>

<div id="download" class="download">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h2><?php echo($main_page_data['gallery_name']); ?></h2>
                    <span><?php echo($main_page_data['gallery_description']); ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="main_slider" class="carousel slide banner-main" data-ride="carousel">
                    <div class="carousel-inner">

                        <?php $gallery_array = json_decode($main_page_data['gallery_array']); ?>

                        <?php for($i=0;$i<count($gallery_array);$i++): ?>
                            <div class="carousel-item  <?php if($i==0) echo('active'); ?>"> <img class="first-slide" src="/template/images/gallery/<?php echo($gallery_array[$i]); ?>.jpg" alt="First slide"> </div>
                        <?php endfor; ?>

                    </div>
                    <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev"> <i class='fa fa-angle-left'></i></a> <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next"> <i class='fa fa-angle-right'></i></a>
                </div>
                <div class="read-more">
                    <a href="/gallery/">Перейти до галереї</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="testimonial" class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="titlepage">
                    <h3><?php echo($main_page_data['testimonials_name']); ?></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-16 col-lg-6 col-md-6 col-sm-12">
                <div id="testimonial_slider" class="carousel slide banner-bg" data-ride="carousel">
                    <div class="carousel-inner">

                        <?php for($i=0;$i<count($testimonials);$i++): ?>

                        <div class="carousel-item <?php if($i==0) echo('active'); ?>">
                            <img class="first-slide" src="/template/images/testimonials/<?php echo($testimonials[$i]['id']); ?>.jpg">
                            <div class="container">
                                <div class="carousel-caption relat">
                                    <h3><?php echo($testimonials[$i]['name']); ?> <?php echo($testimonials[$i]['surname']); ?></h3>
                                    <span><i class="fa fa-quote-left"></i><?php echo($testimonials[$i]['job']); ?><i class="fa fa-quote-right"></i></span>
                                    <p><?php echo($testimonials[$i]['description']); ?></p>
                                </div>
                            </div>
                        </div>

                        <?php endfor; ?>

                    </div>
                    <a class="carousel-control-prev" href="#testimonial_slider" role="button" data-slide="prev"> <i class='fa fa-angle-right'></i></a> <a class="carousel-control-next" href="#testimonial_slider" role="button" data-slide="next"> <i class='fa fa-angle-left'></i></a>
                </div>
            </div>
            <div class="col-xl-16 col-lg-6 col-md-6 col-sm-12">
                <div class="contact">
                    <h3>Надіслати відгук</h3>
                    <form method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-12">
                                <input class="contactus" placeholder="Ім'я" type="text" id="name" name="name" required>
                            </div>
                            <div class="col-sm-12">
                                <input class="contactus" placeholder="Прізвище" type="text" id="surname" name="surname" required>
                            </div>
                            <div class="col-sm-12">
                                <input class="contactus" placeholder="Посада" type="text" id="job" name="job" required>
                            </div>
                            <div class="col-sm-12">
                                <input type="file" class="contactus" id="image" name="image" required>
                            </div>
                            <div class="col-sm-12">
                                <textarea class="textarea" placeholder="Відгук" type="text" id="description" name="description"></textarea>
                            </div>
                            <div class="col-sm-12">
                                <input type="submit" name="submit" value="Створити" class="btn btn-default theme-btn-2 w-100">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once('views/layouts/footer.php'); ?>

<script src="/template/js/jquery.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.bundle.min.js"></script>
<script src="/template/js/jquery-3.0.0.min.js"></script>
<script src="/template/js/plugin.js"></script>
<script src="/template/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/template/js/custom.js"></script>
<script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script>
    $(document).ready(function(){
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });

        $(".zoom").hover(function(){

            $(this).addClass('transition');
        }, function(){

            $(this).removeClass('transition');
        });
    });

</script>
</body>
</html>


