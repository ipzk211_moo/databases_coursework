<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="/template/css/contacts/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="/template/css/contacts/fontawesome.css">
    <link rel="stylesheet" href="/template/css/contacts/templatemo-sixteen.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">
    <link rel="icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <title>Штормове пансіонат - контакти</title>
</head>
<body>


<?php require_once('views/layouts/header.php'); ?>

<div class="find-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="page-name margin-bottom-20">Контакти</h1>
                </div>
            </div>
            <div class="col-md-8">

                <div id="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d78834.2111756017!2d30.6094892109635!3d46.26064113529801!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c6318a0b864c43%3A0x129f8fe28cf2176c!2z0J7QtNC10YHRgdCwLCDQntC00LXRgdGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA2NTAwMA!5e1!3m2!1sru!2sua!4v1649321810834!5m2!1sru!2sua" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
            <div class="col-md-4">
                <div class="left-content">
                    <h4>Наше місцерозташування</h4>
                    <p>Пансіонат "Штормове" знаходиться у селі Молочне Одеської області.<br><br>Повна адреса: с. Молочне, вул. Морська 15, 123.<br>Поштовий індекс: 10001</p>
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.twitter.com" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="send-message">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="page-name margin-bottom-20">Надішліть нам повідомлення</h1>
                </div>
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <form id="contact" action="" method="post">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="name" type="text" class="form-control" id="name" placeholder="Прізвище Ім'я По батькові" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="email" type="text" class="form-control" id="email" placeholder="Електронна пошта" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="subject" type="text" class="form-control" id="subject" placeholder="Тема" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message" placeholder="Ваше повідомлення" required></textarea>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <button type="submit" id="form-submit" class="filled-button">Надіслати повідомлення</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="accordion">
                    <li>
                        <a>Зобронювати номер</a>
                        <div class="content">
                            <p>Ви можете написати нам повідомлення із побажанням забронювати номер і з Вашими уподобаннями щодо кімнати.<br><br>Наш адміністратор зареєструє Вас у тей самий день звернення.</p>
                        </div>
                    </li>
                    <li>
                        <a>Змінити деталі бронювання</a>
                        <div class="content">
                            <p>Якщо Ваші плани змінились і Вам необхідно змінити певні деталі бронювання, напишіть ці деталі у повідомлення.<br><br>Наш адміністратор виправить деталі у тей самий день звернення.</p>
                        </div>
                    </li>
                    <li>
                        <a>Скасувати бронь номера</a>
                        <div class="content">
                            <p>Якщо Ваші плани змінились і Вам необхідно скасувати бронювання, напишіть нам це у повідомлення.<br><br>Наш адміністратор скасує бронь номера у тей самий день звернення.</p>
                        </div>
                    </li>
                    <li>
                        <a>Повернення коштів</a>
                        <div class="content">
                            <p>У разі виникнення будь-якої помилки, напишіть нам повідомлення із поясненням Вашої ситуації.<br><br>Кошти повертаються оперативно і у той самий день звернення.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php require_once('views/layouts/footer.php'); ?>

<script src="/template/js/contacts/jquery.min.js"></script>
<script src="/template/js/contacts/bootstrap.bundle.min.js"></script>


<script src="/template/js/contacts/custom.js"></script>
<script src="/template/js/contacts/owl.js"></script>
<script src="/template/js/contacts/slick.js"></script>
<script src="/template/js/contacts/isotope.js"></script>
<script src="/template/js/contacts/accordions.js"></script>


<script language = "text/Javascript">
    cleared[0] = cleared[1] = cleared[2] = 0;
    function clearField(t){
        if(! cleared[t.id]){
            cleared[t.id] = 1;
            t.value='';
            t.style.color='#fff';
        }
    }
</script>



<script src="/template/js/jquery.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.bundle.min.js"></script>
<script src="/template/js/jquery-3.0.0.min.js"></script>
<script src="/template/js/plugin.js"></script>
<script src="/template/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/template/js/custom.js"></script>
<script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

</body>
</html>



