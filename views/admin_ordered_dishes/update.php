<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - оновлення запису oredered_dishes</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Оновлення наявного запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="id_site_user">Користувач</label>
                            <select name="id_site_user">
                                <?php if(is_array($site_users_list)): ?>
                                    <?php foreach($site_users_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>" <?php if($order['id_site_user'] == $one['id']) echo ' selected="selected"'?>><?php echo $one['email']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="id_dish">Страва</label>
                            <select name="id_dish">
                                <?php if(is_array($dishes_list)): ?>
                                    <?php foreach($dishes_list as $one): ?>
                                        <option value="<?php echo $one['id']; ?>" <?php if($order['id_dish'] == $one['id']) echo ' selected="selected"'?>><?php echo $one['name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="quantity">Кількість</label>
                            <input type="number" name="quantity" min="0" max="25" value='<?php echo $order["quantity"]; ?>'>

                            <label for="from_date">Дата замовлення</label>
                            <input type="date" name="from_date" value='<?php echo $order["from_date"]; ?>'>

                            <label for="to_date">Дата виконання замовлення</label>
                            <input type="date" name="to_date" value='<?php echo $order["to_date"]; ?>'>

                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Редагувати" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>