<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - оновлення запису corps</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'views/layouts/header_admin.php'; ?>
<main>


    <section class="login-area pt-100 pb-100">

        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="basic-login">
                        <h3 class="text-center mb-60">Оновлення наявного запису</h3>
                        <form method="post" enctype="multipart/form-data">

                            <label for="name">Назва</label>
                            <input type="text" name="name" value='<?php echo $corp["name"]; ?>'>

                            <label for="address">Адреса</label>
                            <input type="text" name="address" value='<?php echo $corp["address"]; ?>'>

                            <label for="description">Опис</label>
                            <textarea name="description" class="textarea-grey-color" cols="75" rows="5"><?php echo $corp['description']; ?></textarea>

                            <label for="is_showing">Чи відображається</label>
                            <input type="number" name="is_showing" min="0" max="1" value='<?php echo $corp["is_showing"]; ?>'>

                            <label for="is_popular">Чи популярний</label>
                            <input type="number" name="is_popular" min="0" max="1" value='<?php echo $corp["is_popular"]; ?>'>

                            <label for="sea_line">Лінія берегу</label>
                            <select name="sea_line">
                                <?php if(is_array($sea_lines)): ?>
                                    <?php foreach($sea_lines as $one): ?>
                                        <option value="<?php echo $one['id']; ?>" <?php if($corp['sea_line'] == $one['id']) echo ' selected="selected"'?>><?php echo $one['line']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="category">Категорія</label>
                            <select name="category">
                                <?php if(is_array($categories)): ?>
                                    <?php foreach($categories as $one): ?>
                                        <option value="<?php echo $one['id']; ?>" <?php if($corp['category'] == $one['id']) echo ' selected="selected"'?>><?php echo $one['category']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                            <label for="userfile[]">Оберіть 4 фото (ЧОТИРИ РІЗНИХ ФОТО)</label>
                            <input type="file" name="userfile[]" multiple>

                            <div class="mt-10"></div>
                            <input type="submit" name="submit" value="Редагувати" class="btn btn-default theme-btn-2 w-100">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>