<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Штормове пансіонат - номери</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

<?php require_once('views/layouts/header.php'); ?>


<div class="shop-body mb-90">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <h1 class="page-name">Номери</h1>

        </nav>
        <div class="shop-wrapper">
            <div class="row">
                <div class="col-xl-3 ">

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр номерів за корпусом</h4>
                        <div class="size-link category-list">
                            <?php foreach($rooms_corps as $corp):?>
                                <a href="javascript:void(0)" class="clickable-filter-corps" is-selected="no" id="<?php echo($corp['id']); ?>"><?php echo($corp['name']); ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр за ціною</h4>
                        <div class="size-link category-list">
                            <input type="range" class="clickable-filter-price" id="min_price" value="<?php echo($rooms_min_price); ?>" min="<?php echo($rooms_min_price); ?>" max="<?php echo($rooms_max_price); ?>" oninput="this.nextElementSibling.value = this.value">
                            <output><?php echo($rooms_min_price); ?></output> грн
                            <br>

                            <input type="range" class="clickable-filter-price" id="max_price" value="<?php echo($rooms_max_price); ?>" min="<?php echo($rooms_min_price); ?>" max="<?php echo($rooms_max_price); ?>" oninput="this.nextElementSibling.value = this.value">
                            <output><?php echo($rooms_max_price); ?></output> грн
                        </div>
                    </div>

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр номерів за кількістю ліжок</h4>
                        <div class="size-link category-list">
                            <a href="javascript:void(0)" class="clickable-filter-beds-quantity" is-selected="no" id="1">1</a>
                            <a href="javascript:void(0)" class="clickable-filter-beds-quantity" is-selected="no" id="2">2</a>
                        </div>
                    </div>

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр номерів за наявністю балкону</h4>
                        <div class="size-link category-list">
                            <a href="javascript:void(0)" class="clickable-filter-balcony" is-selected="no" id="0">відсутній</a>
                            <a href="javascript:void(0)" class="clickable-filter-balcony" is-selected="no" id="1">наявний</a>
                        </div>
                    </div>

                    <div class="widget mt-50">
                        <h4 class="mb-30">Фільтр номерів за дозволом на куріння</h4>
                        <div class="size-link category-list">
                            <a href="javascript:void(0)" class="clickable-filter-smoking" is-selected="no" id="0">заборонено</a>
                            <a href="javascript:void(0)" class="clickable-filter-smoking" is-selected="no" id="1">дозволено</a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-9">
                    <div class="filter-content">
                        <div class="tab-content">

                            <div class="tab-pane fade active show" id="shop-tab-2">
                                <div class="product-wrapper mt-55">
                                    <div id="updated-data-filtered" class="row">

                                        <?php foreach($rooms_data as $room):?>
                                            <div class="col-xl-4 col-md-4 col-6">
                                                <div class="product-box mb-40">
                                                    <div class="product-box-wrapper">
                                                        <div class="product-img">
                                                            <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-main.jpg" class="w-100" alt="">
                                                            <a href="/rooms/<?php echo($room['id']); ?>" class="d-block">
                                                                <div class="second-img">
                                                                    <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-1.jpg" alt=""
                                                                         class="w-100">
                                                                </div>
                                                            </a>
                                                            <p href="javascript:void(0)"
                                                               class="product-img-link quick-view-1"><?php echo(mb_substr($room['description'], 0, 30)."..."); ?></p>
                                                        </div>

                                                        <div class="product-desc pb-20">
                                                            <div class="product-desc-top">
                                                                <div class="categories">
                                                                    <a href="javascript:void(0)"
                                                                       class="product-category"><span><?php echo($room['design_style']); ?></span></a>
                                                                </div>

                                                            </div>
                                                            <a href="/rooms/<?php echo($room['id']); ?>" class="product-title">Кімната <?php echo($room['room_number']); ?></a>
                                                            <div class="price-switcher">
                                                                <span class="price switcher-item">₴<?php echo($room['price_per_night']); ?></span>
                                                                <a href="/rooms/<?php echo($room['id']); ?>"
                                                                   class="add-cart text-capitalize switcher-item">+Переглянути Детальніше</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                    <div class="text-center mt-20">
                                        <?php echo $pagination->get(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require_once('views/layouts/footer.php'); ?>


<script>

    let filterCategory1 = document.querySelectorAll('.clickable-filter-corps');

    filterCategory1.forEach(el => el.addEventListener('click', function () {
        renewRanges();
        renewFilter(filterCategory4);
        renewFilter(filterCategory2);
        renewFilter(filterCategory3);
        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');

        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))


    let filterCategory2 = document.querySelectorAll('.clickable-filter-beds-quantity');

    filterCategory2.forEach(el => el.addEventListener('click', function () {
        renewRanges();
        renewFilter(filterCategory1);
        renewFilter(filterCategory4);
        renewFilter(filterCategory3);
        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');

        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))

    let filterCategory3 = document.querySelectorAll('.clickable-filter-balcony');

    filterCategory3.forEach(el => el.addEventListener('click', function () {
        renewRanges();
        renewFilter(filterCategory1);
        renewFilter(filterCategory2);
        renewFilter(filterCategory4);
        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');

        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))

    let filterCategory4 = document.querySelectorAll('.clickable-filter-smoking');

    filterCategory4.forEach(el => el.addEventListener('click', function () {
        renewRanges();
        renewFilter(filterCategory1);
        renewFilter(filterCategory2);
        renewFilter(filterCategory3);
        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');

        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))

    let filterCategory5 = document.querySelectorAll('.clickable-filter-price');

    filterCategory5.forEach(el => el.addEventListener('change', function () {
        renewFilter(filterCategory1);
        renewFilter(filterCategory2);
        renewFilter(filterCategory3);
        renewFilter(filterCategory4);

        let pagin = document.getElementById("pagination");
        pagin.remove();

    }))

    function renewRanges() {
        document.getElementById("min_price").value = document.getElementById("min_price").min;
        document.getElementById("max_price").value = document.getElementById("max_price").max;
    }

    function renewFilter(filterCategory) {
        filterCategory.forEach(el => {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected', 'no');
        });
    }

</script>

<script>
    $(document).ready(function(){
        $(".clickable-filter-corps").click(function() {
            filter_data1();

            function filter_data1()
            {
                var action = 'fetch_data';

                var category = get_filter1('.clickable-filter-corps');

                $.ajax({
                    url:"../../controllers/ajax/rooms1.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }

            function get_filter1(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }
        });

        $(".clickable-filter-beds-quantity").click(function() {
            filter_data2();

            function filter_data2()
            {
                var action = 'fetch_data';

                var category = get_filter2('.clickable-filter-beds-quantity');

                $.ajax({
                    url:"../../controllers/ajax/rooms2.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }

            function get_filter2(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }
        });


        $(".clickable-filter-balcony").click(function() {
            filter_data3();

            function filter_data3()
            {
                var action = 'fetch_data';

                var category = get_filter3('.clickable-filter-balcony');

                $.ajax({
                    url:"../../controllers/ajax/rooms3.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }

            function get_filter3(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }
        });


        $(".clickable-filter-smoking").click(function() {
            filter_data4();

            function filter_data4()
            {
                var action = 'fetch_data';

                var category = get_filter4('.clickable-filter-smoking');

                $.ajax({
                    url:"../../controllers/ajax/rooms4.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }

            function get_filter4(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }
        });

        $('input[type=range]').on('input', function () {

            filter_data5();

            function filter_data5()
            {
                var action = 'fetch_data';

                var price_range = [document.getElementById("min_price").value, document.getElementById("max_price").value];

                $.ajax({
                    url:"../../controllers/ajax/rooms5.php",
                    method:"POST",
                    data:{action:action, price_range:price_range},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });
            }
        });

    });
</script>


<script src="/template/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/isotope.pkgd.min.js"></script>
<script src="/template/js/one-page-nav-min.js"></script>
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.meanmenu.min.js"></script>
<script src="/template/js/ajax-form.js"></script>
<script src="/template/js/fontawesome.min.js"></script>
<script src="/template/js/wow.min.js"></script>
<script src="/template/js/jquery-ui.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/imagesloaded.pkgd.min.js"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script src="/template/js/plugins.js"></script>
<script src="/template/js/main.js"></script>
</body>

</html>