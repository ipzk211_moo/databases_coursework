<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Штормове пансіонат - про номер</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

<?php require_once('views/layouts/header.php'); ?>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Забронювати номер</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <?php if(isset($room_booking_info) && !empty($room_booking_info)): ?>
                    <p><i class="fa far fa-exclamation-triangle"></i> Увага! Даний номер недоступний для бронювання такими числами:</p>
                    <ul>
                        <?php foreach($room_booking_info as $room_book): ?>
                            <li>ВІД <?php echo($room_book['move_in']); ?> ДО <?php echo($room_book['move_out']); ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <form method="post">
                    <label for="ordered_dish_quantity">Вкажіть дату заселення:</label>
                    <br>
                    <input type="date" id="date_move_in" name="date_move_in" min="<?php echo(date('d-m-y h:m')); ?>" max="2025-06-14T00:00">
                    <br>
                    <label for="ordered_dish_date">Вкажіть дату виселення:</label>
                    <br>
                    <input type="date" id="date_move_out" name="date_move_out" min="<?php echo(date('d-m-y h:m')); ?>" max="2025-06-14T00:00">
                    <br>
                    <br>
                    <input type="submit" name="submit" value="Підтвердити і забронювати" class="btn btn-default theme-btn-2 w-100">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="shop-body mb-90">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <h1 class="page-name">Про номер "<?php echo($room['room_number']); ?>"</h1>

        </nav>
        <div class="shop-wrapper">
            <div class="row ">
                <section class="single-product mb-90">
                    <div class="container-fluid margin-left-large">
                        <div class="shop-wrapper">
                            <div class="single-product-top">
                                <div class="row row-margin-left-large">
                                    <div class="col-xl-6 col-lg-6 col-12">
                                        <div class="shop-img">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="tab-content" id="v-pills-tabContent">
                                                        <div class="tab-pane fade show active" id="tab-1">
                                                            <div class="product-img">
                                                                <a class="popup-image" href="/template/images/rooms/room-<?php echo($room['id']); ?>-main.jpg"><img src="/template/images/rooms/room-<?php echo($room['id']); ?>-main.jpg" class="w-100" alt=""></a>
                                                            </div>

                                                        </div>
                                                        <div class="tab-pane fade" id="tab-2">
                                                            <div class="product-img">
                                                                <a class="popup-image" href="/template/images/rooms/room-<?php echo($room['id']); ?>-1.jpg"><img src="/template/images/rooms/room-<?php echo($room['id']); ?>-1.jpg" class="w-100" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab-3">
                                                            <div class="product-img">
                                                                <a class="popup-image" href="/template/images/rooms/room-<?php echo($room['id']); ?>-2.jpg"><img src="/template/images/rooms/room-<?php echo($room['id']); ?>-2.jpg" class="w-100" alt=""></a>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab-4">
                                                            <div class="product-img">
                                                                <a class="popup-image" href="/template/images/rooms/room-<?php echo($room['id']); ?>-3.jpg"><img src="/template/images/rooms/room-<?php echo($room['id']); ?>-3.jpg" class="w-100" alt=""></a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="nav nav-pills has-border-img mt-25" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                        <a class="active" data-toggle="pill" href="#tab-1">
                                                            <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-main.jpg" alt="" width="80" height="80">
                                                        </a>
                                                        <a data-toggle="pill" href="#tab-2">
                                                            <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-1.jpg" width="80" height="80" alt="">
                                                        </a>
                                                        <a data-toggle="pill" href="#tab-3">
                                                            <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-2.jpg" width="80" height="80" alt="">
                                                        </a>
                                                        <a data-toggle="pill" href="#tab-4">
                                                            <img src="/template/images/rooms/room-<?php echo($room['id']); ?>-3.jpg" width="80" height="80" alt="">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-12 row-margin-left-small">
                                        <div class="single-product-sidebar">
                                            <div class="product-content">

                                                <div class="single-product-price">Ціна номеру за добу <span><?php echo($room['price_per_night']); ?></span> ₴</div>
                                                <div class="single-product-desc mb-25">
                                                    <p><?php echo($room['description']); ?></p>



                                                    <div class="single-product-category mt-20">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Корпус: </a></li>
                                                            <?php foreach($corps_categories as $corp_categoty):?>
                                                                <?php if($corp_categoty['id'] == $room['corps']): ?>
                                                                    <li><a href="javascript:void(0)"><?php echo($corp_categoty['name']); ?></a></li>
                                                                    <?php break; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>

                                                    <div class="single-product-category no-margin-top">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Тип ліжка: </a></li>
                                                            <?php foreach($rooms_beds_type as $bed_type):?>
                                                                <?php if($bed_type['id'] == $room['beds_type']): ?>
                                                                    <li><a href="javascript:void(0)"><?php echo($bed_type['type']); ?></a></li>
                                                                    <?php break; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>

                                                    <div class="single-product-category no-margin-top">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Кількість ліжок: </a></li>
                                                            <li><a href="javascript:void(0)"><?php echo($room['beds_quantity']); ?></a></li>
                                                        </ul>
                                                    </div>

                                                    <div class="single-product-category no-margin-top">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Палити цигарки: </a></li>
                                                            <li><a href="javascript:void(0)"><?php if($room['smoking']==1) {
                                                                        echo('дозволено');
                                                                    }
                                                                    else {
                                                                        echo('заборонено');
                                                                    }
                                                                     ?></a></li>
                                                        </ul>
                                                    </div>

                                                    <div class="single-product-category no-margin-top">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Балкон: </a></li>
                                                            <li><a href="javascript:void(0)"><?php if($room['balcony']==1) {
                                                                        echo('є');
                                                                    }
                                                                    else {
                                                                        echo('немає');
                                                                    }
                                                                    ?></a></li>
                                                        </ul>
                                                    </div>

                                                    <div class="single-product-category no-margin-top">
                                                        <ul>
                                                            <li><a href="javascript:void(0)" class="title">Стиль дизайну: </a></li>
                                                            <li><a href="javascript:void(0)"><?php echo($room['design_style']); ?></a></li>
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="quick-quantity mt-0">
                                                <?php if(User::isGuest()): ?>
                                                    <h4><i class="fa far fa-exclamation-triangle"></i> <em>Авторизуйтесь, щоб забронювати даний номер</em></h4>
                                                    <br>
                                                <?php else: ?>
                                                    <form action="#" method="POST">
                                                        <button type="button" class="btn btn-primary list-add-cart-btn red-hover-btn border-0" style="padding-left: 80px;padding-right: 80px;transition: all .5s;" data-toggle="modal" data-target="#exampleModal">
                                                            Забронювати
                                                        </button>
                                                    </form>
                                                <?php endif; ?>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <div class="centered-block generic-title text-center">
                    <h2 class="mb-20">Інші номери із цього корпусу</h2>
                </div>
                <section >

                    <section class="sugession-product">

                        <div class="container-fluid">

                            <div class="main-product-carousel owl-carousel red-nav mt-50">

                                <?php foreach($all_rooms_in_corp as $one_room):?>
                                    <div class="carousel-single-item">
                                        <div class="col-12">
                                            <div class="product-box">
                                                <div class="product-box-wrapper">
                                                    <div class="product-img">
                                                        <img src="/template/images/rooms/room-<?php echo($one_room['id']); ?>-main.jpg" class="w-100" alt="">

                                                        <a href="javascript:void(0)"
                                                           class="product-img-link quick-view-1 text-capitalize"><?php echo(mb_substr($one_room['description'], 0, 22)."..."); ?></a>
                                                    </div>

                                                    <div class="product-desc pb-20">
                                                        <div class="product-desc-top">
                                                            <div class="categories">
                                                                <a href="javascript:void(0)" class="product-category"><span><?php echo($one_room['design_style']); ?></span></a>
                                                            </div>

                                                        </div>
                                                        <a href="/rooms/<?php echo($one_room['id']); ?>" class="product-title">Кімната <?php echo($one_room['room_number']); ?></a>
                                                        <div class="price-switcher">
                                                            <span class="price switcher-item">₴<?php echo($one_room['price_per_night']); ?></span>
                                                            <a href="/rooms/<?php echo($one_room['id']); ?>" class="add-cart text-capitalize switcher-item">
                                                                +детальніше</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </section>
                </section>


                <section class="single-product mb-90 mb-90-custom">
                    <div class="single-product-bottom mt-80 gray-border-top">
                        <ul class="nav nav-pills justify-content-center mt-100" role="tablist">


                            <li class="nav-item">
                                <a class="active" data-toggle="pill" href="#desc-tab-1">Відгуки (<?php echo(count($reviews)); ?>)</a>
                            </li>
                        </ul>

                        <div class="container container-1200">
                            <div class="tab-content mt-20">

                                <?php if(User::isGuest()): ?>
                                    <h4><i class="fa far fa-exclamation-triangle"></i> <em>Авторизуйтесь, щоб додати власний відгук про номер</em></h4>
                                    <br>
                                <?php else: ?>
                                    <h3>Додати власний відгук</h3>

                                    <form method="post">
                                        <label for="rating">Ваш рейтинг</label>

                                        <select class="select-rating" name="stars" id="stars"> <!--Supplement an id here instead of using 'name'-->
                                            <option value="1">1 зірочка</option>
                                            <option value="2">2 зірочки</option>
                                            <option value="3">3 зірочки</option>
                                            <option value="4">4 зірочки</option>
                                            <option value="5" selected>5 зірочок</option>
                                        </select>

                                        <div class="form-group">
                                            <label for="review">Відгук</label>
                                            <textarea name="review" id="review" cols="30" rows="6" class="form-control"></textarea>
                                        </div>


                                        <input type="submit" name="submit" value="Створити" class="btn btn-default theme-btn-2 w-100">

                                    </form>

                                <?php endif; ?>



                                <div class="tab-pane fade show active" id="desc-tab-1">
                                    <div class="single-product-tab-content">
                                        <h3 class="title mb-60 mt-60">Відгуки про номер</h3>
                                        <?php if(count($reviews) != 0): ?>

                                            <?php for($i=0;$i<count($reviews);$i++): ?>
                                                <?php for($j=0;$j<count($site_users_data);$j++): ?>
                                                    <?php if($reviews[$i]['id_site_user'] == $site_users_data[$j]['id']): ?>
                                                        <div class="review-box-item">
                                                            <div class="row">
                                                                <div class="col-xl-1 col-lg-2 col-md-2 col-3 pr-xl-0">
                                                                    <div class="review-box-img d-block text-right">
                                                                        <img src="/template/images/small-img/user.jpg" width="60" height="60" class="avatar-img" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-11 col-lg-10 col-9 pl-0">
                                                                    <div class="review-box-content">
                                                                        <div class="row">
                                                                            <div class="col-7">
                                                                                <div class="author-name">
                                                                                    <h6><?php echo($site_users_data[$j]['name']); ?> <?php echo($site_users_data[$j]['surname']); ?> (<?php echo($reviews[$i]['date']); ?>)</h6>
                                                                                    <div class="content">
                                                                                        <p class="mb-0"><?php echo($reviews[$i]['review']); ?></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-5 pr-5-px pl-0">
                                                                                <div class="rating text-right">
                                                                                    <?php for($k=0;$k<5;$k++): ?>
                                                                                        <i class="fa fa-solid fa-star
                                                                    <?php if($k < $reviews[$i]['stars']): ?>
                                                                    <?php echo('active') ?><?php endif; ?>"></i>
                                                                                    <?php endfor; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            <?php endfor; ?>

                                        <?php else: ?>
                                            <div class="review-box-item">
                                                <div class="row">
                                                    На жаль, відгуків до цього номеру ще ніхто не залишав...
                                                </div>
                                            </div>

                                            <br>
                                        <?php endif; ?>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

            </div>

        </div>
    </div>
</div>


<?php require_once('views/layouts/footer.php'); ?>


<!-- JS here -->
<script src="/template/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/template/js/popper.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/isotope.pkgd.min.js"></script>
<script src="/template/js/one-page-nav-min.js"></script>
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.meanmenu.min.js"></script>
<script src="/template/js/ajax-form.js"></script>
<script src="/template/js/fontawesome.min.js"></script>
<script src="/template/js/wow.min.js"></script>
<script src="/template/js/jquery-ui.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/imagesloaded.pkgd.min.js"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script src="/template/js/plugins.js"></script>
<script src="/template/js/main.js"></script>
</body>

</html>