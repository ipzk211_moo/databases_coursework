<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Штормове пансіонат - галерея</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Place favicon.ico in the root directory -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- preloader -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<?php require_once('views/layouts/header.php'); ?>

<div class="shop-body mb-90">
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <h1 class="page-name">Галерея</h1>

        </nav>
        <div class="shop-wrapper">
            <div class="row">

                <div class="col-12">
                    <div class="filter-heading">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">

                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12">

                            </div>
                            <div class="col-xl-4 col-lg-4 col-md-4 col-12 d-flex justify-content-end position-static">

                                <div class="filter">
                                    <h6 class="d-inline-block filter-widget-toggle">Застосувати фільтр сортування</h6>
                                    <div class="filter-popup">
                                        <div class="row">



                                            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 max-width-100 position-static">
                                                <div class="widget">
                                                    <h4 class="mb-30">Оберіть категорію</h4>
                                                    <div class="category-list">
                                                        <ul>

                                                            <?php for($i=0;$i<count($photo_categories);$i++): ?>

                                                                <li><a href="javascript:void(0)" class="clickable-filter" is-selected="no" id="<?php echo($photo_categories[$i]['id']); ?>"><?php echo($photo_categories[$i]['category']); ?></a></li>

                                                            <?php endfor; ?>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div id="filter-content" class="filter-content mt-30">

                        <div class="tab-pane fade  show active" id="shop-tab-3">
                            <div class="product-content-inner">
                                <div class="product-grid">
                                    <div class="row">
                                        <div class="center-content">
                                            <div id="updated-data-filtered" class="row">

                                                <?php foreach($photos_data as $photo):?>

                                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">

                                                    <div class="product-box position-relative mb-40 middle-view">
                                                        <div class="product-box-wrapper">
                                                            <div class="product-img">
                                                                <img src="/template/images/gallery_large/gallery-<?php echo($photo['id']); ?>.jpg" class="w-100" alt="<?php echo($photo['alt_attribute']); ?>">

                                                            </div>

                                                            <div class="product-desc">
                                                                <div class="product-desc-top">
                                                                    <div class="categories">
                                                                        <a href="javascript:void(0)" class="product-category"><span><?php echo($photo['subtitle']); ?></span></a>
                                                                    </div>
                                                                </div>
                                                                <a href="javascript:void(0)" class="product-title eright-turquoise-color-hover"><?php echo($photo['title']); ?></a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endforeach; ?>
                                            </div>
                                            <div class="text-center mt-20">
                                                <?php echo $pagination->get(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<?php require_once('views/layouts/footer.php'); ?>


<script>
    let expanded = false;

    let filterCategory = document.querySelectorAll('.clickable-filter');
    let arrSelected = [];
    filterCategory.forEach(el => el.addEventListener('click', function () {
        if(el.style.background == "rgb(71, 213, 235)") {
            el.style.background = "#f5f5f5";
            el.style.color = "#777779";
            el.setAttribute('is-selected','no');

        } else {
            el.style.background = "#47D5EB";
            el.style.color = "#fff";
            el.setAttribute('is-selected','yes');

            let pagin = document.getElementById("pagination");
            pagin.remove();
        }

    }))

</script>



<script>
    $(document).ready(function(){
        $(".clickable-filter").click(function() {
            filter_data();

            function filter_data()
            {
                var action = 'fetch_data';

                var category = get_filter('.clickable-filter');

                $.ajax({
                    url:"../../controllers/ajax/gallery.php",
                    method:"POST",
                    data:{action:action, category:category},
                    success:function(data){
                        $('#updated-data-filtered').html(data);
                    }
                });

            }

            function get_filter(class_name)
            {
                var filter = [];
                $(class_name).each(function(){
                    if($(this).attr('is-selected') === 'yes') {
                        filter.push($(this).attr('id'));
                    }

                });
                return filter;
            }

        });

        filter_data();

    });
</script>


<!-- JS here -->
<script src="/template/js/vendor/jquery-1.12.4.min.js"></script>
<script src="/template/popper.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/isotope.pkgd.min.js"></script>
<script src="/template/js/one-page-nav-min.js"></script>
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.meanmenu.min.js"></script>
<script src="/template/js/ajax-form.js"></script>
<script src="/template/js/fontawesome.min.js"></script>
<script src="/template/js/wow.min.js"></script>
<script src="/template/js/jquery-ui.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/imagesloaded.pkgd.min.js"></script>
<script src="/template/js/jquery.magnific-popup.min.js"></script>
<script src="/template/js/plugins.js"></script>
<script src="/template/js/main.js"></script>
</body>

</html>