<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Штормове пансіонат - управління canteen</title>

    <link rel="shortcut icon" type="image/x-icon" href="/template/images/favicon.ico">

    <link rel="stylesheet" href="/template/css/styles_large/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/owl.carousel.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/jquery-ui.css">
    <link rel="stylesheet" href="/template/css/styles_large/animate.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/magnific-popup.css">
    <link rel="stylesheet" href="/template/css/styles_large/fontawesome-all.min.css">
    <link rel="stylesheet" href="/template/css/styles_large/meanmenu.css">
    <link rel="stylesheet" href="/template/css/styles_large/slick.css">
    <link rel="stylesheet" href="/template/css/styles_large/default.css">
    <link rel="stylesheet" href="/template/css/styles_large/style.css">
    <link rel="stylesheet" href="/template/css/styles_large/responsive.css">

    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="/template/css/font-awesome.css">
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
    <link rel="stylesheet" href="/template/css/responsive.css">

    <link rel="stylesheet" href="/template/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
    <?php include 'views/layouts/header_admin.php'; ?>
    <main>

        <div class="shop-body mb-90">
            <div class="container-fluid">
                <h1 class="page-name">Управління таблицею canteen
                    <?php if($access['canteen_table'] == 4 || $access['canteen_table'] == 7): ?>
                        <a href="/admin/canteen/create" class="color-red underlined-text">(Додати новий запис)</a>
                    <?php endif; ?>
                </h1>
                <div class="cart-body-content">
                    <div class="row">

                        <?php if($all_dishes): ?>
                        <div class="col-xl-10 margin-auto">
                            <div class="product-content">
                                <form method="post">
                                    <div class="table-responsive">
                                        <table class="table table-2">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Назва</th>
                                                <th>Категорія</th>
                                                <th>Опис</th>
                                                <th>Інгредієнти</th>
                                                <th>Ціна</th>
                                                <th>Чи відображається</th>
                                                <th>Чи тренд</th>
                                                <?php if($access['canteen_table'] == 4 || $access['canteen_table'] == 7): ?>
                                                    <th></th>
                                                <?php endif; ?>
                                                <?php if($access['canteen_table'] == 5 || $access['canteen_table'] == 7): ?>
                                                    <th></th>
                                                <?php endif; ?>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <?php foreach($all_dishes as $dish): ?>
                                                <tr>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['id']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['name']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['category']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo(mb_substr($dish['description'], 0, 30)."..."); ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo(mb_substr($dish['ingredients'], 0, 30)."..."); ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['price']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['is_showing']; ?>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="table-data">
                                                            <?php echo $dish['is_trend']; ?>
                                                        </div>
                                                    </td>

                                                    <?php if($access['canteen_table'] == 4 || $access['canteen_table'] == 7): ?>
                                                        <td>
                                                            <div class="table-data">
                                                                <a href="/admin/canteen/update/<?php echo $dish['id']; ?>">Редагувати</a>
                                                            </div>
                                                        </td>
                                                    <?php endif; ?>

                                                    <?php if($access['canteen_table'] == 5 || $access['canteen_table'] == 7): ?>
                                                        <td>
                                                            <div class="table-data">
                                                                <a href="/admin/canteen/delete/<?php echo $dish['id']; ?>">Видалити</a>
                                                            </div>
                                                        </td>
                                                    <?php endif; ?>

                                                </tr>

                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include 'views/layouts/footer_admin.php'; ?>
</body>
</html>